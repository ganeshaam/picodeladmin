package com.example.picodeladmin;

public class ShopModel {

    private String shop_id,admin_type,sellername,shop_name,contact,area,email,recharge_amount,recharge_points,shop_category,preferredDeliveryAreaKm,rejectedOrderId,
    view_time,order_id,view_status;

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public String getAdmin_type() {
        return admin_type;
    }

    public void setAdmin_type(String admin_type) {
        this.admin_type = admin_type;
    }

    public String getSellername() {
        return sellername;
    }

    public void setSellername(String sellername) {
        this.sellername = sellername;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRecharge_amount() {
        return recharge_amount;
    }

    public void setRecharge_amount(String recharge_amount) {
        this.recharge_amount = recharge_amount;
    }

    public String getRecharge_points() {
        return recharge_points;
    }

    public void setRecharge_points(String recharge_points) {
        this.recharge_points = recharge_points;
    }

    public String getShop_category() {
        return shop_category;
    }

    public void setShop_category(String shop_category) {
        this.shop_category = shop_category;
    }

    public String getPreferredDeliveryAreaKm() {
        return preferredDeliveryAreaKm;
    }

    public void setPreferredDeliveryAreaKm(String preferredDeliveryAreaKm) {
        this.preferredDeliveryAreaKm = preferredDeliveryAreaKm;
    }

    public String getRejectedOrderId() {
        return rejectedOrderId;
    }

    public void setRejectedOrderId(String rejectedOrderId) {
        this.rejectedOrderId = rejectedOrderId;
    }

    public String getView_time() {
        return view_time;
    }

    public void setView_time(String view_time) {
        this.view_time = view_time;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getView_status() {
        return view_status;
    }

    public void setView_status(String view_status) {
        this.view_status = view_status;
    }
}
