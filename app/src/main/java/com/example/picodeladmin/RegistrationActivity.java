package com.example.picodeladmin;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class RegistrationActivity extends AppCompatActivity {

    Spinner sp_getCity;
    EditText et_mobileNumber,et_name,et_fc_city,et_email;
    ArrayList<String> cityList;
    Button btn_register;
    private String city_value;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        sp_getCity = findViewById(R.id.sp_getCity);
        et_mobileNumber = findViewById(R.id.et_mobileNumber);
        et_name = findViewById(R.id.et_name);
        et_fc_city = findViewById(R.id.et_fc_city);
        btn_register = findViewById(R.id.btn_register);
        et_email = findViewById(R.id.et_email);


        cityList = new ArrayList<>();


        sp_getCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                city_value = sp_getCity.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        getCityList();

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (city_value.isEmpty()) {
                    Toast.makeText(RegistrationActivity.this, "Please Select City Value", Toast.LENGTH_LONG).show();
                } else if (et_mobileNumber.getText().toString().isEmpty()) {
                    Toast.makeText(RegistrationActivity.this, "Please Enter the Contact ", Toast.LENGTH_LONG).show();
                    et_mobileNumber.setError("Please Enter the Contact");
                } else if (et_name.getText().toString().isEmpty()) {
                    Toast.makeText(RegistrationActivity.this, "Please Enter the Name", Toast.LENGTH_LONG).show();
                    et_name.setError("Please Enter the Name");

                }else if (et_email.getText().toString().isEmpty()) {
                    Toast.makeText(RegistrationActivity.this, "Please Enter the Email", Toast.LENGTH_LONG).show();
                    et_email.setError("Please Enter the Email");

                } else {

                    et_name.clearFocus();
                    Log.e("spenner value",""+city_value);
                    registerAdmin(city_value);
                    //Toast.makeText(RegistrationActivity.this, "On Success", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    // get the city list at the spinner value
    private void getCityList() { //TODO Server method here
        if (Connectivity.isConnected(RegistrationActivity.this)) {
            cityList.clear();

            /*JSONObject params = new JSONObject();
            try {
                params.put("contact", gateWay.getContact());
                params.put("v_city", sp_city_spinner.getSelectedItem().toString()); //sp_city_value
                params.put("v_state", spinner.getSelectedItem().toString());
                params.put("type", "Android");//true or false
                Log.e("userParm:",""+params);
            } catch (JSONException e) {
                e.printStackTrace();
            }*/

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.activeCityForAdmin, null, new Response.Listener<JSONObject>() {

                @SuppressWarnings("CanBeFinal")
                @Override
                public void onResponse(JSONObject response) {
                    //  if (!response.isNull("posts")) {
                    try {



                        JSONArray cityJsonArray = response.getJSONArray("posts");
                        for (int i = 0; i < cityJsonArray.length(); i++) {
                            JSONObject jsonCityData = cityJsonArray.getJSONObject(i);
                            cityList.add(jsonCityData.getString("city"));
                        }
                        /*CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(LocationActivity.this, areaList, null, null, "location");
                        sp_Area_spinner.setAdapter(customSpinnerAdapter);*/

                        ArrayAdapter<String> adapter = new ArrayAdapter(RegistrationActivity.this,android.R.layout.simple_spinner_item,cityList);
                        sp_getCity.setAdapter(adapter);

                        //share preparence add city


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //gateWay.progressDialogStop();

                }
                //}
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    //TODO ServerError method here
                }
            });
            // AppController.getInstance().addToRequestQueue(request);
            VolleySingleton.getInstance(RegistrationActivity.this).addToRequestQueue(request);
        }
    }

    // get the city list at the spinner value  //add_adminDetails.php
    private void registerAdmin(String city_value) { //TODO Server method here
        if (Connectivity.isConnected(RegistrationActivity.this)) {
            cityList.clear();

            JSONObject params = new JSONObject();
            try {
                params.put("contact", et_mobileNumber.getText().toString());
                params.put("name", et_name.getText().toString()); //sp_city_value
                params.put("city", city_value);
                params.put("fc_city", et_fc_city.getText().toString());
                params.put("fc_city", et_email.getText().toString());
                Log.e("regParm:",""+params);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.add_adminDetails, params, new Response.Listener<JSONObject>() {

                @SuppressWarnings("CanBeFinal")
                @Override
                public void onResponse(JSONObject response) {

                    Log.e("RegResp:",""+response);
                            //insert successfully
                    try {
                        String result = response.getString("posts");
                         /*JSONObject jsonObject = response.getJSONObject("posts");
                          for(int i=0; i<jsonObject.length();i++){
                              String value =  jsonObject.getString("posts");
                              if(value.equalsIgnoreCase("insert successfully")){

                                  Toast.makeText(RegistrationActivity.this,"Registration Successfully!",Toast.LENGTH_LONG).show();
                              }
                          }*/

                        if(result.equalsIgnoreCase("insert successfully")){

                            Toast.makeText(RegistrationActivity.this,"Registration Successfully!",Toast.LENGTH_LONG).show();
                        }else {

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    finish();
                }
                //}
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    //TODO ServerError method here
                }
            });
            // AppController.getInstance().addToRequestQueue(request);
            VolleySingleton.getInstance(RegistrationActivity.this).addToRequestQueue(request);
        }
    }
}
