package com.example.picodeladmin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import dmax.dialog.SpotsDialog;

public class RejectedOrderActivity extends AppCompatActivity {

    Context mContext;
    AlertDialog progressDialog;
    private RecyclerView.LayoutManager layoutManager;
    ArrayList<ShopModel> orderArrayList;
    private RejectedOrderAdapter rejectedOrderAdapter;
    RecyclerView rv_rejectedOrder;
    TextView tv_network_con;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rejected_order);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Rejected Order List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = RejectedOrderActivity.this;
        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();

        tv_network_con = findViewById(R.id.tv_network_con);
        rv_rejectedOrder = findViewById(R.id.rv_rejectedOrder);

        orderArrayList = new ArrayList<>();
        rv_rejectedOrder.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(mContext);
        rv_rejectedOrder.setLayoutManager(layoutManager);
        rv_rejectedOrder.setItemAnimator(new DefaultItemAnimator());
        rejectedOrderAdapter = new RejectedOrderAdapter(orderArrayList,mContext);
        rv_rejectedOrder.setAdapter(rejectedOrderAdapter);

        if(Connectivity.isConnected(mContext)){
            getRejectedOrderList();
        }else {
            tv_network_con.setVisibility(View.VISIBLE);
        }

        rv_rejectedOrder.addOnItemTouchListener(new RejectedOrderActivity.RecyclerTouchListener(mContext, rv_rejectedOrder,
                new RejectedOrderActivity.RecyclerTouchListener.ClickListener()
                {
                    @Override
                    public void onClick(View view, int position)
                    {
                        String orderId = orderArrayList.get(position).getRejectedOrderId();
                         Intent intent = new Intent(mContext,RejectedOrderReportActivity.class);
                         intent.putExtra("orderId",orderId);
                       //  intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(intent);
                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {
                        Toast.makeText(mContext,"No Records",Toast.LENGTH_SHORT).show();
                    }
                }));
    }

    public void getRejectedOrderList(){

        progressDialog.show();
        orderArrayList.clear();

        String targetUrlOrderList = StaticUrl.getrejectedorder;

        Log.d("targetUrlOrderList",targetUrlOrderList);
        StringRequest orderListRequest = new StringRequest(Request.Method.GET,
                targetUrlOrderList,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("orderlist",response);
                        //cv_viewmore.setVisibility(View.VISIBLE);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean strStatus = jsonObject.getBoolean("status");
                            if ((strStatus==true)){
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if(jsonArray.length()>0){
                                    // Log.d("orderlist",""+jsonArray);
                                    for(int i = 0; i < jsonArray.length(); i++){
                                        JSONObject olderlist = jsonArray.getJSONObject(i);
                                        ShopModel model = new ShopModel();
                                         model.setRejectedOrderId(olderlist.getString("order_id"));
                                         orderArrayList.add(model);
                                    }
                                    rejectedOrderAdapter.notifyDataSetChanged();
                                }else {
                                    tv_network_con.setVisibility(View.VISIBLE);
                                    tv_network_con.setText("No Records Found");
                                }
                            }else if(strStatus=false){
                                tv_network_con.setVisibility(View.VISIBLE);
                                tv_network_con.setText("No Records Found");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(mContext).addToRequestQueue(orderListRequest);
    }

    //RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private RejectedOrderActivity.RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final RejectedOrderActivity.RecyclerTouchListener.ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
