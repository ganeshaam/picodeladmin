package com.example.picodeladmin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class ShopEnquireAdapter  extends RecyclerView.Adapter<ShopEnquireAdapter.ShopEnquireHolder>{

    ArrayList<ShopEnquireModel> shopEnquireModelArrayList;
    Context mContext;

    public ShopEnquireAdapter(ArrayList<ShopEnquireModel> shopEnquireModelArrayList, Context mContext) {
        this.shopEnquireModelArrayList = shopEnquireModelArrayList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ShopEnquireHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View  view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_shoplistenquire,parent,false);
        ShopEnquireAdapter.ShopEnquireHolder shopEnquireHolder = new ShopEnquireAdapter.ShopEnquireHolder(view);
        return shopEnquireHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ShopEnquireHolder holder, int position) {
        TextView tv_shopName = holder.tv_shopName;
        TextView tv_sellerName = holder.tv_sellerName;
        TextView tv_contact = holder.tv_contact;
        TextView tv_area = holder.tv_area;

        tv_shopName.setText(shopEnquireModelArrayList.get(position).getShopName());
        tv_sellerName.setText(shopEnquireModelArrayList.get(position).getSellerName());
        tv_area.setText(shopEnquireModelArrayList.get(position).getArea());
        tv_contact.setText(shopEnquireModelArrayList.get(position).getContact());
    }

    @Override
    public int getItemCount() {
        return shopEnquireModelArrayList.size();
    }

    public static class ShopEnquireHolder extends RecyclerView.ViewHolder{
        TextView tv_shopName,tv_sellerName,tv_contact,tv_area;
        public ShopEnquireHolder(@NonNull View itemView) {
            super(itemView);
            tv_shopName = itemView.findViewById(R.id.tv_shopName);
            tv_sellerName = itemView.findViewById(R.id.tv_sellerName);
            tv_contact = itemView.findViewById(R.id.tv_contact);
            tv_area = itemView.findViewById(R.id.tv_area);
        }
    }
}
