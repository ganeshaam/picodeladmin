package com.example.picodeladmin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import dmax.dialog.SpotsDialog;

public class OrderDetailActivity extends AppCompatActivity {

    String shopId,sessionMainCat,areaName,aam_delivery_charge,updatedat,showcart, pincode, flag, action_time, orderId, cartUniqueId, userId, cartUserId, shipping_address, latitude, longitude, total_amount, order_date, address_id, assign_delboy_id, reason_of_rejection,delivery_time="", address1, address2, address3, address4, address5,
            area,delivery_date,delivery_time_get,delivery_method,payment_option,user_remark,order_count,nearbyshopid;
    TextView tv_shopname,tv_shopkeeperName,tv_shopContact,tv_od_delorderdate,tv_od_deliverycharge,tv_od_orderdelidate,tv_view_cartitem, tv_delboyname, tv_delboymo, tv_od_orderid, tv_od_totalamount, tv_od_orderdate, tv_network_con, tv_address_pincode, tv_address1, tv_address2, tv_address3, tv_address4, tv_address5, tv_area,tv_payment_options,tv_userName,tv_user_remark,tv_user_terms,
            tv_productBrand,tv_productName,tv_productPrice,tv_productQty,tv_last_shop,tv_nshopName,tv_oder_from;
    RecyclerView recyclerViewCartItems;
    Button btn_acceptorder, btn_cancelorder, btn_assignorder, btn_deliverorder, btn_intrested_but;
    Context mContext;
    ArrayList<CartitemModel> cartitemArrayList;
    ArrayList<ShopModel> shopArrayList;
    private RecyclerView.LayoutManager layoutManager;
    private CartListAdapter cartListAdapter;
    private ShopViewDetailsAdaptor shopViewDetailsAdaptor;
    AlertDialog progressDialog;
    SharedPreferencesUtils sharedPreferencesUtils;
    RelativeLayout rl_delboydetail,rl_onerupee_layout;
    ImageView iv_delboy,iv_productimage,img_download_pdf;
    String slot="two",user_fname="", userContact;

    ArrayList<String> Next3Hours;
    ArrayList<String> Slot_ONE;
    ArrayList<String> Slot_TWO;
    ArrayList<String> Slot_Three;
    ArrayList<String> Slot_Four;
    ArrayList<String> Slot_schedules;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Order Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = OrderDetailActivity.this;

        Next3Hours = new ArrayList<>();
        //Slot_Init();

        tv_od_orderid = findViewById(R.id.tv_od_orderid);
        tv_oder_from = findViewById(R.id.tv_oder_from);
        tv_od_totalamount = findViewById(R.id.tv_od_totalamount);
        tv_od_orderdate = findViewById(R.id.tv_od_orderdate);

        tv_address1 = findViewById(R.id.tv_address1);
        tv_address2 = findViewById(R.id.tv_address2);
        tv_address3 = findViewById(R.id.tv_address3);
        tv_address4 = findViewById(R.id.tv_address4);
        tv_address5 = findViewById(R.id.tv_address5);
        tv_area = findViewById(R.id.tv_area);
        tv_address_pincode = findViewById(R.id.tv_address_pincode);

        tv_network_con = findViewById(R.id.tv_network_con);
        recyclerViewCartItems = findViewById(R.id.recyclerViewCartItems);
        btn_acceptorder = findViewById(R.id.btn_acceptorder);
        btn_cancelorder = findViewById(R.id.btn_cancelorder);
        btn_intrested_but = findViewById(R.id.btn_intrested_but);
        btn_assignorder = findViewById(R.id.btn_assignorder);
        btn_deliverorder = findViewById(R.id.btn_deliverorder);

        rl_delboydetail = findViewById(R.id.rl_delboydetail);
        rl_onerupee_layout = findViewById(R.id.rl_onerupee_layout);
        iv_delboy = findViewById(R.id.iv_delboy);
        tv_delboyname = findViewById(R.id.tv_delboyname);
        tv_delboymo = findViewById(R.id.tv_delboymo);
        tv_view_cartitem = findViewById(R.id.tv_view_cartitem);
        tv_od_orderdelidate= findViewById(R.id.tv_od_orderdelidate);
        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();
        tv_network_con = findViewById(R.id.tv_network_con);
        tv_od_deliverycharge = findViewById(R.id.tv_od_deliverycharge);
        tv_od_delorderdate = findViewById(R.id.tv_od_delorderdate);
        tv_payment_options = findViewById(R.id.tv_payment_options);
        tv_user_remark = findViewById(R.id.tv_user_remark);
        tv_user_terms = findViewById(R.id.tv_user_terms);
        tv_shopname= findViewById(R.id.tv_shopname);
        tv_shopkeeperName= findViewById(R.id.tv_shopkeeperName);
        tv_shopContact= findViewById(R.id.tv_shopContact);
        tv_userName= findViewById(R.id.tv_userName);
        tv_last_shop =findViewById(R.id.tv_last_shop);
        tv_nshopName = findViewById(R.id.tv_nshopName);
        //One Rupee Product Details

        //,,,;
        tv_productBrand = findViewById(R.id.tv_productBrand);
        tv_productName = findViewById(R.id.tv_productName);
        tv_productPrice = findViewById(R.id.tv_productPrice);
        tv_productQty = findViewById(R.id.tv_productQty);
        iv_productimage = findViewById(R.id.iv_productimage);
        img_download_pdf = findViewById(R.id.img_download_pdf);

        recyclerViewCartItems.setHasFixedSize(true);
        cartitemArrayList = new ArrayList<>();
        shopArrayList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(mContext);
        recyclerViewCartItems.setLayoutManager(layoutManager);
        recyclerViewCartItems.setItemAnimator(new DefaultItemAnimator());
        cartListAdapter = new CartListAdapter(cartitemArrayList, mContext);
        recyclerViewCartItems.setAdapter(cartListAdapter);

        final Intent intent = getIntent();
        orderId = intent.getStringExtra("orderId");
        cartUniqueId = intent.getStringExtra("cartUniqueId");
        userId = intent.getStringExtra("userId");
        cartUserId = intent.getStringExtra("cartUserId");
        //shipping_address = intent.getStringExtra("shipping_address");
        latitude = intent.getStringExtra("latitude");
        longitude = intent.getStringExtra("longitude");
        total_amount = intent.getStringExtra("total_amount");
        order_date = intent.getStringExtra("order_date");
        address_id = intent.getStringExtra("address_id");
        assign_delboy_id = intent.getStringExtra("assign_delboy_id");
        action_time = intent.getStringExtra("action_time");
        address1 = intent.getStringExtra("address1");
        address2 = intent.getStringExtra("address2");
        address3 = intent.getStringExtra("address3");
        address4 = intent.getStringExtra("address4");
        address5 = intent.getStringExtra("address5");
        area = intent.getStringExtra("area");
        pincode = intent.getStringExtra("pincode");
        flag = intent.getStringExtra("flag");
        showcart = intent.getStringExtra("showcart");
        updatedat = intent.getStringExtra("updatedat");
        aam_delivery_charge = intent.getStringExtra("aam_delivery_charge");
        areaName = intent.getStringExtra("areaName");
        delivery_date = intent.getStringExtra("delivery_date");
        delivery_time_get = intent.getStringExtra("delivery_time");
        delivery_method = intent.getStringExtra("delivery_method");
        payment_option = intent.getStringExtra("payment_option");
        sessionMainCat = intent.getStringExtra("sessionMainCat");
        shopId = intent.getStringExtra("assignShopId");
        user_remark = intent.getStringExtra("user_remark");
        order_count = intent.getStringExtra("order_count");
        nearbyshopid = intent.getStringExtra("nearbyshopid");

        if(nearbyshopid.isEmpty()||nearbyshopid == null||nearbyshopid.equalsIgnoreCase("0")){
            tv_nshopName.setText("Selected Shop: NA");
        }else {
            getShopDetails2(nearbyshopid);

        }

        if(flag.equals("admin")||flag.equalsIgnoreCase("areaWise_order")){
            btn_acceptorder.setVisibility(View.VISIBLE);
            rl_delboydetail.setVisibility(View.VISIBLE);
            btn_deliverorder.setVisibility(View.VISIBLE);
        }else
        if (flag.equals("new_order")) {
            btn_acceptorder.setVisibility(View.VISIBLE);
            btn_intrested_but.setVisibility(View.VISIBLE);
            rejectionStatus(sharedPreferencesUtils.getShopID(), orderId);
        } else if (flag.equals("approved_order")) {
            btn_acceptorder.setVisibility(View.GONE);
            btn_cancelorder.setVisibility(View.VISIBLE);
            btn_assignorder.setVisibility(View.VISIBLE);
            btn_intrested_but.setVisibility(View.GONE);
        } else if (flag.equals("assigned_order")) {
            btn_acceptorder.setVisibility(View.GONE);
            btn_cancelorder.setVisibility(View.VISIBLE);
            btn_assignorder.setVisibility(View.VISIBLE);
            btn_deliverorder.setVisibility(View.VISIBLE);
            rl_delboydetail.setVisibility(View.VISIBLE);
            // btn_assignorder.setText("Re-assign");
            btn_assignorder.setText(getResources().getString(R.string.btn_reassign_order));
            btn_intrested_but.setVisibility(View.GONE);
        } else if (flag.equals("assigned_delboy_order")) {
            btn_acceptorder.setVisibility(View.GONE);
            btn_cancelorder.setVisibility(View.GONE);
            btn_assignorder.setVisibility(View.GONE);
            btn_deliverorder.setVisibility(View.VISIBLE);
            btn_intrested_but.setVisibility(View.GONE);
        } else if (flag.equals("histroy_order")) {
            btn_acceptorder.setVisibility(View.GONE);
            btn_cancelorder.setVisibility(View.GONE);
            btn_assignorder.setVisibility(View.GONE);
            btn_deliverorder.setVisibility(View.GONE);
            rl_delboydetail.setVisibility(View.VISIBLE);
            btn_intrested_but.setVisibility(View.GONE);
        } else {
            btn_acceptorder.setVisibility(View.VISIBLE);
        }

        img_download_pdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://www.picodel.com/seller/shopwise-orders/pdffile?id="+orderId));
                startActivity(intent);
            }
        });

        // Log.e("commitLog_monday:", "" + "ok_1_07_2019");
        //Log.e("ActionTime:", "" + action_time);

        //Check cancel order time calculations
        DiffCalculate(action_time);
        setOrderDetails();
        //Check Rejected status
        //rejectionStatus(sharedPreferencesUtils.getShopID(), orderId);

        if (Connectivity.isConnected(mContext)) {
                getDeliveryboydetails(assign_delboy_id);
                getOrderCartList();
                getSingleItem();
                getShopDetails(shopId);
               //getUserOrderAddress(address_id,userId);
        } else {
            tv_network_con.setVisibility(View.VISIBLE);
        }
        //accept order
        btn_acceptorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                chkOrderStatus();

            }
        });
      /*  //cancel accepted order
        btn_cancelorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Shop keeper cancel order with in 30 min required to check action_time is more than 30 min
                Log.d("action_time", action_time);
                //change status   assign_shop_id=sharedPreferencesUtils.getShopID()
                String name[] = {"Price Mismatch\n(प्रोडक्ट के मूल्य में अंतर)", "Delivery Boy issue\n(डिलीवरी बॉय नही है)", "All items not available\n(कोई भी आइटम उपलब्ध नही है)", "2 items not available\n(2 आइटम उपलब्ध नही है)", "4 items not available\n(4 आइटम उपलब्ध नही है)", "Other issue\n(अन्य समस्या)"};
                selectReason(name);
            }
        });
        //cancel before accept
        btn_intrested_but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String name[] = {"No Price Mismatch", "Other issue"};
                //String name[] = {"No, Price Mismatch", "No, Delivery Boy issue", "No, all items not available", "No,2 items not available", "No,4 items not available", "Other issue"};
                //String name[] = {"Price Mismatch", "Delivery Boy issue", "All items not available", "2 items not available", "4 items not available", "Other issue"};
                String name[] = {"Price Mismatch\n(प्रोडक्ट के मूल्य में अंतर)", "Delivery Boy issue\n(डिलीवरी बॉय नही है)", "All items not available\n(कोई भी आइटम उपलब्ध नही है)", "2 items not available\n(2 आइटम उपलब्ध नही है)", "4 items not available\n(4 आइटम उपलब्ध नही है)", "Other issue\n(अन्य समस्या)"};
                assign_delboy_id = "0";
                selectReason(name);
            }
        });
        //assign order
        btn_assignorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open delivery boy list
                Intent intent = new Intent(mContext, AddDeliveryBoyActivity.class);
                intent.putExtra("assignOrder", "assignOrder");
                intent.putExtra("orderid", orderId);
                intent.putExtra("action_time", action_time);
                //startActivityForResult(intent, 1);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });


*/
        //view cart item from order history if show cart status is 1
        tv_view_cartitem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (showcart.equals("1")) {
                    getOrderCartList();
                } else {
                    tv_network_con.setVisibility(View.VISIBLE);
                    tv_network_con.setText("Please Contact to PICODEL Admin To view this cart item list");
                }
            }
        });

        //delivered order
        btn_deliverorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //orderid, shopid, delboyid
                Log.e("Hello","Test");
                Intent deliverOrdered_intent = new Intent(mContext, OrderDeliverdActivity.class);
                deliverOrdered_intent.putExtra("orderid", orderId);
                deliverOrdered_intent.putExtra("delboyid", assign_delboy_id);
                startActivity(deliverOrdered_intent);

            }
        });

        calculateDeliveryTime2();

        getUserName();

        getViewShopList();


        rl_delboydetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shopviewListDialog();
            }
        });

        tv_userName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openWhatsApp(userContact);
            }
        });

    }


    //order details
    public void setOrderDetails() {

        tv_address1.setVisibility(View.VISIBLE);
        tv_address3.setVisibility(View.VISIBLE);
        tv_address4.setVisibility(View.VISIBLE);
        tv_area.setVisibility(View.VISIBLE);
        tv_address_pincode.setVisibility(View.VISIBLE);
        rl_delboydetail.setVisibility(View.VISIBLE);
        tv_od_orderdelidate.setVisibility(View.VISIBLE);

        tv_od_orderid.setText("Order Id: " + orderId);
        tv_od_totalamount.setText("Total Amount: " + total_amount + " " + getString(R.string.Rs));
        tv_od_deliverycharge.setText("Delivery Charge: " + aam_delivery_charge + " " + getString(R.string.Rs));
        tv_od_orderdate.setText("Order Placed Date: " + order_date);
        tv_od_delorderdate.setText("Order Delivery Date: "+delivery_date+"  Time: "+delivery_time_get);
        tv_od_orderdelidate.setText("Order Delivered Date Time: " + updatedat);
        tv_address2.setText("Building: "+address2);
        tv_address1.setText(address1);
        Log.d("address1",address1);
        tv_address3.setText(address3);
        tv_address4.setText(address4);
        tv_area.setText("Area: "+area);
        tv_address_pincode.setText("Order Category: "+sessionMainCat);
        tv_payment_options.setText("Payment Options: " + payment_option);
        tv_user_remark.setText("User Remark:"+user_remark);


        if ((flag.equals("assigned_order")) || (flag.equals("assigned_delboy_order"))) {
            tv_address1.setVisibility(View.VISIBLE);
            tv_address3.setVisibility(View.VISIBLE);
            tv_address4.setVisibility(View.VISIBLE);
            tv_address5.setVisibility(View.VISIBLE);
            tv_address_pincode.setVisibility(View.VISIBLE);
            rl_delboydetail.setVisibility(View.VISIBLE);
            tv_address1.setText(address1);
            tv_address3.setText(address3);
            tv_address4.setText(address4);
            tv_address5.setText(address5);
            tv_address_pincode.setText(pincode);
        } else if (flag.equals("histroy_order")) {
            tv_address1.setVisibility(View.GONE);
            tv_address2.setVisibility(View.GONE);
            tv_address3.setVisibility(View.GONE);
            tv_address4.setVisibility(View.GONE);
            tv_address5.setVisibility(View.GONE);
            tv_address_pincode.setVisibility(View.GONE);
            tv_area.setVisibility(View.GONE);
            rl_delboydetail.setVisibility(View.VISIBLE);
            tv_view_cartitem.setVisibility(View.VISIBLE);
            tv_od_orderdelidate.setVisibility(View.VISIBLE);

        }
    }

    //cart list
    public void getSingleItem() {

        progressDialog.show();
        cartitemArrayList.clear();

        String urlCartlist = StaticUrl.singeitem
                + "?order_id=" + orderId;


        Log.d("urlSingleItem", urlCartlist);

        StringRequest requestCartList = new StringRequest(Request.Method.GET,
                urlCartlist,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("SingleItemRes", response);
                            boolean strStatus = jsonObject.getBoolean("status");
                            String user_terms = jsonObject.getString("user_terms");
                            String order_from = jsonObject.getString("order_from");
                            tv_user_terms.setText("Alternate Brand and All Items:"+user_terms);
                            tv_oder_from.setText("From: "+order_from);
                            if ((strStatus == true)) {
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if (jsonArray.length() > 0) {
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject cartListObject = jsonArray.getJSONObject(i);
                                        CartitemModel model = new CartitemModel();
                                        model.setBrand_name(cartListObject.getString("product_brand"));
                                        model.setProduct_name(cartListObject.getString("product_name")+"  "+cartListObject.getString("product_size")+" "+cartListObject.getString("product_sizein"));
                                        //model.setIten_qty(cartListObject.getString("iten_qty"));
                                        model.setProduct_mrp(cartListObject.getString("product_mrp"));
                                        model.setItem_price(cartListObject.getString("product_price"));
                                        model.setProduct_image(cartListObject.getString("product_image"));
                                        //model.setUpdated_staus(cartListObject.getString("updated_status"));
                                        //cartitemArrayList.add(model);
                                        rl_onerupee_layout.setVisibility(View.VISIBLE);
                                        //,,,;
                                        tv_productBrand.setText("Brand :"+cartListObject.getString("product_brand"));
                                        tv_productName.setText("Name:"+cartListObject.getString("product_name")+"  "+cartListObject.getString("product_size")+" "+cartListObject.getString("product_sizein"));
                                        tv_productPrice.setText("Price:"+cartListObject.getString("product_price"));
                                        tv_productQty.setText("QTY: 1");
                                        String imgProductUrl = "https://www.simg.picodel.com/"+cartListObject.getString("product_image");
                                        Picasso.with(mContext).load(imgProductUrl)
                                                .placeholder(R.drawable.loading)
                                                .error(R.drawable.loading)
                                                .into(iv_productimage);
                                    }
                                    //cartListAdapter.notifyDataSetChanged();
                                } else {
                                    //tv_network_con.setText("No Records Found");
                                }
                            } else if (strStatus == false) {
                                /*tv_network_con.setVisibility(View.VISIBLE);
                                tv_network_con.setText("No Records Found");*/
                                rl_onerupee_layout.setVisibility(View.GONE);
                                Toast.makeText(mContext,"No OneRuppe Item Found",Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(mContext).addToRequestQueue(requestCartList);

    }

    //cart list
    public void getOrderCartList() {

        progressDialog.show();
        cartitemArrayList.clear();

        String urlCartlist = StaticUrl.cartlist
                + "?cart_unique_id=" + cartUniqueId
                + "&user_id=" + userId
                + "&tblName=Seller"+"Admin";


        Log.d("urlCartlist", urlCartlist);

        StringRequest requestCartList = new StringRequest(Request.Method.GET,
                urlCartlist,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("cartListRes", response);
                            boolean strStatus = jsonObject.getBoolean("status");
                            if ((strStatus = true)) {
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if (jsonArray.length() > 0) {
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject cartListObject = jsonArray.getJSONObject(i);
                                        CartitemModel model = new CartitemModel();
                                        model.setBrand_name(cartListObject.getString("product_brand"));
                                        model.setProduct_name(cartListObject.getString("product_name")+"  "+cartListObject.getString("product_size")+" "+cartListObject.getString("product_sizein"));
                                        model.setIten_qty(cartListObject.getString("iten_qty"));
                                        model.setProduct_mrp(cartListObject.getString("product_mrp"));
                                        model.setItem_price(cartListObject.getString("item_price"));
                                        model.setProduct_image(cartListObject.getString("product_image"));
                                        model.setUpdated_staus(cartListObject.getString("updated_status"));
                                        cartitemArrayList.add(model);
                                    }
                                    cartListAdapter.notifyDataSetChanged();
                                } else {
                                    tv_network_con.setText("No Records Found");
                                }
                            } else if (strStatus = false) {
                                tv_network_con.setVisibility(View.VISIBLE);
                                tv_network_con.setText("No Records Found");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(mContext).addToRequestQueue(requestCartList);

    }

    //get delivery boy details
    public void getDeliveryboydetails(String assign_delboy_id) {
        progressDialog.show();

        String targetUrl = StaticUrl.getDeliveryboy + "?assign_delboy_id=" + assign_delboy_id;

        StringRequest requestUserAdddress = new StringRequest(Request.Method.GET,
                targetUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("getDeliveryboydetails", response);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status = jsonObject.getBoolean("status");

                            if (status = true) {
                                JSONObject jsonDelboy = jsonObject.getJSONObject("data");

                                String imgUrl = jsonDelboy.getString("imageurl");
                                Picasso.with(mContext).load(imgUrl)
                                        .placeholder(R.drawable.loading)
                                        .error(R.drawable.ic_account_box_black_24dp)
                                        .into(iv_delboy);
                                tv_delboyname.setText(jsonDelboy.getString("name"));
                                tv_delboymo.setText(jsonDelboy.getString("mobile_number"));
                            } else if (status = false) {
                                String data = jsonObject.getString("data");
                                Toast.makeText(mContext, data, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                }
        );
        MySingleton.getInstance(mContext).addToRequestQueue(requestUserAdddress);
    }

    //get shop details
    public void getShopDetails(String shopId) {
        progressDialog.show();

        String targetUrl = StaticUrl.getShopDetails+"?shopid="+shopId;

        StringRequest requestShopDetails = new StringRequest(Request.Method.GET,
                targetUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("getDeliveryboydetails", response);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status = jsonObject.getBoolean("status");

                            if (status == true) {
                                tv_shopname.setText("Shop Name: "+jsonObject.getString("shop_name"));
                                tv_shopkeeperName.setText("Shop Keeper Name: "+jsonObject.getString("sellername"));
                                tv_shopContact.setText(jsonObject.getString("contact"));
                            } else if (status == false) {
                                tv_shopname.setText("Shop Name");
                                tv_shopkeeperName.setText("Shop Keeper Name");
                                tv_shopContact.setText("Contact");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                }
        );
        MySingleton.getInstance(mContext).addToRequestQueue(requestShopDetails);
    }

    //get shop details
    public void getShopDetails2(String shopId) {
        progressDialog.show();

        String targetUrl = StaticUrl.getShopDetails+"?shopid="+shopId;

        StringRequest requestShopDetails = new StringRequest(Request.Method.GET,
                targetUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("getDeliveryboydetails", response);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status = jsonObject.getBoolean("status");

                            if (status == true) {
                                //tv_shopname.setText("Shop Name: "+jsonObject.getString("shop_name"));
                                String contact = jsonObject.getString("contact");
                                tv_nshopName.setText("Shop Selected:"+jsonObject.getString("shop_name")+"  contact: "+contact);
                                /*tv_shopkeeperName.setText("Shop Keeper Name: "+jsonObject.getString("sellername"));
                                tv_shopContact.setText(jsonObject.getString("contact"));*/
                            } else if (status == false) {
                                tv_nshopName.setText("NA");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                }
        );
        MySingleton.getInstance(mContext).addToRequestQueue(requestShopDetails);
    }

    //check order is accepted
    public void chkOrderStatus(){

        if(Connectivity.isConnected(mContext)){

            progressDialog.show();

            String urlAcceptOrder = StaticUrl.chkorderstatus+"?order_id="+orderId;

            StringRequest stringRequestAcceptOrder = new StringRequest(Request.Method.GET,
                    urlAcceptOrder,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("checkstauts",response);
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status){
                                    JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    String approvedStatus = jsonObjectData.getString("admin_approve");
                                    String message = jsonObject.getString("message");

                                        if(approvedStatus.equals("Not Approved")){
                                            Toast.makeText(mContext,"Select Shop",Toast.LENGTH_SHORT).show();
                                        }
                                        else{
                                            Toast.makeText(mContext,message,Toast.LENGTH_SHORT).show();
                                            //Toast.makeText(mContext,message,Toast.LENGTH_SHORT).show();
                                           // chkApprovedDialog(message);
                                        }

                                        //change status as accept
                                        //acceptOrder(sharedPreferencesUtils.getShopID(), orderId);
                                        // selectTimeSlot();

                                       // calculateDeliveryTime2();
                                            Intent intent5 = new Intent(mContext,TimeSlotActivity.class);
                                            intent5.putExtra("total_amount",total_amount);
                                            intent5.putExtra("orderId",orderId);
                                            //intent5.putStringArrayListExtra("slot_array",Next3Hours);
                                                if(delivery_method.equals("Within next 3 Hrs")||delivery_method.equals("Within next 2 Hrs")){

                                                    intent5.putStringArrayListExtra("slot_array",Next3Hours);
                                                }else{
                                                    intent5.putStringArrayListExtra("slot_array", Slot_schedules);
                                                }
                                            startActivity(intent5);
//                                        if(slot.equalsIgnoreCase("one")) {
//                                            /*String[] timeslot_array = getResources().getStringArray(R.array.Time_slots);
//                                            selectTimeSlot2(timeslot_array,"ONE");*/
//                                            Intent intent1 = new Intent(mContext,TimeSlotActivity.class);
//                                            intent1.putExtra("total_amount",total_amount);
//                                            intent1.putExtra("orderId",orderId);
//                                            intent1.putStringArrayListExtra("slot_array",Slot_ONE);
//                                            startActivity(intent1);
//                                        }else if(slot.equalsIgnoreCase("two")){
//                                           /* String[] timeslot_array = getResources().getStringArray(R.array.Time_slots2);
//                                            selectTimeSlot2(timeslot_array,"TWO");*/
//                                            Intent intent2 = new Intent(mContext,TimeSlotActivity.class);
//                                            intent2.putExtra("total_amount",total_amount);
//                                            intent2.putExtra("orderId",orderId);
//                                            intent2.putStringArrayListExtra("slot_array",Slot_TWO);
//                                            startActivity(intent2);
//                                        }else if(slot.equalsIgnoreCase("three")){
//                                            /*String[] timeslot_array = getResources().getStringArray(R.array.Time_slots3);
//                                            selectTimeSlot2(timeslot_array,"THREE");*/
//                                            Intent intent3 = new Intent(mContext,TimeSlotActivity.class);
//                                            intent3.putExtra("total_amount",total_amount);
//                                            intent3.putExtra("orderId",orderId);
//                                            intent3.putStringArrayListExtra("slot_array",Slot_Three);
//                                            startActivity(intent3);
//                                        }else if(slot.equalsIgnoreCase("four")){
//                                            /*String[] timeslot_array = getResources().getStringArray(R.array.Time_slots4);
//                                            selectTimeSlot2(timeslot_array,"FOUR");*/
//                                            Intent intent4 = new Intent(mContext,TimeSlotActivity.class);
//                                            intent4.putExtra("total_amount",total_amount);
//                                            intent4.putExtra("orderId",orderId);
//                                            intent4.putStringArrayListExtra("slot_array",Slot_Four);
//                                            startActivity(intent4);
//                                        }else if(slot.equalsIgnoreCase("five")){
//                                            /*String[] timeslot_array = getResources().getStringArray(R.array.Time_slots4);
//                                            selectTimeSlot2(timeslot_array,"Next3Hours");*/
//                                            Intent intent5 = new Intent(mContext,TimeSlotActivity.class);
//                                            intent5.putExtra("total_amount",total_amount);
//                                            intent5.putExtra("orderId",orderId);
//                                            intent5.putStringArrayListExtra("slot_array",Next3Hours);
//                                            startActivity(intent5);
//                                        }
                                }else if(!status){
                                    Toast.makeText(mContext,"No Order Found Refresh Order List.",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestAcceptOrder);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    //show dialog order already accepted
    private void chkApprovedDialog(String message) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_chkapprove);

        //TextView text = dialog.findViewById(R.id.tv_dialog_msg);
        //text.setText(message);
        Button dg_btn_refreshlist =  dialog.findViewById(R.id.btn_refreshlist);

        dg_btn_refreshlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    //On your adapter create a variable:
    private View lastSelectedItem;
    //Define the folowing method:
    private void toggleBackgroundItem(View view) {
        if (lastSelectedItem != null) {
            lastSelectedItem.setBackgroundColor(Color.TRANSPARENT);
        }
        view.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        lastSelectedItem = view;

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
       /* if(sharedPreferencesUtils.getAdminType().equals("Delivery Boy")){
            super.onBackPressed();
            finish();
        }else {
            Intent intent = new Intent(mContext, OrderListActivity.class);
            intent.putExtra("flag", flag);
            intent.putExtra("areaName", areaName);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
              /*  if(sharedPreferencesUtils.getAdminType().equals("Delivery Boy")){
                    finish();
                }else {
                    Intent intent = new Intent(mContext, OrderListActivity.class);
                    intent.putExtra("flag", flag);
                    intent.putExtra("areaName", areaName);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intent);
                }*/
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private String CurrentTimeData() {
        //"action_time":"2019-06-19 1:11pm"
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        String formattedDate = dateFormat.format(new Date()).toString();
        System.out.println(formattedDate);
        Log.e("Current_DateTime", "" + formattedDate);
        return formattedDate;

    }

    private String ActionTimeData(String order_date) {
        //"action_time":"2019-06-19 1:11pm"
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy:MM:dd:HH:mm");
        //String formattedDate = dateFormat.format(new Date()).toString();
        String formattedDate = dateFormat.format("2019-06-19 1:11pm");
        System.out.println(formattedDate);
        Log.e("Current_DateTime", "" + formattedDate);
        return formattedDate;
    }

    private void DiffCalculate(String action_time) {

        try {
            Date userDob = new SimpleDateFormat("yyyy-MM-dd hh:mm").parse(action_time);
            //Date userDob = new SimpleDateFormat("yyyy-MM-dd hh:mm").parse("2019-06-22 10:11");
            Date today = new Date();
            long diff = today.getTime() - userDob.getTime();
            int numOfDays = (int) (diff / (1000 * 60 * 60 * 24));
            int hours = (int) (diff / (1000 * 60 * 60));
            int minutes = (int) (diff / (1000 * 60));
            int seconds = (int) (diff / (1000));

            Log.e("Diffrence is:", "" + numOfDays + "/" + hours + "/" + minutes + "/" + seconds);

            if (numOfDays > 1) {
                //btn_cancelorder.setVisibility(View.GONE);
                //Toast.makeText(mContext,"Exceed the Time in :" +numOfDays+"Days",Toast.LENGTH_LONG).show();

            } else {
                if (hours > 1) {
                    //  btn_cancelorder.setVisibility(View.GONE);
                    //  Toast.makeText(mContext,"Exceed the Time :"+hours+"Hours",Toast.LENGTH_LONG).show();
                } else {
                    if (minutes > 15) {
                        //    btn_cancelorder.setVisibility(View.GONE);
                        //    Toast.makeText(mContext,"Exceed the Time:"+minutes+"Minutes",Toast.LENGTH_LONG).show();
                    } else if (minutes < 15) {
                        //  btn_cancelorder.setVisibility(View.VISIBLE);
                        // Toast.makeText(mContext, "Yes you can Cancel the Order", Toast.LENGTH_LONG).show();
                    }
                }
            }

        } catch (Exception e) {
            e.getMessage();
        }



    }

    //check Rejection status
    //cancel order String assign_shop_id=shopId
    public void rejectionStatus(String shopId, String orderId) {
        if (Connectivity.isConnected(mContext)) {

            progressDialog.show();

            // String urlCancelOrder =  String.format(StaticUrl.cancelorder,orderId,shopId,assign_delboy_id,reason_of_rejection);

            String rejectorder = StaticUrl.checkintrestedbut
                    + "?order_id=" + orderId
                    //+ "?manager_id=" + sharedPreferencesUtils.getManagerId()
                    + "&assign_shop_id=" + shopId;

            Log.e("rejectURL",""+rejectorder);


            StringRequest stringRequestCancelOrder = new StringRequest(Request.Method.GET,
                    rejectorder,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.e("RejectResp:", "" + response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    btn_intrested_but.setVisibility(View.GONE);
                                } else if (!status) {
                                    btn_intrested_but.setVisibility(View.VISIBLE);
                                    //Toast.makeText(mContext, "Sorry. This order you can not Cancel Now.", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestCancelOrder);
        } else {
            Toast.makeText(mContext, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


    public String calculateDeliveryTime2(){

        String ordedate = order_date;
        Log.e("ordedate",""+delivery_time_get+" "+delivery_method);
        if(delivery_time_get!=null||delivery_method!=null) {
            //int  cmpvallue = Integer.parseInt(finalString);  /*	$time1='9 AM - 12 PM';
            //	$time2='12 PM - 3 PM';
            //	$time3='3 PM - 6 PM';
            //	$time4='6 PM - 9 PM';*/
            //String   cmpvallue = "3 PM - 6 PM";
            Log.e("delivery_time", delivery_time_get);
            //delivery_time":"11:21 AM - 02:21 PM"
            String cmpvallue = delivery_time_get;

            if (delivery_method.equals("Within next 3 Hrs")|| delivery_method.equals("Within next 2 Hrs")) {
                slot = "five";
                calculateNext3Hours(delivery_time_get);
            } else {
               /* if (cmpvallue.equalsIgnoreCase("9 AM - 12 PM")) {
                    Log.e("slot", "one");
                    slot = "one";
                } else if (cmpvallue.equalsIgnoreCase("12 PM - 3 PM")) {
                    Log.e("slot", "two");
                    slot = "two";
                } else if (cmpvallue.equalsIgnoreCase("3 PM - 6 PM")) {
                    Log.e("slot", "three");
                    slot = "three";
                } else if (cmpvallue.equalsIgnoreCase("6 PM - 9 PM")) {
                    Log.e("slot", "three");
                    slot = "four";
                }*/
                getTimeSlots(delivery_time_get);
            }
        }
        return slot;
    }
    private void getTimeSlots(final String slot) { //TODO Server method here
        if (Connectivity.isConnected(mContext)) {

            Slot_schedules = new ArrayList<>();

            JSONObject params = new JSONObject();
            try {
                params.put("slot", slot);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.newScheduleforShopkeeper, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("TimeslotRes:",""+response);

                    if (response.isNull("posts1")) {
                    } else {
                        try {
                            JSONArray categoryJsonArray = response.getJSONArray("posts1");
                            for (int i = 0; i < categoryJsonArray.length(); i++) {
                                JSONObject jsonCatData = categoryJsonArray.getJSONObject(i);
                                Slot_schedules.add(jsonCatData.getString("time"));

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            //VolleySingleton.getInstance(mContext).setPriority(Request.Priority.HIGH);
            VolleySingleton.getInstance(mContext).addToRequestQueue(request);
        }
    }
    private void calculateNext3Hours(String timeValue){
        Next3Hours.clear();
        String fslot = timeValue.substring(0,8);
        String to_time = timeValue.substring(timeValue.length()- 8);
        Log.e("ActualTime_slot:",""+fslot+"-"+to_time);
        String fromTime = fslot;
        String toTime = to_time;
        SimpleDateFormat df = new SimpleDateFormat("hh:mm a");
        try {
            Date dStart = df.parse(fromTime);
            Date dEnd = df.parse(toTime);
            System.out.println(dStart);
            System.out.println(dEnd);
            Calendar calendarStart = Calendar.getInstance();
            calendarStart.setTime(dStart);
            while(calendarStart.getTime().before(dEnd)){

                String v1 =df.format(calendarStart.getTime());
                calendarStart.add(Calendar.MINUTE, 60);
                String v2 = df.format(calendarStart.getTime());
                Log.e("Slot_AMPM:",""+v1+" - "+v2);
                Next3Hours.add(v1+" - "+v2);
            }
        }catch (Exception E){

        }
    }

    private void Slot_Init(){
        Slot_ONE = new ArrayList<>();
        Slot_ONE.add("9 AM - 10 AM");
        Slot_ONE.add("10 AM - 11 AM");
        Slot_ONE.add("11 AM - 12 Noon");

        Slot_TWO = new ArrayList<>();
        Slot_TWO.add("12 Noon - 1 PM");
        Slot_TWO.add("1 PM - 2 PM");
        Slot_TWO.add("2 PM - 3 PM");

        Slot_Three = new ArrayList<>();
        Slot_Three.add("3 PM - 4 PM");
        Slot_Three.add("4 PM - 5 PM");
        Slot_Three.add("5 PM - 6 PM");

        Slot_Four = new ArrayList<>();
        Slot_Four.add("6 PM - 7 PM");
        Slot_Four.add("7 PM - 8 PM");
        Slot_Four.add("8 PM - 9 PM");
    }

    // Get User Name of Order id
    public void getUserName(){
        if(Connectivity.isConnected(mContext)){

            String urlOtpsend = StaticUrl.getUserName
                    +"?userid="+userId;

            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlOtpsend,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {

                                Log.e("Res_User_shop",""+response);

                                JSONObject jsonObject = new JSONObject(response);
                                boolean statuss = jsonObject.getBoolean("status");
                               String  ShopName = jsonObject.getString("shopname");
                                tv_last_shop.setText("Last Shopkeeper:"+ShopName);
                                if(statuss){

                                    JSONObject jsonObjectData =  jsonObject.getJSONObject("data");

                                    user_fname = jsonObjectData.getString("fname");
                                    Log.e("contactno","userContact:"+jsonObjectData.getString("contactno"));
                                    userContact = jsonObjectData.getString("contactno");
                                    tv_userName.setVisibility(View.VISIBLE);
                                    tv_userName.setText("Name: "+user_fname+" Contact No:"+jsonObjectData.getString("contactno"));
                                    Log.e("Orde_User_Data",""+jsonObjectData);

                                }else if(!statuss) {

                                    Toast.makeText(mContext,"No User Found..!!",Toast.LENGTH_LONG).show();

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    // Get User Name of Order id
    public void getViewShopList(){
        if(Connectivity.isConnected(mContext)){
            String urlOtpsend = StaticUrl.getViewbyshop
                    +"?order_id="+orderId;

            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlOtpsend,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {

                                Log.e("Res_User_shop",""+response);

                                JSONObject jsonObject = new JSONObject(response);
                                boolean statuss = jsonObject.getBoolean("status");

                                if(statuss){

                                    JSONObject jsonObjectData =  jsonObject.getJSONObject("data");

                                    //user_fname = jsonObjectData.getString("fname");
                                    //Log.e("contactno","userContact:"+jsonObjectData.getString("contactno"));

                                    for(int i=0; i<jsonObjectData.length();i++) { //Must have to Check Array on Post man

                                        ShopModel shopModel = new ShopModel();

                                        shopModel.setShop_id(jsonObjectData.getString("shop_id"));
                                        shopModel.setView_time(jsonObjectData.getString("view_at"));
                                        shopModel.setView_status(jsonObjectData.getString("admin_aprove_status"));

                                        shopArrayList.add(shopModel);
                                    }

                                    Log.e("Order_shop_Data",""+jsonObjectData);

                                }else if(!statuss) {

                                    Toast.makeText(mContext,"No User Found..!!",Toast.LENGTH_LONG).show();

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    //Method to show the View by ShopList Details
    public void shopviewListDialog() {

        final Dialog dialog = new Dialog(this,android.R.style.Theme_Light);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        View view = getLayoutInflater().inflate(R.layout.dialog_shop_view_list, null);

        Button btn_submitreson = view.findViewById(R.id.btn_close);
        final RecyclerView cart_list = (RecyclerView) view.findViewById(R.id.rv_cart_list);
        cart_list.setHasFixedSize(true);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        cart_list.setLayoutManager(layoutManager);
        /* cart_list.setItemAnimator(new DefaultItemAnimator());*/
        cart_list.setVisibility(View.VISIBLE);
        // getOrderCartList();

        Log.e("CartList Size:",""+shopArrayList.size());
        final ShopViewDetailsAdaptor shopViewDetailsAdaptor = new ShopViewDetailsAdaptor(shopArrayList, OrderDetailActivity.this);
        cart_list.setAdapter(shopViewDetailsAdaptor);

        cart_list.addOnItemTouchListener(new RecyclerTouchListener(mContext, cart_list,
                new RecyclerTouchListener.ClickListener()
                {
                    @Override
                    public void onClick(View view, int position)
                    {

                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {
                        // Toast.makeText(mContext,"No Records",Toast.LENGTH_SHORT).show();
                    }
                }));



        btn_submitreson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if (reason_of_rejection==null||reason_of_rejection.isEmpty()||reason_of_rejection.equals("")) {
                    Toast.makeText(mContext, "Select Reason", Toast.LENGTH_SHORT).show();
                } else {*/
                    dialog.dismiss();

                //}
            }
        });
        dialog.setContentView(view);

        dialog.show();
    }

    ///RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final RecyclerTouchListener.ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {

                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }

    // Send Whatsapp Message to User
    private void openWhatsApp(String mobileNo) {

        try {
            String sms_text = "Dear All, \n" +
                    "\n" +
                    "Thanks for your continuous orders and patience.\n" +
                    "Those who were using us before Corona about the kind of services we maintain.But it's too tough right now for everybody !\n" +
                    "\n" +
                    "PICODEL App is continuously trying to serve as many orders as possible in this situation as well.\n" +
                    "\n" +
                    "Many of you already got and many other's order are in process.\n" +
                    "\n" +
                    "The biggest challenge is that on one side govt is assuring the supply of all essential without restriction, whereas bitter truth is that Supply chain has completely broken.\n" +
                    "Why ?\n" +
                    "1.Most of labour/employees in supply chain has gone to Home Town, hence there is huge scarcity of labour.And all remaining labour cost has become very high to motivate them to work in this situation.\n" +
                    "2.Manufacturing has severly impacted due to labour and raw material for them.So it will take time to refill if stock gets exhausted.\n" +
                    "3.Farmers are facing big losses due to problem in reaching to different mandies due to limited options and high cost and different locked down.\n" +
                    "4.Police starts beating before asking the reason, hence all Delivery boys are avoiding to come for deliveries.\n" +
                    "5.Shops are over crowded and stocks are not available easily.\n" +
                    "\n" +
                    "We are continuously trying to increase our team to help all of you asap.Pls bear with us.\n" +
                    "\n" +
                    "Pls give the order on PICODEL for Grocery and Vegetables and let it be pending till you do not get alternate arrangements, so that we will also keep trying our best to serve you.\uD83D\uDE4F\n" +
                    "\n" +
                    "Many of you are feeling that Customers are exploited at this moment.\n" +
                    "\n" +
                    "Pls note, that PICODEL is not charging anything at the moment and every single rupee is going to Shopkeeper/ Delivery boy to cover their additional costs/get them better margin to serve you in critical condition and to keep you safe ! \uD83D\uDE4F\n" +
                    "\n" +
                    "Stay Safe and take care of families !!!\n" +
                    "\n" +
                    "Thanks & Regards\n" +
                    "Team@\n" +
                    "PICODEL";// Replace with your message.

            String toNumber = mobileNo; // Replace with mobile phone number without +Sign or leading zeros, but with country code
            //Suppose your country is India and your phone number is “xxxxxxxxxx”, then you need to send “91xxxxxxxxxx”.


            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("http://api.whatsapp.com/send?phone="+91+toNumber+"&text="+sms_text));
            startActivity(intent);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }






}
