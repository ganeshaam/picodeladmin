package com.example.picodeladmin;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import dmax.dialog.SpotsDialog;

public class LoginActivity extends AppCompatActivity {

    Context mContext;
    EditText et_mobileNumber;
    Button btn_login;
    Spinner sp_getCity;
    AlertDialog progressDialog;
    boolean flag = false;
    String shopadmin_id,admin_type,sellername,shop_id,email,sellerstatus,moibleno,fc_city,admin_id;
    private final ArrayList<String> cityList = new ArrayList<>();
    SharedPreferencesUtils sharedPreferencesUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mContext = LoginActivity.this;
        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();

        mContext = LoginActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);

        et_mobileNumber = findViewById(R.id.et_mobileNumber);
        btn_login = findViewById(R.id.btn_login);
        sp_getCity = findViewById(R.id.sp_getCity);

        et_mobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 10) {
                    verifyMobileAdmin(et_mobileNumber.getText().toString());
                }
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(et_mobileNumber.getText().toString().equals("")){
                    Toast.makeText(mContext,"Enter Mobile Number",Toast.LENGTH_SHORT).show();
                }if(sp_getCity.getSelectedItem().toString().isEmpty()){
                    Toast.makeText(mContext,"Please Select the Admin City",Toast.LENGTH_SHORT).show();
                }else {
                    sharedPreferencesUtils.setAdminCity(sp_getCity.getSelectedItem().toString());
                    sendOtp();
                }
            }
        });

        getCityList();
    }

    public void verifyMobileAdmin(String mobileNumber){
         if(Connectivity.isConnected(mContext)){
             progressDialog.show();
             //String targetUrl = StaticUrl.getloginadmin+"?contact="+mobileNumber;
             String targetUrl = StaticUrl.getloginadmin+"?contact="+mobileNumber+"&admin_city="+sp_getCity.getSelectedItem().toString();

             Log.e("Admin_login",""+targetUrl);

             StringRequest requestLogin = new StringRequest(Request.Method.GET,
                     targetUrl,
                     new Response.Listener<String>() {
                         @Override
                         public void onResponse(String response) {
                             progressDialog.dismiss();
                             try {
                                 JSONObject jsonObject = new JSONObject(response);
                                 Log.d("adminLogin",response);
                                 boolean statuss = jsonObject.getBoolean("status");
                                 if(statuss){
                                     flag=true;
                                      hideSoftKeyBoard();
                                     JSONArray jsonArray = jsonObject.getJSONArray("data");
                                     //JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                     for (int i=0; i<jsonArray.length();i++) {
                                         shopadmin_id = jsonArray.getJSONObject(i).getString("shopadmin_id");
                                         sellername = jsonArray.getJSONObject(i).getString("sellername");
                                         shop_id = jsonArray.getJSONObject(i).getString("shop_id");
                                         admin_type = jsonArray.getJSONObject(i).getString("admin_type");
                                         email = jsonArray.getJSONObject(i).getString("email");
                                         sellerstatus = jsonArray.getJSONObject(i).getString("sellerstatus");
                                         moibleno = jsonArray.getJSONObject(i).getString("contact");
                                         fc_city = jsonArray.getJSONObject(i).getString("fc_city");
                                        // admin_id = jsonArray.getJSONObject(i).getString("admin_id");
                                         }
                                    }else if(!statuss) {
                                     flag=false;
                                       getError("\n\t Mobile Number is Not Registered \n \t with PICODEL Admin.");
                                 }
                             } catch (JSONException e) {
                                 e.printStackTrace();
                             }
                         }
                     },
                     new Response.ErrorListener() {
                         @Override
                         public void onErrorResponse(VolleyError error) {
                             progressDialog.dismiss();
                         }
                     });
             MySingleton.getInstance(mContext).addToRequestQueue(requestLogin);
         }else {
             Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
         }
    }
    private void getError (String Error){
        androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(LoginActivity.this);
        alertDialogBuilder.setMessage(Error);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });
        androidx.appcompat.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
    private void hideSoftKeyBoard() {
        try {
            // hides the soft keyboard when the drawer opens
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //send otp
    public void sendOtp(){
        if(Connectivity.isConnected(mContext)){
            progressDialog.show();
            String urlOtpsend = StaticUrl.sendotp
                    + "?mobileno=" + et_mobileNumber.getText().toString()
                    + "&email=" + email;

            Log.d("urlOtpsend",urlOtpsend);

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlOtpsend,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("sendOtp",response);
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean statuss = jsonObject.getBoolean("status");
                                if(statuss)
                                {
                                    JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    String vcode = jsonObjectData.getString("vcode");
                                    String email = jsonObjectData.getString("email");
                                    String contact = jsonObjectData.getString("contact");
                                    String vdate = jsonObjectData.getString("vdate");
                                    Log.e("VCODE:",""+vcode+admin_type);

                                    Intent intent = new Intent(mContext,OtpVerifyActivity.class);
                                    intent.putExtra("vcode",vcode);
                                    intent.putExtra("email",email);
                                    intent.putExtra("contact",contact);
                                    intent.putExtra("vdate",vdate);
                                    intent.putExtra("shop_id",shop_id);
                                    intent.putExtra("shopadmin_id",shopadmin_id);
                                    intent.putExtra("sellername",sellername);
                                    intent.putExtra("email",email);
                                    intent.putExtra("admin_type",admin_type);
                                    intent.putExtra("shop_status",sellerstatus);
                                    intent.putExtra("contact",moibleno);
                                    intent.putExtra("fc_city",fc_city);
                                    //intent.putExtra("admin_id",admin_id);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    //finish();
                                }else {
                                    Toast.makeText(mContext,"Invalid Mobile Number",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    private void getCityList() { //TODO Server method here
        if (Connectivity.isConnected(LoginActivity.this)) {
            cityList.clear();

            /*JSONObject params = new JSONObject();
            try {
                params.put("contact", gateWay.getContact());
                params.put("v_city", sp_city_spinner.getSelectedItem().toString()); //sp_city_value
                params.put("v_state", spinner.getSelectedItem().toString());
                params.put("type", "Android");//true or false
                Log.e("userParm:",""+params);
            } catch (JSONException e) {
                e.printStackTrace();
            }*/

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.activeCityForAdmin, null, new Response.Listener<JSONObject>() {

                @SuppressWarnings("CanBeFinal")
                @Override
                public void onResponse(JSONObject response) {
                    //  if (!response.isNull("posts")) {
                    try {



                        JSONArray cityJsonArray = response.getJSONArray("posts");
                        for (int i = 0; i < cityJsonArray.length(); i++) {
                            JSONObject jsonCityData = cityJsonArray.getJSONObject(i);
                            cityList.add(jsonCityData.getString("city"));
                        }
                        /*CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(LocationActivity.this, areaList, null, null, "location");
                        sp_Area_spinner.setAdapter(customSpinnerAdapter);*/

                        ArrayAdapter<String> adapter = new ArrayAdapter(LoginActivity.this,android.R.layout.simple_spinner_item,cityList);
                        sp_getCity.setAdapter(adapter);

                        //share preparence add city


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //gateWay.progressDialogStop();

                }
                //}
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                     //TODO ServerError method here
                }
            });
            // AppController.getInstance().addToRequestQueue(request);
            VolleySingleton.getInstance(LoginActivity.this).addToRequestQueue(request);
        }
    }
}
