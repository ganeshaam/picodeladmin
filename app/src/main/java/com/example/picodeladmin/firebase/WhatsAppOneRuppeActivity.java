package com.example.picodeladmin.firebase;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.picodeladmin.R;
import com.example.picodeladmin.ReferralPointsAdaptor;
import com.example.picodeladmin.SharedPreferencesUtils;
import com.example.picodeladmin.UserModel;
import com.example.picodeladmin.VolleySingleton;
import com.example.picodeladmin.WhatAppActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import dmax.dialog.SpotsDialog;

public class WhatsAppOneRuppeActivity extends AppCompatActivity {

    TextView tv_refercode;
    Button bt_share,bt_receive,bt_setmessage,bt_page3,bt_page4,bt_search;
    SharedPreferencesUtils sharedPreferencesUtils;
    android.app.AlertDialog progressDialog2;
    ArrayList<UserModel> arrayList;
    ArrayList<UserModel> arrayListFinal;
    ArrayList<UserModel> arrayList1;
    ArrayList<UserModel> arrayList2;
    ArrayList<UserModel> arrayList3;
    ArrayList<UserModel> arrayList4;
    ArrayList<UserModel> arrayList5;
    ArrayList<UserModel> arrayList6;
    RecyclerView rv_referral_points;
    ReferralPointsAdaptor referralPointsAdaptor;
    ArrayList<UserModel> ReferPointsList ;
    LinearLayoutManager layoutManager;
    //String sms_text ="",start ="100",end="500";
    String sms_text ="",start ="5500",end="6000";
    String temp_sms_text ="Glad to share the link to get 50/- off on your 1st order of Groceries and Vegetables from PICODEL!\nIt helps to get delivery of the order from 60 minutes to Max 2 hours...\nDownload PICODEL App and enjoy lot more unique features !";
    EditText et_message;
    SearchView et_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_whatsapp);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("User Traking");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        progressDialog2 = new SpotsDialog.Builder().setContext(this).build();
        sharedPreferencesUtils = new SharedPreferencesUtils(WhatsAppOneRuppeActivity.this);


        arrayList = new ArrayList<>();
        arrayListFinal = new ArrayList<>();
        arrayList1 = new ArrayList<>();
        arrayList2 = new ArrayList<>();
        arrayList3 = new ArrayList<>();
        arrayList4 = new ArrayList<>();
        arrayList5 = new ArrayList<>();
        arrayList6 = new ArrayList<>();

        tv_refercode = findViewById(R.id.tv_refercode);
        bt_share = findViewById(R.id.bt_next);
        bt_receive = findViewById(R.id.bt_page2);
        bt_page3 = findViewById(R.id.bt_page3);
        bt_page4 = findViewById(R.id.bt_page4);
        bt_search = findViewById(R.id.bt_search);
        bt_setmessage = findViewById(R.id.bt_setmessage);
        et_message = findViewById(R.id.et_message);
        et_name = findViewById(R.id.et_name);
        rv_referral_points = findViewById(R.id.rv_referral_points);
        rv_referral_points.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(this,1);
        rv_referral_points.setLayoutManager(layoutManager);
        // rv_referral_points.setItemAnimator(new DefaultItemAnimator());
        arrayList = new ArrayList<>();
        referralPointsAdaptor = new ReferralPointsAdaptor(arrayListFinal,this,"user");
        rv_referral_points.setAdapter(referralPointsAdaptor);


        //et_message.setText(temp_sms_text);

        bt_setmessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sms_text = et_message.getText().toString();
                et_message.setText(sms_text);
                //Log.e("name",et_name.getQuery().toString());
            }
        });



        bt_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                start = end;
                int temp = Integer.parseInt(end);
                int temp2 = temp+200;
                end = String.valueOf(temp2);
                getAllUsers(start, String.valueOf(temp2),"false");
                Log.e("start_end:",""+start+":"+end);

            }
        });



        bt_receive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                end = start;
                int temp = Integer.parseInt(start);
                int temp2 = temp-200;
                String start1 = String.valueOf(temp2);
                start = start1;
                getAllUsers(start,end,"false");
                Log.e("start_end_p:",""+start+":"+end);
            }
        });
        getAllUsers(start,end,"false");
        bt_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAllUsers(start,end,"true");
                hideKeyBoard();
            }
        });

        Log.e("name",et_name.getQuery().toString());

        bt_page3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                referralPointsAdaptor = new ReferralPointsAdaptor(arrayList3,WhatsAppOneRuppeActivity.this,"user");
                rv_referral_points.setAdapter(referralPointsAdaptor);
                referralPointsAdaptor.notifyDataSetChanged();
            }
        });


        bt_page4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                referralPointsAdaptor = new ReferralPointsAdaptor(arrayList4,WhatsAppOneRuppeActivity.this,"user");
                rv_referral_points.setAdapter(referralPointsAdaptor);
                referralPointsAdaptor.notifyDataSetChanged();

            }
        });

        rv_referral_points.addOnItemTouchListener(new RecyclerTouchListener(WhatsAppOneRuppeActivity.this, rv_referral_points,
                new RecyclerTouchListener.ClickListener()
                {
                    @Override
                    public void onClick(View view, int position)
                    {


                        String contact = arrayListFinal.get(position).getContact();
                        //openWhatsApp(contact);
                        Toast.makeText(WhatsAppOneRuppeActivity.this,"Clicked"+contact , Toast.LENGTH_SHORT).show();
                        updateUserStatus(start,end,contact);
                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {
                        // Toast.makeText(mContext,"No Records",Toast.LENGTH_SHORT).show();
                    }
                }));

        if (et_name != null) {
            et_name.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {

                    //hideKeyBoard();
                    return true;
                }

                @Override
                public boolean onQueryTextChange(final String newText) {
                    try {

                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                    return true;
                }
            });
        }



    }

    public void sendReferral(Context context) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, getInvitationMessage());
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
        sendIntent.setType("text/plain");
        context.startActivity(Intent.createChooser(sendIntent, "PICODEL REFARAL"));
    }

    private String getInvitationMessage(){
        String playStoreLink = "https://play.google.com/store/apps/details?id=com.podtest.referdemo&referrer=";
        return playStoreLink + "PICODEL111";
    }


    private void openWhatsApp(String mobileNo) {
        /*String smsNumber = mobileno;//"7****"; // E164 format without '+' sign
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("text/plain");
        sendIntent.putExtra(Intent.EXTRA_TEXT, "PICODEL Admin.");
        sendIntent.putExtra("jid", smsNumber + "@s.whatsapp.net"); //phone number without "+" prefix
        sendIntent.setPackage("com.whatsapp");
        if (sendIntent.resolveActivity(getPackageManager()) == null) {
            //Toast.makeText(this, "Error/n" + e.toString(), Toast.LENGTH_SHORT).show();
            return;
        }*/

//        Intent sendIntent = new Intent("android.intent.action.MAIN");
//        sendIntent.setComponent(new ComponentName("com.whatsapp","com.whatsapp.Conversation"));
//        sendIntent.putExtra("Hi", PhoneNumberUtils.stripSeparators(mobileNo)+"@s.whatsapp.net");
//        startActivity(sendIntent);
        try {
            //String text = "Glad to share the link to get 50/- off on your 1st order of Groceries and Vegetables from PICODEL!\nIt helps to get delivery of the order from 60 minutes to Max 3 hours...\nDownload PICODEL App and enjoy lot more unique features !";// Replace with your message.

            String toNumber = mobileNo; // Replace with mobile phone number without +Sign or leading zeros, but with country code
            //Suppose your country is India and your phone number is “xxxxxxxxxx”, then you need to send “91xxxxxxxxxx”.


            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("http://api.whatsapp.com/send?phone="+91+toNumber+"&text="+sms_text));
            startActivity(intent);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private void getAllUsers(String start, String end, String flag) { //TODO Server method here

        progressDialog2.show();
        arrayListFinal.clear();
        arrayList.clear();
        arrayList1.clear();
        arrayList2.clear();
        arrayList3.clear();
        arrayList4.clear();
        JSONObject params = new JSONObject();
        try {
            params.put("contact", "7276201058");
            params.put("user_id", "4063");
            params.put("name", et_name.getQuery().toString());
            params.put("start", start);
            params.put("end", end);
            params.put("flag", "onerupee");//true or false
            params.put("type", "user");//true or false
            Log.e("userParm:",""+params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String url ="https://www.picodel.com/And/shopping/AppAPI/getAllUserData.php";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {

            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(JSONObject response) {

                Log.e("ReferInsertRes", "" + response.toString());
                progressDialog2.dismiss();
                if (response.isNull("posts")) {

                    progressDialog2.dismiss();
                }else{

                    try {
                        JSONArray postsJsonArray = response.getJSONArray("posts");

                        Log.e("user_RS:",""+postsJsonArray.toString());

                        for (int i = 0; i < postsJsonArray.length(); i++) {
                            JSONObject jSonClassificationData = postsJsonArray.getJSONObject(i);

                            UserModel userModel = new UserModel();
                            userModel.setName(jSonClassificationData.getString("fname"));
                            userModel.setLname(jSonClassificationData.getString("lname"));
                            userModel.setContact(jSonClassificationData.getString("contact"));//contact
                            userModel.setId(jSonClassificationData.getString("user_id"));//user_id
                            userModel.setRedeem_points(jSonClassificationData.getString("redeem_points"));
                            userModel.setDate(jSonClassificationData.getString("date"));
                            userModel.setLastlogin_date(jSonClassificationData.getString("lastlogin_date"));
                            userModel.setStatus(jSonClassificationData.getString("status"));

                            arrayListFinal.add(userModel);


                        }


                        referralPointsAdaptor.notifyDataSetChanged();
                    }catch (Exception e){

                    }



                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog2.dismiss();

            }
        });
        VolleySingleton.getInstance(WhatsAppOneRuppeActivity.this).addToRequestQueue(request);

    }

    private void updateUserStatus(String start2, String end2, String contact) { //TODO Server method here

        progressDialog2.show();
        arrayList.clear();
        arrayList1.clear();
        arrayList2.clear();
        arrayList3.clear();
        arrayList4.clear();
        JSONObject params = new JSONObject();
        try {
            params.put("contact", "7276201058");
            params.put("user_id", "4063");
            params.put("name", contact);
            params.put("start", start);
            params.put("end", end);
            params.put("flag", "true");//true or false
            params.put("type", "update");//true or false
            Log.e("userParm:",""+params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String url ="https://www.picodel.com/And/shopping/AppAPI/getAllUserData.php";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {

            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(JSONObject response) {

                Log.e("ReferInsertRes", "" + response.toString());
                progressDialog2.dismiss();
                if (response.isNull("posts")) {

                    progressDialog2.dismiss();
                }else{

                    try {
                        JSONArray postsJsonArray = response.getJSONArray("posts");

                        Log.e("user_RS:",""+postsJsonArray.toString());

                        String user_update = response.getString("user_update");

                        //if(user_update.equalsIgnoreCase("true")){
                        getAllUsers("5500","6000","false");
                        Toast.makeText(WhatsAppOneRuppeActivity.this,"Clicked" , Toast.LENGTH_SHORT).show();
                        //}


                        referralPointsAdaptor.notifyDataSetChanged();
                    }catch (Exception e){

                    }



                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog2.dismiss();

            }
        });
        VolleySingleton.getInstance(WhatsAppOneRuppeActivity.this).addToRequestQueue(request);

    }

    ///RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final RecyclerTouchListener.ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {

                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }

    private void hideKeyBoard() {
        View view = getCurrentFocus();  // Check if no view has focus:
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
