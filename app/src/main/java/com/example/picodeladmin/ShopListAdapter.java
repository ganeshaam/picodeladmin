package com.example.picodeladmin;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class ShopListAdapter extends RecyclerView.Adapter<ShopListAdapter.SoplistHolder>{

    ArrayList<ShopModel> shopModels;
    Context mContext;

    public ShopListAdapter(ArrayList<ShopModel> shopModels, Context mContext) {
        this.shopModels = shopModels;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ShopListAdapter.SoplistHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_shoplist,parent,false);
        ShopListAdapter.SoplistHolder soplistHolder = new ShopListAdapter.SoplistHolder(view);
        return soplistHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ShopListAdapter.SoplistHolder soplistHolder, int position) {
        TextView tx_shopname = soplistHolder.tx_shopname;
        TextView tx_sellername = soplistHolder.tx_sellername;
        TextView tx_contact = soplistHolder.tx_contact;
        TextView tx_email = soplistHolder.tx_email;
        TextView tx_area = soplistHolder.tx_area;
        TextView tx_shopcategory = soplistHolder.tx_shopcategory;
        TextView tx_admintype = soplistHolder.tx_admintype;
        TextView tx_prefareakm = soplistHolder.tx_prefareakm;
        LinearLayout ll_layout = soplistHolder.ll_shopList;

       // Log.d("shopname",shopModels.get(position).getShop_name());
        tx_shopname.setText(shopModels.get(position).getShop_name());
        tx_sellername.setText(shopModels.get(position).getSellername());
        tx_contact.setText(shopModels.get(position).getContact());
        tx_email.setText(shopModels.get(position).getEmail());
        tx_area.setText("Area: "+shopModels.get(position).getArea());
        tx_shopcategory.setText("Shop Category: "+shopModels.get(position).getShop_category());
        tx_admintype.setText("Shop Type: "+shopModels.get(position).getAdmin_type()+" Points:  "+shopModels.get(position).getRecharge_points());
        tx_prefareakm.setText("preferred Delivery Area km: "+shopModels.get(position).getPreferredDeliveryAreaKm());
        int value_points = Integer.parseInt(shopModels.get(position).getRecharge_points());
         /*if(value_points<0){
             ll_layout.setBackgroundColor(Color.parseColor("#FF9800"));
         }else {
             ll_layout.setBackgroundColor(Color.parseColor("#FFFFFF"));
         }*/

      /*  if(shopModels.get(position).getShop_name().isEmpty() && shopModels.get(position).getShop_name().equals("") && shopModels.get(position).getShop_name().equals("null")) {
            tx_shopname.setText("");
        }else {
            tx_shopname.setText(shopModels.get(position).getShop_name());
        }

        if(shopModels.get(position).getSellername().isEmpty() && shopModels.get(position).getSellername().equals("")) {
            tx_sellername.setText("");
        }else {
            tx_sellername.setText(shopModels.get(position).getSellername());
        }

        if(shopModels.get(position).getContact().isEmpty() && shopModels.get(position).getContact().equals("")) {
            tx_contact.setText("");
        }else {
            tx_contact.setText(shopModels.get(position).getContact());
        }

        if(shopModels.get(position).getEmail().isEmpty() && shopModels.get(position).getEmail().equals("")) {
            tx_email.setText("");
        }else {
            tx_email.setText(shopModels.get(position).getEmail());
        }

        if(shopModels.get(position).getArea().isEmpty() && shopModels.get(position).getArea().equals("")) {
            tx_area.setText("");
        }else {
            tx_area.setText(shopModels.get(position).getArea());
        }*/
    }

    @Override
    public int getItemCount() {
        return shopModels.size();
    }

    public static class SoplistHolder extends RecyclerView.ViewHolder {

        TextView tx_shopname,tx_sellername,tx_contact,tx_email,tx_area,tx_shopcategory,tx_admintype,tx_prefareakm;
        //ImageView iv_location;
        LinearLayout ll_shopList;

        public SoplistHolder(@NonNull View itemView) {
            super(itemView);
            tx_shopname = itemView.findViewById(R.id.tx_shopname);
            tx_sellername = itemView.findViewById(R.id.tx_sellername);
            tx_contact = itemView.findViewById(R.id.tx_contact);
            tx_email = itemView.findViewById(R.id.tx_email);
            //iv_location = itemView.findViewById(R.id.iv_location);
            tx_area = itemView.findViewById(R.id.tx_area);
            tx_shopcategory = itemView.findViewById(R.id.tx_shopcategory);
            tx_admintype = itemView.findViewById(R.id.tx_admintype);
            tx_prefareakm= itemView.findViewById(R.id.tx_prefareakm);
            ll_shopList = itemView.findViewById(R.id.ll_shopList);
        }
    }
}
