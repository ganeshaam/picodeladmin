package com.example.picodeladmin;

public class StaticUrl {

    //public static String baseUrl = "https://www.picodel.com/seller";
    public static String baseUrl2 = "https://www.picodel.com/seller/shoptestapi/";
    public static String baseUrl3 = "https://www.picodel.com/seller/supperadmin/";
    public static String baseUrl = "https://www.picodel.com/seller/shopapi/";
    public static String SHOPPING_INITIAL_URL1 = "https://www.picodel.com/And/shopping/RESTAPI/";
    public static String SHOPPING_INITIAL_URL = "https://www.picodel.com/And/shopping/AppAPI/";
    public static String SHOPPING_INITIAL_URL2 = "https://www.picodel.com/And/shopping/";

    public static String versionName = BuildConfig.VERSION_NAME;

    public static String IMAGE_URL = "https://simg.picodel.com/";

    public static String verifyMobile = baseUrl+"checkmono";
    public static String verifydeliveryBoyMobile = baseUrl+"checkdeliveryboynumber";
    public static String verifymanagerMobile = baseUrl+"checkmanagernumber";
    public static String getlatlong = baseUrl+"getlatlong";
    public static String sendotp = baseUrl+"sendotp";
    //public static String orderlist = baseUrl+"getorders"; Live previous
    //public static String orderlist = baseUrl2+"getorders"; Live in Admin
    public static String orderlist = baseUrl2+"getordersadmin"; // for the City wise List
    //public static String orderlist = baseUrl2+"getordersadminp"; //Live in Admin version 1.16.4
    public static String getorderbyid = baseUrl2+"getorderbyid";
    public static String cartlist = baseUrl2+"cartitem";  // Live  with Area wise Price Changes

    public static String getUseraddress = baseUrl+"getaddress";
    public static String shopregistration= baseUrl+"createshop";
    public static String shopadminref= baseUrl+"shopadminref";
    public static String shop_address_update = baseUrl+"addressupdate";
    public static String shop_profile_address_update = baseUrl+"profileupdate";
    public static String acceptorder = baseUrl+"acceptorderadmin";
    public static String chkorderstatus  = baseUrl+"chkorderstatus";
    public static String cancelorder = baseUrl+"cancelorder";
    public static String checkintrestedbut = baseUrl+"checkintrestedbut";
    //public static String cancelorder = baseUrl+"/shoptestapi/cancelorder?order_id=%s&assign_shop_id=%s&assign_delboy_id=%s&reason_of_rejection=%s";
    public static String deliverdorder =  baseUrl+"deliverorder?order_id=order_id";
    public static String adddeliveryboy = baseUrl+"adddeliveryboy";
    public static String addShopmanager = baseUrl+"shopmanager";
    public static String getDeliveryboys = baseUrl+"getdeliveryboy";
    public static String getmanagers = baseUrl+"getmanagers";
    public static String checkmanager = baseUrl+"checkmanager";
    public static String assignorder = baseUrl+"assignorder";
    public static String getPaymentMode = baseUrl+"paymentmode";
    public static String setdeliverstatus = baseUrl+"deliverstatus";
    //public static String assignorder = baseUrl+"/shoptestapi/assignorder";
    public static String getDeliveryboy = baseUrl+"deliveryboy";
    public static String histroycount  = baseUrl+"histroycount";
    public static String deliveryreports = baseUrl+"deliveryreports";
    public static String rejectionreports = baseUrl+"rejectionreports";
    public static String checkstatus = baseUrl+"checkstatus";
    //public static String freelancerArea = baseUrl+"freelancerarea";
    //public static String freelancerArea = baseUrl2+"adminarealist"; Current Live 1.16.2
    public static String freelancerArea = baseUrl2+"adminarealistcategory"; //Live in admin App
    public static String updatetoken = baseUrl+"updatetoken";

    //public static String registration = baseUrl+"create?area=%s&nearest_areas=%s";

    //start------ Wishlist
    public static String urlGetMyWishListItems = SHOPPING_INITIAL_URL1 + "wishListProducts.php";
    public static String urlAddMyWishListItems = SHOPPING_INITIAL_URL + "AddWishListDetails.php";
    public static String urlDeleteMyWishListItems = SHOPPING_INITIAL_URL + "deleteWishListItems.php";
    public static String urlGetWishListItem = SHOPPING_INITIAL_URL + "fetchWishlistID.php";//sync with local
    public static String urlAddCartItemToWishlist = SHOPPING_INITIAL_URL + "addCarttoWishlist.php";//sync with local

    //start----- commonly used urls
    public static String urlGetuserdata = SHOPPING_INITIAL_URL + "PkfetchUserDataWithAddress.php"; //fetchUserDataWithAddress
    public static String urlSimilarResult = SHOPPING_INITIAL_URL + "similarProductResult.php";
    public static String urlReviewResult = SHOPPING_INITIAL_URL + "rating_review.php";
    public static String urlReviewAll = SHOPPING_INITIAL_URL + "picodel_review.php";

    //start----- FilterLayout
    public static String urlGetDependentBrand = SHOPPING_INITIAL_URL + "getDependentBrandData.php";
    public static String urlGetDependentSize = SHOPPING_INITIAL_URL + "getDependentSizeData.php";
    //end----- FilterLayout
    //start----- SearchProducts
    public static String urlNewGetProducts = SHOPPING_INITIAL_URL + "searchProducts.php";
    public static String urlGetAllFilterData = SHOPPING_INITIAL_URL + "get_all_filter.php";
    public static String urlGetSubCategories = SHOPPING_INITIAL_URL + "new_getSubCategories.php";
    public static String urlGetCategories = SHOPPING_INITIAL_URL + "getCategories.php";
    public static String urlCheckCouponCode = SHOPPING_INITIAL_URL + "checkCouponCode.php";
    //end----- SearchProducts

    //start----- ProductInformationFragment
    public static String urlGetProductReviewResult = SHOPPING_INITIAL_URL + "get_Product_Review.php";
    public static String urlGetSingleProductInformation = SHOPPING_INITIAL_URL + "single_product_information.php";
    //end----- ProductInformationFragment
    //start----- AddToCart
    public static String urlGetCartCount = SHOPPING_INITIAL_URL + "PkGetCartCount.php";//sync with local GetCartCount
    public static String urlCheckQtyProductWise = SHOPPING_INITIAL_URL + "PkcheckQtyProductWise.php"; //checkQtyProductWise
    public static String urlDeleteCartResult = SHOPPING_INITIAL_URL + "PkdeleteCartItems.php";//For Normal & Combo Cart deleteCartItems
    public static String urlAdd_UpdateCartDetails = SHOPPING_INITIAL_URL + "PkNewAddUpdateCartDetails.php";//For Normal Cart    newAddUpdateCartDetails
    public static String urlComboCartResult = SHOPPING_INITIAL_URL + "updateComboCartDetails.php";//For Combo Cart
    public static String urlGetCartItem = SHOPPING_INITIAL_URL + "fetchCartID.php";
    public static String urlGetMyCartItems = SHOPPING_INITIAL_URL1 + "PkcartProducts.php";//For Normal & Combo Cartpublic static cartProducts
    public static String urlGetMyCartOutOfStockItems = SHOPPING_INITIAL_URL + "PkGetMyCartOutOfStockItems.php";//For Normal & Combo Cart GetMyCartOutOfStockItems
    //end----- AddToCart

    public static String urlStopOrder = SHOPPING_INITIAL_URL + "stopOrder.php";

    //start----- QuickLinks
    public static String about_us = SHOPPING_INITIAL_URL2 + "aboutUs.html";
    public static String terms = SHOPPING_INITIAL_URL2 + "termsConditions.html";
    public static String cash_back = SHOPPING_INITIAL_URL2 + "cashback.html";
    public static String help = SHOPPING_INITIAL_URL2 + "help.html";
    public static String blank_page = SHOPPING_INITIAL_URL2 + "blank_page.html";
    //end----- QuickLinks

    //start----- MyOrder
    public static String urlMyOrderForUser = SHOPPING_INITIAL_URL + "PkuserMyOrdersHome.php";   // userMyOrdersHome.php
     public static String urlOrderRemark = SHOPPING_INITIAL_URL + "PkuserOrderRemark.php";   //userOrderRemark.php
    public static String urlCancelOrderSpinnerData = SHOPPING_INITIAL_URL + "cancelOrderSpinner.php";
    public static String urlCancelOrder = SHOPPING_INITIAL_URL + "PkuserCancelOrder.php";  //userCancelOrder.php
    public static String urlOrderDetails = SHOPPING_INITIAL_URL + "PkDeliveryType_v1.7.php"; //DeliveryType_v1.7
    //end----- MyOrder

    //start----- NotificationsHome
   public static String urlGetNotification = SHOPPING_INITIAL_URL + "PkgetNotifications.php";   //getNotifications.php
    public static String urlDeactiveNotification = SHOPPING_INITIAL_URL + "deactiveNotification.php";

    //start----- MyOrderDetails
    public static String urlProductDetails = SHOPPING_INITIAL_URL + "PkuserMyOrderDetails.php";   //userMyOrderDetails.php
    public static String urlmyLastOrder = SHOPPING_INITIAL_URL + "userMyLastOrder.php";
    public static String urlUserRepeatOrder = SHOPPING_INITIAL_URL + "userRepeatOrder.php";
    //end----- MyOrderDetails

    //start----- CashBackOffer
    public static String urlUserCashBackCategory = SHOPPING_INITIAL_URL + "getRedeemCategory.php";
    //end----- CashBackOffer

    //start----- OrderDetails
    public static String urlGetPriceDetails = SHOPPING_INITIAL_URL + "PknewAddToCartCalculation_v1.7.php"; //newAddToCartCalculation_v1.7
    public static String urlCashOnDelivery = SHOPPING_INITIAL_URL + "Pknew_place_order_v1.7.php"; //new_place_order_v1.7.php  Pknew_place_order_v1.7.php
    public static String urlProblematicDeleteCondition = SHOPPING_INITIAL_URL + "problematicDeleteCartItems.php";//
    public static String urlOrderConfirmationNotification = SHOPPING_INITIAL_URL + "PkorderConfirmationNotification.php";
    public static String urlCouponsDetails = SHOPPING_INITIAL_URL + "couponDetails.php";
    public static String urlNewScheduleDates = SHOPPING_INITIAL_URL + "PknewScheduleDatesSlots_v1.7.php";
    //end----- OrderDetails

    //start----applayCouponCode
    public static String urlsendcouponCode = SHOPPING_INITIAL_URL + "sendcouponCode.php";
    public static String urlgetNewScheduledDatesForCoupon = SHOPPING_INITIAL_URL + "getNewScheduledDatesForCoupon.php";
    public static String urlverifyTRN = SHOPPING_INITIAL_URL + "verifyTRN.php";
    //end-----

    //shop keeper get recharge points and amount
    public static String placerecharge = baseUrl+"placerecharge";
    public static String getrechargehistroy = baseUrl+"getrechargehistroy";
    public static String getrecharge = baseUrl+"getrecharge";
    public static String getrechargeplan = baseUrl+"getrechargeplan";
    public static String stafflist  = baseUrl+"getstafflist";
    public static String invoicedetails = baseUrl+"invoicedetails";
    public static String termscond  = baseUrl+"terms";
    public static String helpmerchant = baseUrl+"helpmerchant";

    public static String brandlist = baseUrl+"getbrandlist";
    public static String partnerkit = baseUrl+"partnerkit";

    public static String shopkeeperlist = baseUrl+"shopkeeperlist";
    public static String getShopDetails = baseUrl2+"getshop";

    public static String getRejectedorderreports = baseUrl+"rejectedorderreports";
    public static String getrejectedorder = baseUrl+"rejectedorder";
    //public static String getloginadmin = baseUrl+"adminlogin"; Live
    public static String getloginadmin = baseUrl2+"adminlogin";
    public static String updateManagertoken = baseUrl+"updatetokenm";

    //public static String addstaff = baseUrl+"addstaff"; // Live api
    public static String addstaff = baseUrl2+"addstaff"; // for the city addition
    //public static String getUserName = baseUrl+"getuser";
    public static String getUserName = baseUrl2+"getuser";
    public static String getViewbyshop = baseUrl2+"viewbyshop";

    //public static String shopenquireList = baseUrl+"shopenquire";
    public static String shopenquireList = baseUrl2+"shoplist";
    public static String addshopenquire = baseUrl+"addshopenquire";

    //Send Notification
    public static String sendNotificationFromAdmin = SHOPPING_INITIAL_URL + "/sendNotification_fromAdmin.php";
    public static String uploadNotificationImage = baseUrl2 + "addnotification";
    public static String offernotificatonlist = SHOPPING_INITIAL_URL + "notificationList.php";

    public static String newScheduleforShopkeeper = SHOPPING_INITIAL_URL +"newScheduleforShopkeeper.php";

    public static String GetUnplaceOrderDetsils = "https://www.picodel.com/seller/shoptestapi/unplaceorders";
    public static String singeitem = baseUrl2+"singeitem";
    public static String checkadmin = baseUrl3+"checkadmin";

    public static String get_whatsapp_containt = SHOPPING_INITIAL_URL + "get_whatsapp_containt.php";
    public static String activeCityForAdmin = SHOPPING_INITIAL_URL + "activeCityForAdmin.php";
    public static String urlget_zone_area = SHOPPING_INITIAL_URL + "get_zone_area.php";
    public static String urlchange_shop_distance = SHOPPING_INITIAL_URL + "change_shop_distance.php";
    public static String urlfranchisee_payment = SHOPPING_INITIAL_URL + "franchisee_payment.php";
    public static String add_adminDetails = SHOPPING_INITIAL_URL + "add_adminDetails.php";

}
