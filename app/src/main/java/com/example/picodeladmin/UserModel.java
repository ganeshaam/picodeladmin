package com.example.picodeladmin;

public class UserModel {
    String id,name,contact,shop_name,redeem_points,date,lastlogin_date,lname,status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getRedeem_points() {
        return redeem_points;
    }

    public void setRedeem_points(String redeem_points) {
        this.redeem_points = redeem_points;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLastlogin_date() {
        return lastlogin_date;
    }

    public void setLastlogin_date(String lastlogin_date) {
        this.lastlogin_date = lastlogin_date;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
