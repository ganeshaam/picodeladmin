package com.example.picodeladmin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class OTPListAdaptor extends RecyclerView.Adapter<OTPListAdaptor.OTPHolder>{


    ArrayList<OTPModel> arrayList;
    Context mContext;

    public OTPListAdaptor(ArrayList<OTPModel> arrayList, Context mContext) {
        this.arrayList = arrayList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public OTPHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        /*return null;*/
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dialog_otp_list,parent,false);
        OTPListAdaptor.OTPHolder holder = new  OTPListAdaptor.OTPHolder(view);
        return holder;
    }


    @Override
    public void onBindViewHolder(@NonNull OTPHolder holder, int position) {

        TextView tv_otp,tv_date;
        TextView tv_otp_contact;

        tv_otp = holder.tv_otp;
        tv_otp_contact = holder.tv_otp_contact;
        tv_date = holder.tv_date;

        tv_otp.setText(arrayList.get(position).getVcode());
        tv_otp_contact.setText(arrayList.get(position).getContact());
        tv_date.setText(arrayList.get(position).getVdate());

    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public  static  final class  OTPHolder extends RecyclerView.ViewHolder{

        TextView tv_otp,tv_otp_contact,tv_date;

        public OTPHolder(@NonNull View itemView) {
            super(itemView);
            tv_date = itemView.findViewById(R.id.tv_date);
            tv_otp_contact = itemView.findViewById(R.id.tv_otp_contact);
            tv_otp = itemView.findViewById(R.id.tv_otp);
        }
    }
}
