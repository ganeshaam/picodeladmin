package com.example.picodeladmin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class RejectedOrderAdapter extends RecyclerView.Adapter<RejectedOrderAdapter.OrderlistHolder>{

    ArrayList<ShopModel> shopModels;
    Context mContext;

    public RejectedOrderAdapter(ArrayList<ShopModel> shopModels, Context mContext) {
        this.shopModels = shopModels;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public RejectedOrderAdapter.OrderlistHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_rejectedorderlist,parent,false);
        RejectedOrderAdapter.OrderlistHolder soplistHolder = new RejectedOrderAdapter.OrderlistHolder(view);
        return soplistHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RejectedOrderAdapter.OrderlistHolder soplistHolder, int position) {
             TextView tv_orderid = soplistHolder.tx_orderId;
             tv_orderid.setText("OrderId: "+shopModels.get(position).getRejectedOrderId());
        }

        @Override
    public int getItemCount() {
        return shopModels.size();
    }


    public static class OrderlistHolder extends RecyclerView.ViewHolder {

        TextView tx_orderId;

        public OrderlistHolder(@NonNull View itemView) {
            super(itemView);
            tx_orderId = itemView.findViewById(R.id.tv_orderid);
        }
    }
}
