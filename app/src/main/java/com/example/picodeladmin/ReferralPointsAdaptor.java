package com.example.picodeladmin;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;


public class ReferralPointsAdaptor extends RecyclerView.Adapter<ReferralPointsAdaptor.CarlistHolder>{

    ArrayList<UserModel> cartArrayList;
    Context mContext;
    String type;

    public ReferralPointsAdaptor(ArrayList<UserModel> cartArrayList, Context mContext, String type) {
        this.cartArrayList = cartArrayList;
        this.mContext = mContext;
        this.type = type;

    }

    @NonNull
    @Override
    public CarlistHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_for_refer_points,parent,false);
        CarlistHolder carlistHolder = new  CarlistHolder(view);
        return carlistHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CarlistHolder carlistHolder, final int position) {

        TextView contact = carlistHolder.tv_contact;
        CardView cardView = carlistHolder.cd_view;
        TextView name = carlistHolder.user_name;
        TextView id = carlistHolder.tv_user_id;
        TextView status = carlistHolder.tv_status;

        Log.e("cotacts:",""+cartArrayList.get(position).getContact());


        id.setText(cartArrayList.get(position).getId());

        if(type.equalsIgnoreCase("shop")){
            contact.setText(cartArrayList.get(position).getContact()+"\n "+cartArrayList.get(position).getShop_name());
            name.setText(cartArrayList.get(position).getName());
        }else {
            name.setText(cartArrayList.get(position).getName()+" "+cartArrayList.get(position).getLname());
            contact.setText(cartArrayList.get(position).getContact()+"\n"+cartArrayList.get(position).date+"\n");
            status.setText(cartArrayList.get(position).getStatus());
            if(cartArrayList.get(position).getStatus().equalsIgnoreCase("1")){
               carlistHolder. tv_status.setBackgroundColor(Color.parseColor("#FF9800"));
            }
        }


        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // openWhatsApp(cartArrayList.get(position).getContact());
            }
        });

    }

    @Override
    public int getItemCount() {
        return cartArrayList.size();
    }

    public static class CarlistHolder extends RecyclerView.ViewHolder {
        LinearLayout ll_not_avail;
        CardView cd_view;

        TextView tv_contact,user_name,tv_user_id,tv_status;
        public CarlistHolder(@NonNull View itemView) {
            super(itemView);
            tv_contact = itemView.findViewById(R.id.tv_contact);
            cd_view = itemView.findViewById(R.id.cd_view);
            user_name = itemView.findViewById(R.id.user_name);
            tv_user_id = itemView.findViewById(R.id.tv_user_id);
            tv_status = itemView.findViewById(R.id.tv_status);

        }
    }
    private void openWhatsApp(String mobileNo) {
        /*String smsNumber = mobileno;//"7****"; // E164 format without '+' sign
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("text/plain");
        sendIntent.putExtra(Intent.EXTRA_TEXT, "PICODEL Admin.");
        sendIntent.putExtra("jid", smsNumber + "@s.whatsapp.net"); //phone number without "+" prefix
        sendIntent.setPackage("com.whatsapp");
        if (sendIntent.resolveActivity(getPackageManager()) == null) {
            //Toast.makeText(this, "Error/n" + e.toString(), Toast.LENGTH_SHORT).show();
            return;
        }*/

//        Intent sendIntent = new Intent("android.intent.action.MAIN");
//        sendIntent.setComponent(new ComponentName("com.whatsapp","com.whatsapp.Conversation"));
//        sendIntent.putExtra("Hi", PhoneNumberUtils.stripSeparators(mobileNo)+"@s.whatsapp.net");
//        startActivity(sendIntent);
        try {
            String text = "Glad to share the link to get 50/- off on your 1st order of Groceries and Vegetables from PICODEL!\nIt helps to get delivery of the order from 60 minutes to Max 3 hours...\nDownload PICODEL App and enjoy lot more unique features !";// Replace with your message.

            String toNumber = mobileNo; // Replace with mobile phone number without +Sign or leading zeros, but with country code
            //Suppose your country is India and your phone number is “xxxxxxxxxx”, then you need to send “91xxxxxxxxxx”.

            //toNumber = toNumber.replace("+", "").replace(" ", "").replace("-","");
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("http://api.whatsapp.com/send?phone="+91+toNumber +"&text="+text));
            mContext.startActivity(intent);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}

