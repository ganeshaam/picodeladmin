package com.example.picodeladmin;

public class UnplacedCartModel {

    String cart_item_id,cart_unique_id,appVersionCode,user_id,final_status,cdate,sessionMainCat;

    public String getCart_item_id() {
        return cart_item_id;
    }

    public void setCart_item_id(String cart_item_id) {
        this.cart_item_id = cart_item_id;
    }

    public String getCart_unique_id() {
        return cart_unique_id;
    }

    public void setCart_unique_id(String cart_unique_id) {
        this.cart_unique_id = cart_unique_id;
    }

    public String getAppVersionCode() {
        return appVersionCode;
    }

    public void setAppVersionCode(String appVersionCode) {
        this.appVersionCode = appVersionCode;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getFinal_status() {
        return final_status;
    }

    public void setFinal_status(String final_status) {
        this.final_status = final_status;
    }

    public String getCdate() {
        return cdate;
    }

    public void setCdate(String cdate) {
        this.cdate = cdate;
    }

    public String getSessionMainCat() {
        return sessionMainCat;
    }

    public void setSessionMainCat(String sessionMainCat) {
        this.sessionMainCat = sessionMainCat;
    }
}
