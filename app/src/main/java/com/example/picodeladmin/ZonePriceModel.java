package com.example.picodeladmin;

public class ZonePriceModel {

    private  String price_val,zone;

    public String getPrice_val() {
        return price_val;
    }

    public void setPrice_val(String price_val) {
        this.price_val = price_val;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }
}
