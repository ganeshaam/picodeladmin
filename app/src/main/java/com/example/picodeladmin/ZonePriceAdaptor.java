package com.example.picodeladmin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class ZonePriceAdaptor extends RecyclerView.Adapter<ZonePriceAdaptor.ZoneValueHolder>{


    private ArrayList<ZonePriceModel> arrayList;
    private Context mContext;

    public ZonePriceAdaptor(ArrayList<ZonePriceModel> arrayList, Context mContext) {
        this.arrayList = arrayList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ZoneValueHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //return null;

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_zone_items,parent,false);
        ZonePriceAdaptor.ZoneValueHolder holder = new  ZonePriceAdaptor.ZoneValueHolder(view);
        return holder;


    }


    @Override
    public void onBindViewHolder(@NonNull ZoneValueHolder holder, int position) {

        TextView tv_zone = holder.tv_zone;
        TextView tv_price_val = holder.tv_price_val;

        tv_price_val.setText(arrayList.get(position).getPrice_val());
        tv_zone.setText(arrayList.get(position).getZone());

    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static class ZoneValueHolder extends RecyclerView.ViewHolder{

        TextView tv_zone,tv_price_val;

        public ZoneValueHolder(@NonNull View itemView) {
            super(itemView);

            tv_zone = itemView.findViewById(R.id.tv_zone);
            tv_price_val = itemView.findViewById(R.id.tv_price_val);
        }
    }
}
