package com.example.picodeladmin;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class FranchiseePaymentActivity extends AppCompatActivity {


    Context context;
    SharedPreferencesUtils sharedPreferencesUtils;

    EditText et_contact,et_paid_amount,et_paid_date,et_paid_type;
    Button btn_submit;
    String shop_id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_franchisee_payment);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Shop Associate");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        context = FranchiseePaymentActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(context);
        shop_id = sharedPreferencesUtils.getShopID();

        et_contact = findViewById(R.id.et_contact);
        et_paid_amount = findViewById(R.id.et_paid_amount);
        et_paid_date = findViewById(R.id.et_paid_date);
        et_paid_type = findViewById(R.id.et_paid_type);
        btn_submit = findViewById(R.id.btn_submit);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(et_contact.getText().toString().isEmpty()||et_contact.getText().toString() == ""){
                    et_contact.setError("Please Enter Franchisee Contact");
                }else if(et_paid_amount.getText().toString().isEmpty()||et_paid_amount.getText().toString() == ""){
                    et_paid_amount.setError("Please Enter Amount");
                }else if(et_paid_date.getText().toString().isEmpty()||et_paid_date.getText().toString() == ""){
                    et_paid_date.setError("Please Enter Paid Date");
                }else if(et_paid_type.getText().toString().isEmpty()||et_paid_type.getText().toString() == ""){
                    et_paid_type.setError("Please Enter Payment Mode");
                }else {
                    AddFranchiseePayment(shop_id);
                }
            }
        });

    }

    // API To change the Distance Area  //urlchange_shop_distance
    private void AddFranchiseePayment(String shopid) { //TODO Server method here

        JSONObject params = new JSONObject();
        try {
            params.put("f_contact", et_contact.getText().toString());
            params.put("admin_id", sharedPreferencesUtils.getShopID());// new Admin ID
            params.put("paid_amount", et_paid_amount.getText().toString());
            params.put("paid_date", et_paid_date.getText().toString());
            params.put("paid_by", et_paid_type.getText().toString());
            Log.e("Fran_PaymentParam:",""+params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String url = StaticUrl.urlfranchisee_payment;
        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {

            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(JSONObject response) {

                Log.e("ShopDUpdateRes", "" + response.toString());
                //progressDialog2.dismiss();
                if (response.isNull("posts")) {
                    //  progressDialog2.dismiss();
                }else{

                    try {
                        String res = response.getString("posts");

                        if(res.equalsIgnoreCase("Update successfully")){
                            Toast.makeText(context,"Update successfully",Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                //progressDialog2.dismiss();

            }
        });
        VolleySingleton.getInstance(FranchiseePaymentActivity.this).addToRequestQueue(request);

    }

}
