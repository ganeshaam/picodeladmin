package com.example.picodeladmin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class ShopViewDetailsAdaptor extends RecyclerView.Adapter<ShopViewDetailsAdaptor.ShopViewHolder> {

    ArrayList<ShopModel> arrayList;
    Context mcontext;

    public ShopViewDetailsAdaptor(ArrayList<ShopModel> arrayList, Context mcontext) {
        this.arrayList = arrayList;
        this.mcontext = mcontext;
    }

    @NonNull
    @Override
    public ShopViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //return null;

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_shop_detail_layout,parent,false);
        ShopViewHolder soplistHolder = new ShopViewHolder(view);
        return soplistHolder;

    }


    @Override
    public void onBindViewHolder(@NonNull ShopViewHolder holder, int position) {

        holder.tv_shop_name.setText(arrayList.get(position).getShop_name());
        holder.tv_orderid.setText(arrayList.get(position).getOrder_id());
        holder.tv_viewstatus.setText(arrayList.get(position).getView_status());
        holder.tv_viewtime.setText(arrayList.get(position).getView_time());

    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public  static class ShopViewHolder extends RecyclerView.ViewHolder{

        TextView tv_orderid,tv_shop_name,tv_viewstatus,tv_viewtime;

        public ShopViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_orderid = itemView.findViewById(R.id.tv_orderid);
            tv_shop_name = itemView.findViewById(R.id.tv_shop_name);
            tv_viewstatus = itemView.findViewById(R.id.tv_viewstatus);
            tv_viewtime = itemView.findViewById(R.id.tv_viewtime);
        }
    }
}
