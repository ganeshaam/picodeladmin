package com.example.picodeladmin;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Locale;

import dmax.dialog.SpotsDialog;

import static java.net.URLEncoder.encode;

public class FreelancerActivity extends AppCompatActivity {

    SharedPreferencesUtils sharedPreferencesUtils;
    Context mContext;
    AlertDialog progressDialog;
    ArrayList<FreelancerModel> modelArrayList;
    ArrayList<FreelancerModel> modelArrayList2;
    private RecyclerView.LayoutManager layoutManager;
    private FreelancerAdapter freelancerAdapter;
    RecyclerView freRecyclerView;
    TextView tv_network_con;
    EditText et_message;
    String category = "";
    boolean flag_search = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_freelancer);

        Toolbar toolbar =   findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Select Area Wise Order");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = FreelancerActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();
        tv_network_con = findViewById(R.id.tv_network_con);
        freRecyclerView = findViewById(R.id.rv_freelancer_area);
        et_message = findViewById(R.id.et_message);

        Resources resources = getResources();
        Locale local = new Locale(sharedPreferencesUtils.getLocal());
        Configuration configuration =  resources.getConfiguration();
        configuration.setLocale(local);
        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());


        freRecyclerView.setHasFixedSize(true);
        modelArrayList = new ArrayList<>();
        modelArrayList2 = new ArrayList<>();
        layoutManager = new LinearLayoutManager(mContext);
        freRecyclerView.setLayoutManager(layoutManager);
        freRecyclerView.setItemAnimator(new DefaultItemAnimator());
        freelancerAdapter = new FreelancerAdapter(modelArrayList,mContext);
        freRecyclerView.setAdapter(freelancerAdapter);

        category = getIntent().getStringExtra("category");

        if(Connectivity.isConnected(mContext)){
            getFreelancerArea();
        }else {
            tv_network_con.setVisibility(View.VISIBLE);
        }

        et_message.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {



            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                modelArrayList2.clear();

                for(int k=0; k<modelArrayList.size();k++ ){
                    if(modelArrayList.get(k).getAreaName().contains(charSequence)){
                        modelArrayList2.add(modelArrayList.get(k));
                    }
                }

                flag_search = true;
                freelancerAdapter = new FreelancerAdapter(modelArrayList2,mContext);
                freRecyclerView.setAdapter(freelancerAdapter);
                freelancerAdapter.notifyDataSetChanged();
                Log.e("model_size:",""+modelArrayList2.size());

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        //order list area wise for freelancer
        freRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), freRecyclerView,
                new RecyclerTouchListener.ClickListener()
                {
                    @Override
                    public void onClick(View view, int position)
                    {
                        if(flag_search) {
                            String areaName = modelArrayList2.get(position).getAreaName();
                            Intent intent = new Intent(mContext, MainActivity.class);
                            intent.putExtra("areaWise_order", "areaWise_order");
                            intent.putExtra("areaName", areaName);
                            intent.putExtra("category", category);
                           // intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            startActivity(intent);

                        }else {
                            String areaName = modelArrayList.get(position).getAreaName();
                            Intent intent = new Intent(mContext, MainActivity.class);
                            intent.putExtra("areaWise_order", "areaWise_order");
                            intent.putExtra("areaName", areaName);
                            intent.putExtra("category", category);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            startActivity(intent);

                        }
                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {
                        Toast.makeText(mContext,"No Records", Toast.LENGTH_SHORT).show();
                    }
                }));

    }

    public void getFreelancerArea(){
        progressDialog.show();
        modelArrayList.clear();

        String targetUrl = null;
        try {
            targetUrl = StaticUrl.freelancerArea+"?category="+ encode(category,"utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        StringRequest orderListRequest = new StringRequest(Request.Method.GET,
                targetUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("orderlist",response);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean strStatus = jsonObject.getBoolean("status");
                            if ((strStatus)){
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if(jsonArray.length()>0){
                                    for(int i = 0; i < jsonArray.length(); i++){
                                        JSONObject olderlist = jsonArray.getJSONObject(i);
                                        FreelancerModel model = new FreelancerModel();
                                        model.setAreaName(olderlist.getString("area"));
                                        model.setArecount(olderlist.getString("area_count"));
                                        modelArrayList.add(model);
                                    }
                                    freelancerAdapter.notifyDataSetChanged();
                                }else {
                                    tv_network_con.setText("No Records Found");
                                }
                            }else if(strStatus=false){
                                tv_network_con.setVisibility(View.VISIBLE);
                                tv_network_con.setText("No Records Found");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(mContext).addToRequestQueue(orderListRequest);
    }

    //RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private FreelancerActivity.RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final FreelancerActivity.RecyclerTouchListener.ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
