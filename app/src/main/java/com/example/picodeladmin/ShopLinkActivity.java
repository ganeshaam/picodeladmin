package com.example.picodeladmin;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ShopLinkActivity extends AppCompatActivity {


    EditText et_contact;
    TextView tv_shopname,tv_shopcontact,tv_shoparea,tv_shopassociate,tv_network_con;
    Button btn_search_contact,btn_shop_link;

    Context context;
    SharedPreferencesUtils sharedPreferencesUtils;
    ArrayList<ShopEnquireModel> arrayList;
    String city,fc_city,shop_id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_shoplink);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Shop Associate");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        context = ShopLinkActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(context);
        arrayList = new ArrayList<>();

        initviews();

        fc_city = sharedPreferencesUtils.getFC_City();
        city = sharedPreferencesUtils.getAdminCity();

        btn_search_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getSearchShop();
            }
        });

        btn_shop_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(shop_id.isEmpty()){

                    Toast.makeText(context,"Please Search Shop First",Toast.LENGTH_LONG).show();

                }else {

                    AddShopLink(shop_id);
                }
            }
        });

    }

    void initviews(){

        et_contact = findViewById(R.id.et_contact);
        tv_shopname = findViewById(R.id.tv_shopname);
        tv_shopcontact = findViewById(R.id.tv_shopcontact);
        tv_shoparea = findViewById(R.id.tv_shoparea);
        tv_shopassociate = findViewById(R.id.tv_shopassociate);
        btn_search_contact = findViewById(R.id.btn_search_contact);
        btn_shop_link = findViewById(R.id.btn_shop_link);
        tv_network_con = findViewById(R.id.tv_network_con);

    }

    private void getSearchShop() { //TODO Server method here


        JSONObject params = new JSONObject();
        try {
            params.put("name", et_contact.getText().toString());
            params.put("type", "shop");//true or false
            params.put("filter_type", "contact");
            params.put("fc_city", fc_city);
            params.put("city", city);
            Log.e("userParm:",""+params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String url ="https://www.picodel.com/And/shopping/AppAPI/getAllShopData.php";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {

            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(JSONObject response) {

                Log.e("ShopSearchRes", "" + response.toString());
                //progressDialog2.dismiss();
                if (response.isNull("posts")) {

                    //  progressDialog2.dismiss();
                }else{

                    try {
                        JSONArray jsonArray = response.getJSONArray("posts");
                        arrayList.clear();

                        Log.e("user_RS:",""+jsonArray.toString());
                        //if(jsonArray.length()>0){
                            // Log.d("orderlist",""+jsonArray);
                            for(int i = 0; i < jsonArray.length(); i++){
                                JSONObject shopList = jsonArray.getJSONObject(i);
                                ShopEnquireModel model = new ShopEnquireModel();
                                /*model.setId(shopList.getString("shop_id"));
                                model.setContact(shopList.getString("contact"));
                                model.setSellerName(shopList.getString("sellername"));
                                model.setShopName(shopList.getString("shop_name"));
                                model.setArea(shopList.getString("area"));
                                model.setUpdatedAt(shopList.getString("created_at"));
                                model.setRecharge_amount("recharge_amount");
                                model.setDistance(shopList.getString("distance"));
                                model.setAdmin_id(shopList.getString("admin_id"));
                                model.setReg_by("signup_suggested_by");
                                arrayList.add(model);
*/
                                Log.e("Shop Name: ",""+shopList.getString("shop_name"));
                                Log.e("Shop Contact:",""+shopList.getString("contact"));
                                Log.e("Area :",""+shopList.getString("area"));
                                shop_id =  shopList.getString("shop_id");
                                tv_shopname.setText("Shop Name: "+shopList.getString("shop_name"));
                                tv_shopcontact.setText("Shop Contact:"+shopList.getString("contact"));
                                tv_shoparea.setText("Area :"+shopList.getString("area"));
                                btn_shop_link.setVisibility(View.VISIBLE);
                            }
                            Log.e("ShopList_Size",""+arrayList.size());

                        //}else {
                           // tv_network_con.setVisibility(View.VISIBLE);
                            //tv_network_con.setText("No Records Found");
                        //}


                        //referralPointsAdaptor.notifyDataSetChanged();
                    }catch (Exception e){

                    }



                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                //progressDialog2.dismiss();

            }
        });
        VolleySingleton.getInstance(ShopLinkActivity.this).addToRequestQueue(request);

    }

    // API To change the Distance Area  //urlchange_shop_distance
    private void AddShopLink(String shopid) { //TODO Server method here

        JSONObject params = new JSONObject();
        try {
            params.put("shop_id", shopid);
            params.put("value", sharedPreferencesUtils.getShopID());// new Admin ID
            params.put("fc_city", fc_city);
            params.put("city", city);
            params.put("type", "shoplink");
            Log.e("changeDistanceParam:",""+params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String url = StaticUrl.urlchange_shop_distance;
        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {

            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(JSONObject response) {

                Log.e("ShopDUpdateRes", "" + response.toString());
                //progressDialog2.dismiss();
                if (response.isNull("posts")) {
                    //  progressDialog2.dismiss();
                }else{

                    try {
                        String res = response.getString("posts");

                        if(res.equalsIgnoreCase("Update successfully")){
                            Toast.makeText(context,"Update successfully",Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                //progressDialog2.dismiss();

            }
        });
        VolleySingleton.getInstance(ShopLinkActivity.this).addToRequestQueue(request);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
              /*  if(sharedPreferencesUtils.getAdminType().equals("Delivery Boy")){
                    finish();
                }else {
                    Intent intent = new Intent(mContext, OrderListActivity.class);
                    intent.putExtra("flag", flag);
                    intent.putExtra("areaName", areaName);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intent);
                }*/
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
