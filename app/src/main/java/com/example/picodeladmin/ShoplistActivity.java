package com.example.picodeladmin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import dmax.dialog.SpotsDialog;

public class ShoplistActivity extends AppCompatActivity {

    Context mContext;
    AlertDialog progressDialog;
    private RecyclerView.LayoutManager layoutManager;
    ArrayList<ShopModel> orderArrayList,orderArrayList2;
    private ShopListAdapter orderlistAdapter;
    RecyclerView rv_home_orderlist;
    String distance_value,strLatitude="",strLongitude="",strDeliveryinkm="",logintype="",flag="admin",appversion,slot,orderId,total_amount;
    TextView tv_network_con;
    //SeekBar sb_shopList;
    //TextView tv_destancekm;
    EditText et_search_shop;
    CardView cv_ShopList,cv_allShopList,cv_testShopList,cv_UnregisterList;

    SharedPreferencesUtils sharedPreferencesUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shoplist);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Shop List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = ShoplistActivity.this;
        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);

        tv_network_con = findViewById(R.id.tv_network_con);
        rv_home_orderlist = findViewById(R.id.rv_home_orderlist);
        //sb_shopList = findViewById(R.id.sb_shopList);
        //tv_destancekm = findViewById(R.id.tv_destancekm);
        cv_ShopList = findViewById(R.id.cv_ShopList);
        cv_allShopList = findViewById(R.id.cv_allShopList);
        cv_testShopList = findViewById(R.id.cv_testShopList);
        cv_UnregisterList = findViewById(R.id.cv_UnregisterList);
        et_search_shop = findViewById(R.id.et_search_shop);

        orderArrayList = new ArrayList<>();
        orderArrayList2 = new ArrayList<>();
        rv_home_orderlist.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(mContext);
        rv_home_orderlist.setLayoutManager(layoutManager);
        rv_home_orderlist.setItemAnimator(new DefaultItemAnimator());
        orderlistAdapter = new ShopListAdapter(orderArrayList,mContext);
        rv_home_orderlist.setAdapter(orderlistAdapter);

        Intent intent = getIntent();
        slot= intent.getStringExtra("slot");
        orderId = intent.getStringExtra("orderId");
        total_amount = intent.getStringExtra("total_amount");

            if (Connectivity.isConnected(mContext)) {
                getShopList("shoplist");
              //  sb_shopList.setProgress(10);
               // tv_destancekm.setText("10/20");
            } else {
                tv_network_con.setVisibility(View.VISIBLE);
            }

      /*  sb_shopList.setMax(40);
        sb_shopList.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            //int pval = 1;
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                //pval = progress;
                float pval=(float) progress/2;
                tv_destancekm.setText(pval + "/20");//+ seekBar.getMax()
                distance_value= String.valueOf(pval);
                Log.d("distance_value",distance_value);
                getShopList(distance_value);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });*/

        //select shop
        rv_home_orderlist.addOnItemTouchListener(new RecyclerTouchListener(mContext, rv_home_orderlist,
                new RecyclerTouchListener.ClickListener()
                {
                    @Override
                    public void onClick(View view, int position)
                    {

                        String shopId = orderArrayList.get(position).getShop_id();
                        String adminType = orderArrayList.get(position).getAdmin_type();
                        String shopArea = orderArrayList.get(position).getArea();
                        String contact = orderArrayList.get(position).getContact();
                        String shopName = orderArrayList.get(position).getShop_name();
                        String category = orderArrayList.get(position).getShop_category();
                        String email = orderArrayList.get(position).getEmail();
                        String rechargeAmount = orderArrayList.get(position).getRecharge_amount();
                        String rechargePoints  = orderArrayList.get(position).getRecharge_points();
                        String sellerName = orderArrayList.get(position).getSellername();

                        acceptConfirmDialog(shopId,orderId,slot,email,shopName);

                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {
                        Toast.makeText(mContext,"No Records", Toast.LENGTH_SHORT).show();
                    }
                }));
        cv_ShopList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //all shop list
                // Shoplist Api
                Log.e("TestCommit:","TestPush");
                getShopList("shoplist");
            }
        });
        cv_allShopList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //all shop list
                getShopList("shoplistall");
            }
        });
        cv_testShopList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getShopList("testshoplist");
            }
        });

        cv_UnregisterList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getShopList("shoplistunregister");
            }
        });

        et_search_shop.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                //if(charSequence.length()>=2) {
                    orderArrayList2.clear();

                    for (int k = 0; k < orderArrayList.size(); k++) {
                        if (orderArrayList.get(k).getShop_name().contains(charSequence)) {
                            orderArrayList2.add(orderArrayList.get(k));
                        }
                    }
                    orderlistAdapter = new ShopListAdapter(orderArrayList2, mContext);
                    rv_home_orderlist.setAdapter(orderlistAdapter);
                    orderlistAdapter.notifyDataSetChanged();
                    Log.e("model_size:", "" + orderArrayList2.size());
                //}

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    //shop list based on order location
    public void getShopList(String shopListType){

        progressDialog.show();
        orderArrayList.clear();
        orderArrayList2.clear();

        String targetUrlShopList = "https://www.picodel.com/And/shopping/AppAPI/testDistanceapi.php?orderId="+orderId+"&shopListType="+shopListType+"&city="+sharedPreferencesUtils.getAdminCity();
        //String targetUrlShopList = "https://www.picodel.com/And/shopping/AppAPI/testDistanceapi.php?orderId="+orderId+"&shopListType="+shopListType;

        Log.d("targetUrlShopList",targetUrlShopList);
        StringRequest orderListRequest = new StringRequest(Request.Method.GET,
                targetUrlShopList,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("orderlist",response);
                        //cv_viewmore.setVisibility(View.VISIBLE);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean strStatus = jsonObject.getBoolean("status");
                            if ((strStatus==true)){
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if(jsonArray.length()>0){
                                   // Log.d("orderlist",""+jsonArray);
                                    for(int i = 0; i < jsonArray.length(); i++){
                                        JSONObject olderlist = jsonArray.getJSONObject(i);
                                        ShopModel model = new ShopModel();
                                        model.setShop_id(olderlist.getString("shop_id"));
                                        model.setContact(olderlist.getString("contact"));
                                        model.setEmail(olderlist.getString("email"));
                                        model.setAdmin_type(olderlist.getString("admin_type"));
                                        model.setArea(olderlist.getString("area"));
                                        model.setRecharge_amount(olderlist.getString("recharge_amount"));
                                        model.setRecharge_points(olderlist.getString("recharge_points"));
                                        model.setSellername(olderlist.getString("sellername"));
                                        model.setShop_category(olderlist.getString("shop_category"));
                                        model.setShop_name(olderlist.getString("shop_name"));
                                        model.setPreferredDeliveryAreaKm(olderlist.getString("preferred_delivery_area_km"));
                                        //Log.d("shopName",olderlist.getString("shop_name"));
                                        orderArrayList.add(model);
                                    }
                                    orderlistAdapter = new ShopListAdapter(orderArrayList,mContext);
                                    rv_home_orderlist.setAdapter(orderlistAdapter);
                                    orderlistAdapter.notifyDataSetChanged();
                                }else {
                                    tv_network_con.setVisibility(View.VISIBLE);
                                    tv_network_con.setText("No Records Found");
                                }
                            }else if(strStatus=false){
                                tv_network_con.setVisibility(View.VISIBLE);
                                tv_network_con.setText("No Records Found");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(mContext).addToRequestQueue(orderListRequest);
    }
    //accept order
    public void acceptOrder( String shopID, String orderId, String delivery_time,String email,String shopName) {
        if (Connectivity.isConnected(mContext)) {

            progressDialog.show();

            String urlAcceptOrder = null;
            try {
                urlAcceptOrder = StaticUrl.acceptorder
                        + "?order_id=" + orderId
                        + "&approveBy=" + email
                        //+ "&manager_id=" + sharedPreferencesUtils.getManagerId()
                        + "&delivery_time=" + URLEncoder.encode(delivery_time,"utf-8")
                        + "&assign_shop_id=" + shopID
                        + "&tblName=Seller"//+sharedPreferencesUtils.getAdminType()
                        + "&totalAmount="+total_amount
                        + "&shopName="+URLEncoder.encode(shopName,"utf-8");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            // + "&delivery_time="+delivery_time;
            Log.d("urlAcceptOrder",urlAcceptOrder);
            StringRequest stringRequestAcceptOrder = new StringRequest(Request.Method.GET,
                    urlAcceptOrder,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                Log.e("acceptorderRes:",""+response);
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status = true) {
                                    //JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    Toast.makeText(mContext, "Order Assigned", Toast.LENGTH_SHORT).show();

                                  /*Intent intent = new Intent(mContext,OrderListActivity.class);
                                    intent.putExtra("flag","approved_order");
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(intent);*/

                                } else if (status = false) {
                                    Toast.makeText(mContext, "Sorry. This order you can not accept.", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestAcceptOrder);
        } else {
            Toast.makeText(mContext, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }
    //accept order dialog
    public void acceptConfirmDialog(final String shopId,final String orderId,final String slot,final String email,final String shopName){

        final Dialog dialog = new Dialog(ShoplistActivity.this);
        dialog.setContentView(R.layout.accept_order_confirm_dialog);
        Button btn_dialog_assign = dialog.findViewById(R.id.btn_dialog_assign);
        Button btn_dialog_close = dialog.findViewById(R.id.btn_dialog_close);
        btn_dialog_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        btn_dialog_assign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                acceptOrder(shopId,orderId,slot,email,shopName);
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
       /* if(sharedPreferencesUtils.getAdminType().equals("Delivery Boy")){
            super.onBackPressed();
            finish();
        }else {
            Intent intent = new Intent(mContext, OrderListActivity.class);
            intent.putExtra("flag", flag);
            intent.putExtra("areaName", areaName);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
              /*  if(sharedPreferencesUtils.getAdminType().equals("Delivery Boy")){
                    finish();
                }else {
                    Intent intent = new Intent(mContext, OrderListActivity.class);
                    intent.putExtra("flag", flag);
                    intent.putExtra("areaName", areaName);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intent);
                }*/
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ShoplistActivity.RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final ShoplistActivity.RecyclerTouchListener.ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }

}
