package com.example.picodeladmin;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;

public class SendNotificationActivity extends AppCompatActivity {

    Button bt_page1, bt_page2, bt_page3, bt_page4, bt_page5, bt_page6, bt_page7, bt_page8, bt_page9;
    Button bt_sendshop, btn_user_test, btn_shop_test, btn_uploadNotification;
    String sms = "", imageNotifurl = "", imageStatus = "no", flagstatus = "no", notification_message = "", imageStatusUrl = "no";
    TextView success_count, fail_count, tv_selected_notification;
    android.app.AlertDialog progressDialog;
    public static final int PICK_IMAGE = 1,CAPTURE_IMG = 2;
    Bitmap notificatinBitmap=null;
    String[] PERMISSIONS = {android.Manifest.permission.WRITE_EXTERNAL_STORAGE,  Manifest.permission.CAMERA};
    int PERMISSION_ALL = 1;
    ImageView iv_notification;
    SharedPreferencesUtils sharedPreferencesUtils;

    ArrayList<NotificationListModel> notificationListModelArrayList;
    RecyclerView recycler_view_notification;
    private RecyclerView.LayoutManager layoutManager;
    private NotificationListAdapter notificationListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_notification);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Send Notification");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        progressDialog = new SpotsDialog.Builder().setContext(this).build();
        sharedPreferencesUtils = new SharedPreferencesUtils(SendNotificationActivity.this);

        bt_page1 = findViewById(R.id.bt_page1);
        bt_page2 = findViewById(R.id.bt_page2);
        bt_page3 = findViewById(R.id.bt_page3);
        bt_page4 = findViewById(R.id.bt_page4);
        bt_page5 = findViewById(R.id.bt_page5);
        bt_page6 = findViewById(R.id.bt_page6);
        bt_page7 = findViewById(R.id.bt_page7);
        bt_page8 = findViewById(R.id.bt_page8);
        bt_page9 = findViewById(R.id.bt_page9);

        bt_sendshop = findViewById(R.id.bt_sendshop);
        success_count = findViewById(R.id.success_count);
        fail_count = findViewById(R.id.fail_count);
        //iv_notification = findViewById(R.id.iv_notification);
        btn_user_test = findViewById(R.id.btn_user_test);
        btn_shop_test = findViewById(R.id.btn_shop_test);
        btn_uploadNotification = findViewById(R.id.btn_uploadNotification);
        recycler_view_notification = findViewById(R.id.recycler_view_notification);
        tv_selected_notification = findViewById(R.id.tv_selected_notification);

        if (!hasPermissions(SendNotificationActivity.this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(SendNotificationActivity.this, PERMISSIONS, PERMISSION_ALL);
        }

        notificationListModelArrayList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(SendNotificationActivity.this);
        recycler_view_notification.setLayoutManager(layoutManager);
        notificationListAdapter = new NotificationListAdapter(SendNotificationActivity.this, notificationListModelArrayList);
        recycler_view_notification.setAdapter(notificationListAdapter);

        bt_page1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (notification_message.equals("")) {
                    Toast.makeText(SendNotificationActivity.this, "Select Notification from List", Toast.LENGTH_SHORT);
                }else {
                    sendNotification("page1", notification_message, "user");
                }
            }
        });
        bt_page2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (notification_message.equals("")) {
                    Toast.makeText(SendNotificationActivity.this, "Select Notification from List", Toast.LENGTH_SHORT);
                }else {
                    sendNotification("page2", notification_message, "user");
                }
            }
        });
        bt_page3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (notification_message.equals("")) {
                    Toast.makeText(SendNotificationActivity.this, "Select Notification from List", Toast.LENGTH_SHORT);
                }else {
                    sendNotification("page3", notification_message, "user");
                }
            }
        });
        bt_page4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (notification_message.equals("")) {
                    Toast.makeText(SendNotificationActivity.this, "Select Notification from List", Toast.LENGTH_SHORT);
                }else {
                    sendNotification("page4", notification_message, "user");
                }
            }
        });
        bt_page5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (notification_message.equals("")) {
                    Toast.makeText(SendNotificationActivity.this, "Select Notification from List", Toast.LENGTH_SHORT);
                } else {
                    sendNotification("page5", notification_message, "user");
                }
            }
        });
        bt_page6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (notification_message.equals("")) {
                    Toast.makeText(SendNotificationActivity.this, "Select Notification from List", Toast.LENGTH_SHORT);
                } else {
                    sendNotification("page6", notification_message, "user");
                }
            }
        });
        bt_page7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (notification_message.equals("")) {
                    Toast.makeText(SendNotificationActivity.this, "Select Notification from List", Toast.LENGTH_SHORT);
                } else {
                    sendNotification("page7", notification_message, "user");
                }
            }
        });
        bt_page8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (notification_message.equals("")) {
                    Toast.makeText(SendNotificationActivity.this, "Select Notification from List", Toast.LENGTH_SHORT);
                } else {
                    sendNotification("page8", notification_message, "user");
                }
            }
        });
        bt_page9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (notification_message.equals("")) {
                    Toast.makeText(SendNotificationActivity.this, "Select Notification from List", Toast.LENGTH_SHORT);
                } else {
                    sendNotification("page9", notification_message, "user");
                }
            }
        });
        //end pageing button

        bt_sendshop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (notification_message.equals("")) {
                    Toast.makeText(SendNotificationActivity.this, "Select Notification from List", Toast.LENGTH_SHORT);
                }else {
                    sendNotification("page4", notification_message, "shop");
                }
            }
        });

        btn_user_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (notification_message.equals("")) {
                    Toast.makeText(SendNotificationActivity.this, "Select Notification from List", Toast.LENGTH_SHORT);
                }else {
                    sendNotification("testuser", notification_message, "user");
                }
            }
        });
        btn_shop_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (notification_message.equals("")) {
                    Toast.makeText(SendNotificationActivity.this, "Select Notification from List", Toast.LENGTH_SHORT);
                }else {
                    sendNotification("testshop", notification_message, "shop");
                }
            }
        });

        //Get notification list
        getNotificationList();

        btn_uploadNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog();
            }
        });

        recycler_view_notification.addOnItemTouchListener(new RecyclerTouchListener(SendNotificationActivity.this, recycler_view_notification, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                notification_message = notificationListModelArrayList.get(position).getMessage();
                String imageUrl = notificationListModelArrayList.get(position).getImageUrl();
                if (imageUrl.equals("")) {
                    imageStatusUrl = "no";
                } else {
                    imageStatusUrl = "yes";
                    imageNotifurl = imageUrl;
                }
                flagstatus = notificationListModelArrayList.get(position).getFlag();
                tv_selected_notification.setText(notification_message);
                Toast.makeText(SendNotificationActivity.this, "Notification Selected", Toast.LENGTH_SHORT);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void select_images(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE);
        Log.e("Test Commit","Test Comit 2/7/2020");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE) {
            //TODO: action
            // Get the url from data
            Uri selectedImageUri = data.getData();
            //for bit map data
            try {
                   /* Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    thumbnail.compress(Bitmap.CompressFormat.PNG, 100, bytes);
                    shop_photo1.setImageBitmap(thumbnail);
                    shopbitmap=thumbnail;*/
                //Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(SendNotificationActivity.this.getContentResolver(),selectedImageUri);
                //Deliveryboy_image.setImageBitmap(bitmap);
                notificatinBitmap = bitmap;
                Log.e("shopbitmap", "" + notificatinBitmap);

            } catch (Exception e) {
                e.getMessage();
                Log.e("BitmapExp", "" + e.getMessage());
            }
        }
    }

    private void customDialog() {
        final Dialog dialog = new Dialog(SendNotificationActivity.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);

        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_notificationlist);

        final EditText et_message = dialog.findViewById(R.id.et_message);
        CheckBox cb_selectImage = dialog.findViewById(R.id.cb_selectImage);
        CheckBox cb_selectOffer = dialog.findViewById(R.id.cb_selectOffer);
        final Button btn_select_img = dialog.findViewById(R.id.btn_select_img);
        Button btn_upload_notification = dialog.findViewById(R.id.btn_upload_notification);
        final TextView tv_image_url = dialog.findViewById(R.id.tv_image_url);

        btn_select_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                select_images();
            }
        });

        cb_selectImage.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    imageStatus = "yes";
                    btn_select_img.setVisibility(View.VISIBLE);
                    tv_image_url.setVisibility(View.VISIBLE);
                } else {
                    imageStatus = "no";
                    btn_select_img.setVisibility(View.GONE);
                    tv_image_url.setVisibility(View.GONE);
                }
            }
        });

        cb_selectOffer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    flagstatus = "yes";
                } else {
                    flagstatus = "no";
                }
            }
        });

        btn_upload_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!et_message.getText().toString().equals("")) {
                    dialog.dismiss();
                    uploadNotificatoinData(et_message.getText().toString(), flagstatus);
                } else {
                    Toast.makeText(SendNotificationActivity.this, "Enter Message", Toast.LENGTH_SHORT);
                }
            }
        });

        dialog.show();
    }

    public void uploadNotificatoinData(final String message, final String flagstatus) {

        if(Connectivity.isConnected(SendNotificationActivity.this)){
            progressDialog.show();
            String targetUrl = StaticUrl.uploadNotificationImage;
            VolleyMultipartRequest notificationImageRequest = new VolleyMultipartRequest(Request.Method.POST,
                    targetUrl,
                    new Response.Listener<NetworkResponse>() {
                        @Override
                        public void onResponse(NetworkResponse response) {
                            progressDialog.dismiss();
                            String resultResponse = new String(response.data);
                            Log.d("resultResponse", resultResponse);
                            try {
                                JSONObject jsonObject = new JSONObject(resultResponse);
                                String status = jsonObject.getString("status");
                                if(status.equals("true")){
                                    //JSONObject jsonObjectImage = jsonObject.getJSONObject("data");
                                    //String notificationImageUrl = jsonObjectImage.getString("imageurl");
                                    //tv_image_url.setText(notificationImageUrl);
                                    //imageNotifurl = notificationImageUrl;
                                    getNotificationList();

                                } if(status.equals("false")){
                                    //tv_image_url.setText("Image does not Uploaded, Please try Again");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                            NetworkResponse networkResponse = error.networkResponse;

                            String errorMessage = "Unknown error";
                            if (networkResponse == null) {
                                if (error.getClass().equals(TimeoutError.class)) {
                                    errorMessage = "Request timeout";

                                } else if (error.getClass().equals(NoConnectionError.class)) {
                                    errorMessage = "Failed to connect server";
                                }
                            } else {
                                String result = new String(networkResponse.data);
                                try {
                                    JSONObject response = new JSONObject(result);

                                    Log.d("MultipartErrorRES:", "" + response.toString());
                                    String status = response.getString("status");
                                    String message = response.getString("data");
                                    Log.e("Error Status", status);
                                    Log.e("Error Message", message);
                                    if (networkResponse.statusCode == 404) {
                                        errorMessage = "Resource not found";
                                    } else if (networkResponse.statusCode == 401) {
                                        errorMessage = message + " Please try again";
                                    } else if (networkResponse.statusCode == 400) {
                                        errorMessage = message + " Check your inputs";
                                    } else if (networkResponse.statusCode == 500) {
                                        errorMessage = message + " Something is getting wrong";
                                    }
                                } catch (JSONException e) {
                                    //progressDialog.dismiss();
                                    e.printStackTrace();
                                }
                            }
                            Log.i("Error", errorMessage);
                            error.printStackTrace();
                        }
                    }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("message", message);
                    params.put("flag", flagstatus);
                    return params;
                }

                @Override
                protected Map<String, DataPart> getByteData() throws AuthFailureError {
                    Map<String, DataPart> params = new HashMap<>();
                    if (notificatinBitmap != null) {
                        params.put("imageurl", new DataPart(getByteFromBitmap(notificatinBitmap)));
                    }
                    return params;
                }
            };
            VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(notificationImageRequest);
        }else {
            Toast.makeText(SendNotificationActivity.this,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }
    public byte[] getByteFromBitmap(Bitmap bitmap){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100,stream);
        return stream.toByteArray();
    }

    private void getNotificationList() {
        progressDialog.show();
        String urlGetNotificationUrl = StaticUrl.offernotificatonlist;
        StringRequest stringRequestNotification = new StringRequest(Request.Method.GET,
                urlGetNotificationUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Log.d("getNotificationList", response);
                        try {
                            notificationListModelArrayList.clear();
                            JSONObject jsonObject = new JSONObject(response);
                            //boolean status = jsonObject.getBoolean("status");
                            //if(status){
                            JSONArray jsonArray = jsonObject.getJSONArray("posts");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObjectN = jsonArray.getJSONObject(i);
                                NotificationListModel model = new NotificationListModel();
                                model.setId(jsonObjectN.getString("id"));
                                model.setMessage(jsonObjectN.getString("message"));
                                model.setFlag(jsonObjectN.getString("flag"));
                                model.setImageUrl(jsonObjectN.getString("imageurl"));
                                model.setStatus(jsonObjectN.getString("status"));
                                notificationListModelArrayList.add(model);
                            }
                            //}
                            notificationListAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        VolleySingleton.getInstance(SendNotificationActivity.this).addToRequestQueue(stringRequestNotification);
    }

    private void sendNotification(String page, String message, String user) { //TODO Server method here

        progressDialog.show();
        String urlSendNotification =StaticUrl.sendNotificationFromAdmin
                +"?page="+page
                +"&body="+message
                +"&type="+user
                +"&imageNotificationUrl="+imageNotifurl
                + "&imageStatus=" + imageStatusUrl
                + "&flagStatus=" + flagstatus;
        Log.e("urlSendNotification", urlSendNotification);
        StringRequest  stringRequest = new StringRequest(Request.Method.GET,
                urlSendNotification,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();

                        Log.e("sendnotif",response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            success_count.setText("Success: "+ jsonObject.getInt("success"));
                            fail_count.setText("Failure: "+ jsonObject.getInt("failure"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                });
        VolleySingleton.getInstance(SendNotificationActivity.this).addToRequestQueue(stringRequest);
    }
    private void hideKeyBoard() {
        View view = getCurrentFocus();  // Check if no view has focus:
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private SendNotificationActivity.RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final SendNotificationActivity.RecyclerTouchListener.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
        }

        public interface ClickListener {
            void onClick(View view, int position);

            void onLongClick(View view, int position);
        }
    }

}
