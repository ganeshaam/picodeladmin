package com.example.picodeladmin;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;

public class OrderlistAdapter extends RecyclerView.Adapter<OrderlistAdapter.OrderlistHolder> {

    ArrayList<OrderlistModel> orderArrayList;
    Context mContext;
    String flag;


    public OrderlistAdapter(ArrayList<OrderlistModel> orderArrayList, Context mContext,String flag) {
        this.orderArrayList = orderArrayList;
        this.mContext = mContext;
        this.flag= flag;
    }

    @NonNull
    @Override
    public OrderlistAdapter.OrderlistHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.new_order_list_items,parent,false);
        OrderlistAdapter.OrderlistHolder orderlistHolder = new OrderlistAdapter.OrderlistHolder(view);
        return orderlistHolder;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull OrderlistAdapter.OrderlistHolder orderlistHolder, int position) {

            TextView tv_orderid = orderlistHolder.tv_orderid;
            TextView tv_order_from = orderlistHolder.tv_order_from;
            TextView tv_orderamount = orderlistHolder.tv_orderamount;
            TextView tv_status= orderlistHolder.tv_status;
            TextView tv_orderaddress= orderlistHolder.tv_orderaddress;
            TextView tv_orderdatetime= orderlistHolder.tv_orderdatetime;
            ImageView iv_location = orderlistHolder.iv_location;
            TextView tv_approveBy = orderlistHolder.tv_approveBy;
            TextView tv_userName = orderlistHolder.tv_userName;
            TextView tv_userCount = orderlistHolder.tv_userCount;
            //,tv_orderdistance= orderlistHolder.tv_orderid;
            TextView tv_ordercategory = orderlistHolder.tv_ordercategory;
            TextView tv_orderDelDateTime = orderlistHolder.tv_orderDelDateTime;
            RelativeLayout rl_newOrder = orderlistHolder.rl_newOrder;

        tv_orderid.setText("OrderId: "+orderArrayList.get(position).getOrder_id());
        tv_order_from.setText("Order From: "+orderArrayList.get(position).getOrder_from());
        tv_orderamount.setText("Order Amount: "+orderArrayList.get(position).getTotal_amount()+" "+ mContext.getString(R.string.Rs));
        if(orderArrayList.get(position).getSessionMainCat().equalsIgnoreCase("Grocery")){
            tv_orderid.setBackgroundColor(Color.parseColor("#FF9800"));
        }else {
            tv_orderid.setBackgroundColor(Color.parseColor("#8ab935"));
        }
        tv_userName.setText("User Name: "+orderArrayList.get(position).getUser_fname()+" "+orderArrayList.get(position).getUser_lname());
        tv_userCount.setText("("+orderArrayList.get(position).getOrder_count()+")");

        if(flag.equalsIgnoreCase("admin")){
            if (orderArrayList.get(position).getOrder_count().equalsIgnoreCase("1")) {
                tv_userCount.setBackgroundColor(Color.parseColor("#FF9800"));
            }
        }else{

        }

        //set status of order
        if(orderArrayList.get(position).getAdmin_approve().equals("Not Approved"))
        {
            if(orderArrayList.get(position).getStatus().equals("Cancelled by User"))
             {
                tv_status.setText("Cancelled by User");
            }else if(orderArrayList.get(position).getStatus().equals(""))
                {
                tv_status.setText("Not Accepted");
            }
        }else  if(orderArrayList.get(position).getAdmin_approve().equals("Approved"))
        {
            if(orderArrayList.get(position).getStatus().equals("Cancelled by User"))
            {
                tv_status.setText("Cancelled by User");
            }else  if(orderArrayList.get(position).getStatus().equals("Delivered"))
            {
                tv_status.setText("Delivered");
            }else{
                tv_status.setText("Accepted");
            }
        }/*else if((orderArrayList.get(position).getAdmin_approve().equals("Approved")) && (orderArrayList.get(position).getStatus().equals("Delivered"))){
            tv_status.setText("Delivered");
        }else if(orderArrayList.get(position).getAdmin_approve().equals("Approved") || orderArrayList.get(position).getStatus().equals("")){
                  tv_status.setText("Accepted");
        }else if(orderArrayList.get(position).getStatus().equals("Cancelled by User") && orderArrayList.get(position).getAdmin_approve().equals("Not Approved")){
            tv_status.setText("Cancelled by User");
        }else if(orderArrayList.get(position).getStatus().equals("Cancelled by User") && orderArrayList.get(position).getAdmin_approve().equals("Approved")){
            tv_status.setText("Cancelled by User");
        }else if((orderArrayList.get(position).getAdmin_approve().equals("Approved")) && (orderArrayList.get(position).getStatus().equals("Delivered"))){
            tv_status.setText("Delivered");
        }*/

        //tv_status.setText(orderArrayList.get(position).getAdmin_approve()+"-"+orderArrayList.get(position).getStatus());

        tv_orderdatetime.setText("Order DateTime: "+ mContext.getString(R.string.Rs)+" "+orderArrayList.get(position).getOrder_date());

        tv_orderDelDateTime.setText("Order Delivery Date: "+orderArrayList.get(position).getDelivery_date() +" Time: "+orderArrayList.get(position).getDelivery_time());

        tv_orderaddress.setText(""+orderArrayList.get(position).getAddress1()
                +" "+orderArrayList.get(position).getAddress2()
                +"\n"+orderArrayList.get(position).getAddress3()
                +" "+orderArrayList.get(position).getAddress4()
                +"\nArea: "+orderArrayList.get(position).getArea());

        tv_ordercategory.setText("Order Category: "+orderArrayList.get(position).getSessionMainCat());

        //tv_approveBy.setText("Approve By: "+orderArrayList.get(position).getApproveBy());
        tv_approveBy.setText("Approve By: "+orderArrayList.get(position).getShop_name()+"("+orderArrayList.get(position).getRecharge_points() +")"+"\n"+"Seller Name:"+orderArrayList.get(position).getSellername()
                + " "+"Contact:"+orderArrayList.get(position).getShop_contact());

        if(orderArrayList.get(position).getLatitude().equals("")){

            rl_newOrder.setBackgroundColor(Color.parseColor("#F44336"));
        }
    }

    @Override
    public int getItemCount() {
        return orderArrayList.size();
    }

    public static class OrderlistHolder extends RecyclerView.ViewHolder {
        TextView tv_orderid,tv_status,tv_orderaddress,tv_orderdatetime,tv_approveBy,tv_order_from,
                tv_ordercategory,tv_orderDelDateTime,tv_userName,tv_userCount,tv_orderamount;//,tv_orderdistance
        ImageView iv_location;
        RelativeLayout rl_newOrder;
        public OrderlistHolder(@NonNull View itemView) {
            super(itemView);
            tv_orderid = itemView.findViewById(R.id.tv_orderid);
            tv_status = itemView.findViewById(R.id.tv_status);
            tv_orderaddress = itemView.findViewById(R.id.tv_orderaddress);
            tv_orderdatetime = itemView.findViewById(R.id.tv_orderdatetime);
            tv_approveBy = itemView.findViewById(R.id.tv_approveBy);
            tv_userName = itemView.findViewById(R.id.tv_userName);
            tv_userCount = itemView.findViewById(R.id.tv_userCount);
            //,tv_orderdistance = itemView.findViewById(R.id.tv_orderid);
            tv_ordercategory = itemView.findViewById(R.id.tv_ordercategory);
            tv_orderDelDateTime= itemView.findViewById(R.id.tv_orderDelDateTime);
            rl_newOrder = itemView.findViewById(R.id.rl_newOrder);
            tv_orderamount = itemView.findViewById(R.id.tv_orderamount);
            tv_order_from = itemView.findViewById(R.id.tv_order_from);
        }
    }
}
