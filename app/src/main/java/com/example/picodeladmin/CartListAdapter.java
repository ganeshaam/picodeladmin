package com.example.picodeladmin;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CartListAdapter extends RecyclerView.Adapter<CartListAdapter.CarlistHolder>{

    ArrayList<CartitemModel> cartArrayList;
    Context mContext;

    public CartListAdapter(ArrayList<CartitemModel> cartArrayList, Context mContext) {
        this.cartArrayList = cartArrayList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public CarlistHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_cart_item_layout,parent,false);
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_cart_item_cancel_layout,parent,false);
        CartListAdapter.CarlistHolder carlistHolder = new  CartListAdapter.CarlistHolder(view);
        return carlistHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CarlistHolder carlistHolder, int position) {

        final LinearLayout layout = carlistHolder.ll_not_avail;
        TextView tv_productName = carlistHolder.tv_productName;
        TextView tv_productPrice = carlistHolder.tv_productPrice;
        TextView tv_productQty = carlistHolder.tv_productQty;
        TextView tv_productBrand = carlistHolder.tv_productBrand;
        ImageView iv_productImage =  carlistHolder.iv_productImage;
        final ImageView item_cancel = carlistHolder.iv_item_cancel;
        final ImageView item_checked = carlistHolder.iv_item_checked;

        String imgProductUrl = "https://www.simg.picodel.com/"+cartArrayList.get(position).getProduct_image();
        Picasso.with(mContext).load(imgProductUrl)
                .placeholder(R.drawable.loading)
                .error(R.drawable.loading)
                .into(iv_productImage);
        tv_productBrand.setText(cartArrayList.get(position).getBrand_name());
        tv_productName.setText(cartArrayList.get(position).getProduct_name());
        //tv_productPrice.setText("MRP: "+cartArrayList.get(position).getProduct_mrp() +"\nSelling Price: "+ cartArrayList.get(position).getItem_price());
        tv_productPrice.setText("MRP: "+cartArrayList.get(position).getProduct_mrp() +"\nSelling Price: "+ cartArrayList.get(position).getItem_price()+" Quantity: "+cartArrayList.get(position).getIten_qty());
        Double item_price = Double.valueOf(cartArrayList.get(position).getItem_price());
        Double item_qty = Double.valueOf(cartArrayList.get(position).getIten_qty());
        Log.e("updated_status:",""+cartArrayList.get(position).getUpdated_staus());

        if(cartArrayList.get(position).getUpdated_staus().equalsIgnoreCase("old")){
            item_cancel.setVisibility(View.VISIBLE);
        }else if(cartArrayList.get(position).getUpdated_staus().equalsIgnoreCase("new")){
            item_checked.setVisibility(View.VISIBLE);
        }

        //Double total = item_qty * item_price;
        //tv_productQty.setText("  Total: "+total);
        try {
            Double total = item_qty * item_price;
            String.format("%.2f", total);
            tv_productQty.setText("  Total: " + total);
        }catch (Exception e){
            e.getMessage();
        }
    }

    @Override
    public int getItemCount() {
        return cartArrayList.size();
    }

    public static class CarlistHolder extends RecyclerView.ViewHolder {
        LinearLayout ll_not_avail;
        ImageView iv_productImage,iv_item_cancel,iv_item_checked;
        TextView tv_productName,tv_productPrice,tv_productQty,tv_productBrand;

        public CarlistHolder(@NonNull View itemView) {
            super(itemView);
            tv_productName = itemView.findViewById(R.id.tv_productName);
            tv_productPrice = itemView.findViewById(R.id.tv_productPrice);
            tv_productQty = itemView.findViewById(R.id.tv_productQty);
            iv_productImage= itemView.findViewById(R.id.iv_productimage);
            tv_productBrand= itemView.findViewById(R.id.tv_productBrand);
            ll_not_avail = itemView.findViewById(R.id.ll_not_avail);
            iv_item_checked =itemView.findViewById(R.id.iv_item_checked);
            iv_item_cancel = itemView.findViewById(R.id.iv_item_cancel);
        }
    }
}
