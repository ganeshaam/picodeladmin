package com.example.picodeladmin;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

public class OtpVerifyActivity extends AppCompatActivity {

    Context mContext;
    String shopadmin_id,admin_type,sellername,shop_id,email,sellerstatus,moibleno,vcode,vdate,firebaseTokan,fc_city,admin_id;
    ProgressDialog progressDialog;
    Button btn_verifyotp;
    TextView tv_resend;
    EditText et_verifyotp;
    SharedPreferencesUtils sharedPreferencesUtils;
    Locale local;

    private static final int PERMISSION_ACCESS_COARSE_LOCATION = 1;
    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private LocationManager locationManager,mLocationManager;
    private LocationRequest mLocationRequest;
    String  latitude,longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verify);

        mContext = OtpVerifyActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
        btn_verifyotp = findViewById(R.id.btn_verifyotp);
        tv_resend = findViewById(R.id.tv_resend);
        et_verifyotp = findViewById(R.id.et_verifyotp);
        progressDialog = new ProgressDialog(mContext);


        Intent intent = getIntent();
        if(intent!=null){
            email = intent.getStringExtra("email");
            moibleno = intent.getStringExtra("contact");
            vcode = intent.getStringExtra("vcode");
            vdate = intent.getStringExtra("vdate");
            shop_id = intent.getStringExtra("shop_id");
            shopadmin_id = intent.getStringExtra("shopadmin_id");
            sellername = intent.getStringExtra("sellername");
            admin_type = intent.getStringExtra("admin_type");
            sellerstatus = intent.getStringExtra("shop_status");
            fc_city = intent.getStringExtra("fc_city");
            //admin_id = intent.getStringExtra("admin_id");
        }

        btn_verifyotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyOtp();
            }
        });
        tv_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reSendOtp();
            }
        });

        // Get token
        // [START retrieve_current_token]
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("firebaseTokan", "getInstanceId failed", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        firebaseTokan = task.getResult().getToken();
                        // Log and toast
                        //String msg = getString(R.string.msg_token_fmt, token);
                        Log.d("firebaseTokan", firebaseTokan);
                        //Toast.makeText(RegistrationActivity.this, firebaseTokan, Toast.LENGTH_SHORT).show();
                    }
                });
        // [END retrieve_current_token]


        /*Get Current Location */


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.ACCESS_COARSE_LOCATION , Manifest.permission.ACCESS_FINE_LOCATION }, PERMISSION_ACCESS_COARSE_LOCATION);
        }


    }



    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                    }
                });
        dialog.show();
    }



    public void verifyOtp(){

       /* progressDialog.setMessage("Please wait...");
         progressDialog.setCancelable(false);
         progressDialog.show();*/

        if(et_verifyotp.getText().toString().equals("")) {
          //  progressDialog.dismiss();
            Toast.makeText(mContext,"Please Enter OTP", Toast.LENGTH_SHORT).show();
        }else {
            if(vcode.equals(et_verifyotp.getText().toString().trim())){
                updateFirebaseToken();
            }else {
                Toast.makeText(mContext,"Invalid OTP", Toast.LENGTH_SHORT).show();
            }
        }
    }
    //update shop firebase token
    public void updateFirebaseToken(){

        if(Connectivity.isConnected(mContext)){
           progressDialog.show();

            String urlManagerFirebaseToken = StaticUrl.updateManagertoken
                    +"?shop_id="+shop_id
                    +"&shopadmin_id="+shopadmin_id
                    +"&firebase_token="+firebaseTokan;

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlManagerFirebaseToken,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status=true){
                                    JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    //String vcode = jsonObjectData.getString("shop_id");
                                    sharedPreferencesUtils.setEmailId(email);
                                    sharedPreferencesUtils.setShopID(shopadmin_id);
                                    sharedPreferencesUtils.setPhoneNumber(moibleno);
                                    sharedPreferencesUtils.setUserName(sellername);
                                    sharedPreferencesUtils.setAdminType(admin_type);
                                    sharedPreferencesUtils.setShopStatus(sellerstatus);
                                    sharedPreferencesUtils.setLoginFlag(true);
                                    sharedPreferencesUtils.setManagerId(shop_id);
                                    sharedPreferencesUtils.setFC_City(fc_city);
                                    sharedPreferencesUtils.setLocal("en_US");
                                    Toast.makeText(mContext,"Thanks, Verification Successfully", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(mContext, MainActivity.class);
                                    intent.putExtra("areaName","areaName");
                                    intent.putExtra("areaWise_order","admin");
                                    intent.putExtra("category","Grocery");

                                    //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(intent);
                                    finishAffinity();
                                }else if(status=true){
                                    Toast.makeText(mContext,"Check Internet Connection", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    //update manager firebase token
    /*public void updateManagerFirebaseToken(){
        if(Connectivity.isConnected(mContext)){

            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            String urlManagerFirebaseToken = StaticUrl.updateManagertoken
                    +"?shop_id="+shop_id
                    +"&shopadmin_id="+managerID
                    +"&firebase_token="+firebaseTokan;

        Log.d("urlManagerFirebaseToken",urlManagerFirebaseToken);

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlManagerFirebaseToken,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status==true){
                                    JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    sharedPreferencesUtils.setEmailId(emailid);
                                    sharedPreferencesUtils.setShopID(shop_id);
                                    sharedPreferencesUtils.setPhoneNumber(moibleno);
                                    sharedPreferencesUtils.setLatitude(shop_latitude);
                                    sharedPreferencesUtils.setLongitude(shop_longitude);
                                    sharedPreferencesUtils.setUserName(seller_name);
                                    sharedPreferencesUtils.setDistancekm(shop_distanceKm);
                                    sharedPreferencesUtils.setAdminType(admin_type);
                                    sharedPreferencesUtils.setShopStatus(shop_status);
                                    sharedPreferencesUtils.setLoginFlag(true);
                                    sharedPreferencesUtils.setDelId(deliveryboyID);
                                    sharedPreferencesUtils.setLocal("en_US");
                                    sharedPreferencesUtils.setShopName(shopName);
                                    sharedPreferencesUtils.setManagerId(managerID);
                                    sharedPreferencesUtils.setShopAdminType(shopAdminType);

                                    Toast.makeText(mContext,"Thanks, Verification Successfully", Toast.LENGTH_SHORT).show();

                                    Intent intent = new Intent(mContext, HomeActivity.class);
                                    //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NO_HISTORY|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    finishAffinity();
                                }else if(status==true){
                                    Toast.makeText(mContext,"Check Internet Connection", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }*/
    //send otp
    public void reSendOtp(){
        if(Connectivity.isConnected(mContext)){

            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            String urlOtpsend = StaticUrl.sendotp
                    +"?mobileno="+moibleno
                    +"&email="+email;

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlOtpsend,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status=true){
                                    JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    String vcode = jsonObjectData.getString("vcode");
                                    String email = jsonObjectData.getString("email");
                                    String contact = jsonObjectData.getString("contact");
                                    String vdate = jsonObjectData.getString("vdate");

                                    Intent intent = new Intent(mContext,OtpVerifyActivity.class);
                                    intent.putExtra("vcode",vcode);
                                    intent.putExtra("email",email);
                                    intent.putExtra("contact",contact);
                                    intent.putExtra("vdate",vdate);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    //finish();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }



}
