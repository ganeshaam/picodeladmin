package com.example.picodeladmin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import dmax.dialog.SpotsDialog;

public class OrderHistoryActivity extends AppCompatActivity {//implements SwipeRefreshLayout.OnRefreshListener

    Context mContext;
    AlertDialog progressDialog;
    private RecyclerView.LayoutManager layoutManager;
    ArrayList<OrderlistModel> orderArrayList;
    private OrderlistAdapter orderlistAdapter;
    RecyclerView rv_home_orderlist;
    String strLatitude="",strLongitude="",strDeliveryinkm="",logintype="",flag="adminShop",appversion,strMonth,strDay;
    TextView tv_network_con,tv_selecteddate,tv_selectedenddate;
   // SwipeRefreshLayout srl_refreshorderlist;
    private DatePicker datePicker;
    private Calendar calendar;
    private int year, month, day,time;
    private String gender,mobile_no;
    private DatePickerDialog datePickerDialog;
    private DatePickerDialog datePickerDialogEndDate;
    ImageView btn_calender,btn_enddate;
    String startDate,endDate;
    Button btn_submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_histroy);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setTitle("Order History");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = OrderHistoryActivity.this;

        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();

        tv_network_con = findViewById(R.id.tv_network_con);
        rv_home_orderlist = findViewById(R.id.rv_home_orderlist);
        orderArrayList = new ArrayList<>();
        rv_home_orderlist.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(mContext);
        rv_home_orderlist.setLayoutManager(layoutManager);
        rv_home_orderlist.setItemAnimator(new DefaultItemAnimator());
        orderlistAdapter = new OrderlistAdapter(orderArrayList,mContext,flag);
        rv_home_orderlist.setAdapter(orderlistAdapter);
        //srl_refreshorderlist = findViewById(R.id.srl_refreshorderlist);
        btn_calender = findViewById(R.id.btn_calender);
        btn_enddate = findViewById(R.id.btn_enddate);
        tv_selecteddate = findViewById(R.id.tv_selecteddate);
        btn_submit = findViewById(R.id.btn_submit);
        tv_selectedenddate = findViewById(R.id.tv_selectedenddate);
       // srl_refreshorderlist.setOnRefreshListener(this);


        //order details
        rv_home_orderlist.addOnItemTouchListener(new RecyclerTouchListener(mContext, rv_home_orderlist,
                new RecyclerTouchListener.ClickListener()
                {
                    @Override
                    public void onClick(View view, int position)
                    {
                        String orderId = orderArrayList.get(position).getOrder_id();
                        String cartUniqueId = orderArrayList.get(position).getCart_unique_id();
                        String orderByUserId = orderArrayList.get(position).getUser_id();
                        String cartUserId = orderArrayList.get(position).getCart_user_id();
                        String shipping_address = orderArrayList.get(position).getShipping_address();
                        String latitude = orderArrayList.get(position).getLatitude();
                        String longitude = orderArrayList.get(position).getLongitude();
                        String total_amount = orderArrayList.get(position).getTotal_amount();
                        String order_date  = orderArrayList.get(position).getOrder_date();
                        String address_id = orderArrayList.get(position).getAddress_id();
                        String assign_delboy_id = orderArrayList.get(position).getAssign_delboy_id();
                        String action_time = orderArrayList.get(position).getActionTime();
                        String address1 = orderArrayList.get(position).getAddress1();
                        String address2 = orderArrayList.get(position).getAddress2();
                        String address3 = orderArrayList.get(position).getAddress3();
                        String address4 = orderArrayList.get(position).getAddress4();
                        String address5 = orderArrayList.get(position).getAddress5();
                        String area = orderArrayList.get(position).getArea();
                        String pincode = orderArrayList.get(position).getPincode();
                        String showcart = orderArrayList.get(position).getShow_cart();
                        String updatedat = orderArrayList.get(position).getUpdated_at();
                        String aam_delivery_charge = orderArrayList.get(position).getAam_delivery_charge();
                        String delivery_date = orderArrayList.get(position).getDelivery_date();
                        String delivery_time = orderArrayList.get(position).getDelivery_time();
                        String delivery_method = orderArrayList.get(position).getDelivery_method();
                        String payment_option = orderArrayList.get(position).getPayment_option();
                        String sessionMainCat = orderArrayList.get(position).getSessionMainCat();
                        String assignShopId = orderArrayList.get(position).getAssign_shop_id();
                        //chkOrderStatus(orderId,cartUniqueId,orderByUserId,cartUserId,cartUserId,shipping_address,latitude,longitude,total_amount,order_date,address_id,assign_delboy_id,action_time);

                        Intent intent = new Intent(mContext,OrderDetailActivity.class);
                        intent.putExtra("orderId",orderId);
                        intent.putExtra("cartUniqueId",cartUniqueId);
                        intent.putExtra("userId",orderByUserId);
                        intent.putExtra("cartUserId",cartUserId);
                        intent.putExtra("shipping_address",shipping_address);
                        intent.putExtra("latitude",latitude);
                        intent.putExtra("longitude",longitude);
                        intent.putExtra("total_amount",total_amount);
                        intent.putExtra("order_date",order_date);
                        intent.putExtra("address_id",address_id);
                        intent.putExtra("assign_delboy_id",assign_delboy_id);
                        intent.putExtra("action_time",action_time);
                        intent.putExtra("address1",address1);
                        intent.putExtra("address2",address2);
                        intent.putExtra("address3",address3);
                        intent.putExtra("address4",address4);
                        intent.putExtra("address5",address5);
                        intent.putExtra("area",area);
                        intent.putExtra("pincode",pincode);
                        intent.putExtra("showcart",showcart);
                        intent.putExtra("flag",flag);
                        intent.putExtra("updatedat",updatedat);
                        intent.putExtra("aam_delivery_charge",aam_delivery_charge);
                        intent.putExtra("areaName","areaName");
                        intent.putExtra("delivery_date",delivery_date);
                        intent.putExtra("delivery_time",delivery_time);
                        intent.putExtra("delivery_method",delivery_method);
                        intent.putExtra("payment_option",payment_option);
                        intent.putExtra("sessionMainCat",sessionMainCat);
                        intent.putExtra("assignShopId",assignShopId);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(intent);
                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {
                        Toast.makeText(mContext,"No Records",Toast.LENGTH_SHORT).show();
                    }
                }));

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        btn_calender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDate();
            }
        });
        btn_enddate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDateEnd();
            }
        });
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Connectivity.isConnected(mContext)){
                    getOrderList(startDate,endDate);//2019-08-13
                }else {
                    tv_network_con.setVisibility(View.VISIBLE);
                }
            }
        });

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        startDate = format.format(calendar.getTime());

        if(Connectivity.isConnected(mContext)){
            getOrderList(startDate,endDate);//2019-08-13
        }else {
            tv_network_con.setVisibility(View.VISIBLE);
        }

    }

    public void getOrderList(String strDate,String endDate){

        progressDialog.show();
        orderArrayList.clear();
        //srl_refreshorderlist.setRefreshing(true);

        String targetUrlOrderlist = null;
        try {
            targetUrlOrderlist = StaticUrl.orderlist
                    + "?latitude=" + strLatitude
                    + "&longitude=" + strLongitude
                    + "&deliveryinkm=" + strDeliveryinkm
                    + "&shopid=78"
                    + "&delboyid=0"
                    + "&areaName=areaName"
                    + "&flag=" + "adminShop"

                    + "&page=" + "0"
                    + "&back=" + "false"
                    + "&category=" + URLEncoder.encode("category","utf-8")


                    + "&tblName="+ URLEncoder.encode("Seller")
                    + "&orderdate="+strDate
                    + "&endDate="+endDate;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        Log.d("targetUrlOrderlist",targetUrlOrderlist);

        StringRequest orderListRequest = new StringRequest(Request.Method.GET,
                targetUrlOrderlist,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("orderlist_history",response);
                        //cv_viewmore.setVisibility(View.VISIBLE);
                        //srl_refreshorderlist.setRefreshing(false);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean strStatus = jsonObject.getBoolean("status");
                            if ((strStatus=true)){
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if(jsonArray.length()>0){

                                    for(int i = 0; i < jsonArray.length(); i++){
                                        JSONObject olderlist = jsonArray.getJSONObject(i);
                                        OrderlistModel model = new OrderlistModel();
                                        model.setOrder_id(olderlist.getString("order_id"));
                                        model.setUser_id(olderlist.getString("user_id"));
                                        model.setAssign_shop_id(olderlist.getString("assign_shop_id"));
                                        model.setAssign_delboy_id(olderlist.getString("assign_delboy_id"));
                                        model.setStatus(olderlist.getString("status"));
                                        model.setCart_user_id(olderlist.getString("cart_user_id"));
                                        model.setCart_unique_id(olderlist.getString("cart_unique_id"));
                                        model.setPayment_method(olderlist.getString("payment_method"));
                                        model.setUpdate_payment_method(olderlist.getString("update_payment_method"));
                                        model.setPayment_type(olderlist.getString("payment_type"));
                                        model.setReferrence_transaction_id(olderlist.getString("referrence_transaction_id"));
                                        model.setTotal_amount(olderlist.getString("total_amount"));
                                        model.setUpdated_total_amount(olderlist.getString("updated_total_amount"));
                                        model.setShipping_address(olderlist.getString("shipping_address"));
                                        model.setLatitude(olderlist.getString("latitude"));
                                        model.setLongitude(olderlist.getString("longitude"));
                                        model.setUpdated_shipping_charges(olderlist.getString("updated_shipping_charges"));
                                        model.setOrder_date(olderlist.getString("order_date"));
                                        model.setUser_pending_bill(olderlist.getString("user_pending_bill"));
                                        model.setPending_bill_status(olderlist.getString("pending_bill_status"));
                                        model.setPending_bill_updatedy(olderlist.getString("pending_bill_updatedy"));
                                        model.setPending_bii_date(olderlist.getString("pending_bii_date"));
                                        model.setDistance(olderlist.getString("distance"));
                                        model.setAddress_id(olderlist.getString("address_id"));
                                        model.setActionTime(olderlist.getString("action_time"));
                                        model.setAddress1(olderlist.getString("address1"));
                                        model.setAddress2(olderlist.getString("address2"));
                                        model.setAddress3(olderlist.getString("address3"));
                                        model.setAddress4(olderlist.getString("address4"));
                                        model.setAddress5(olderlist.getString("address5"));
                                        model.setArea(olderlist.getString("area"));
                                        model.setPincode(olderlist.getString("pincode"));
                                        model.setShow_cart(olderlist.getString("show_cart"));
                                        model.setDelivery_date(olderlist.getString("delivery_date"));
                                        model.setDelivery_time(olderlist.getString("delivery_time"));
                                        model.setDelivery_method(olderlist.getString("delivery_method"));
                                        model.setAdmin_approve(olderlist.getString("admin_approve"));
                                        model.setApproveBy(olderlist.getString("approveBy"));
                                        model.setPayment_option(olderlist.getString("payment_option"));
                                        model.setSessionMainCat(olderlist.getString("sessionMainCat"));

                                        /*model.setUpdated_at(olderlist.getString("updated_at"));
                                        model.setShop_name(olderlist.getString("shop_name"));
                                        model.setRecharge_points(olderlist.getString("recharge_points"));
                                        model.setSellername(olderlist.getString("sellername"));
                                        model.setShop_contact(olderlist.getString("shop_contact"));
                                        model.setUser_fname(olderlist.getString("user_fname"));
                                        model.setUser_lname(olderlist.getString("user_lname"));
                                        model.setRemark(olderlist.getString("user_remark"));
                                        model.setOrder_count(olderlist.getString("order_count"));*/

                                        orderArrayList.add(model);
                                    }
                                    orderlistAdapter.notifyDataSetChanged();
                                }else {
                                    tv_network_con.setVisibility(View.VISIBLE);
                                    tv_network_con.setText("No Records Found");
                                }
                            }else if(strStatus=false){
                                tv_network_con.setVisibility(View.VISIBLE);
                                tv_network_con.setText("No Records Found");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //srl_refreshorderlist.setRefreshing(false);
                        progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(mContext).addToRequestQueue(orderListRequest);
    }
    /* @Override
    public void onRefresh() {
        if(Connectivity.isConnected(mContext)){
            getOrderList("");//2019-08-13
        }else {
            tv_network_con.setVisibility(View.VISIBLE);
        }
    }*/
    public void setDate()
    {
        showDialog(999);
        //Toast.makeText(getApplicationContext(), "ca", Toast.LENGTH_SHORT).show();
    }
    public void  setDateEnd()
    {
        showDialog(100);
        //Toast.makeText(getApplicationContext(), "ca", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected Dialog onCreateDialog(int id)
    {
        // TODO Auto-generated method stub
        if (id == 999)
        {
            datePickerDialog = new DatePickerDialog(this, myDateListener, year, month, day);
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            return datePickerDialog;
        }else  if (id == 100)
        {
            datePickerDialogEndDate = new DatePickerDialog(this, myDateListenerEndDate, year, month, day);
            datePickerDialogEndDate.getDatePicker().setMaxDate(System.currentTimeMillis());
            return datePickerDialogEndDate;
        }
        return null;
    }
    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener()
    {
        @SuppressLint("ResourceAsColor")
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3)
        {
            // TODO Auto-generated method stub
            // arg1 = year
            // arg2 = month
            // arg3 = day
            //arg0.setBackgroundColor(red);
            arg0.setMaxDate(System.currentTimeMillis());
            showDate(arg1, arg2+1, arg3);
        }
    };
    private DatePickerDialog.OnDateSetListener myDateListenerEndDate = new DatePickerDialog.OnDateSetListener()
    {
        @SuppressLint("ResourceAsColor")
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3)
        {
            // TODO Auto-generated method stub
            // arg1 = year
            // arg2 = month
            // arg3 = day
            //arg0.setBackgroundColor(red);
            arg0.setMaxDate(System.currentTimeMillis());
            showEndDate(arg1, arg2+1, arg3);
        }
    };
    String  months;
    private void showDate(int year, int month, int day)
    {

        tv_selecteddate.setText(day+"-"+month+"-"+year);
        //Date d = new Date(year, month, day);
        //SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        //String strDate = dateFormatter.format(year,month,day);
        //getOrderList(year+"-"+month+"-"+day);
        startDate=year+"-"+month+"-"+day;
        //getOrderList(year+"-"+month+"-"+day);
    }
    private void showEndDate(int year, int month, int day)
    {
        tv_selectedenddate.setText(new StringBuilder().append(day).append("-").append(month).append("-").append(year));
        //Date d = new Date(year, month, day);
        //SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        //String strDate = dateFormatter.format(year,month,day);
        //getOrderList(year+"-"+month+"-"+day);
        endDate=year+"-"+month+"-"+(day+1);
        //getOrderList(year+"-"+month+"-"+day);
    }

    //RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private OrderHistoryActivity.RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final OrderHistoryActivity.RecyclerTouchListener.ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
