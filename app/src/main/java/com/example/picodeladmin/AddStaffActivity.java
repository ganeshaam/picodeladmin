package com.example.picodeladmin;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;

public class AddStaffActivity extends AppCompatActivity {

    Context mContext;
    SharedPreferencesUtils sharedPreferencesUtils;
    public static final String TAG = "StafflistActivity";
    AlertDialog progressDialog;
    ArrayList<StaffModel> staffModelArrayList;
    ArrayList<String> cityList;
    RecyclerView recycler_view_staff;
    private RecyclerView.LayoutManager layoutManager;
    private StaffListAdaptor staffListAdaptor;
    FloatingActionButton fb_staff_list;
    public static final int PICK_IMAGE = 1,CAPTURE_IMG = 2;
    Bitmap delivery_boybitmap=null;
    String[] PERMISSIONS = {android.Manifest.permission.WRITE_EXTERNAL_STORAGE,  Manifest.permission.CAMERA};
    int PERMISSION_ALL = 1;
    ImageView Deliveryboy_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_staff);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Staff List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = AddStaffActivity.this;
        cityList = new ArrayList<>();
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();
        recycler_view_staff = findViewById(R.id.recycler_view_staff);
        fb_staff_list = findViewById(R.id.fb_staff_list);

        staffModelArrayList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(mContext);
        recycler_view_staff.setLayoutManager(layoutManager);
        staffListAdaptor = new StaffListAdaptor(mContext,staffModelArrayList);
        recycler_view_staff.setAdapter(staffListAdaptor);

        //Code to check permission in the above marshmallow versions
        if (!hasPermissions(AddStaffActivity.this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(AddStaffActivity.this, PERMISSIONS, PERMISSION_ALL);
        }
        fb_staff_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog();
            }
        });
        getStaffList();
        getCityList();
    }

    private void select_images(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE);
    }
    private void capture_image(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAPTURE_IMG);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE) {
            //TODO: action
            //Get the url from data
            Uri selectedImageUri = data.getData();
            //for bit map data
            try {
                   /* Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    thumbnail.compress(Bitmap.CompressFormat.PNG, 100, bytes);
                    shop_photo1.setImageBitmap(thumbnail);
                    shopbitmap=thumbnail;*/
                //Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(AddStaffActivity.this.getContentResolver(),selectedImageUri);
                Deliveryboy_image.setImageBitmap(bitmap);
               // delivery_boybitmap = bitmap;
                delivery_boybitmap = getResizedBitmap(bitmap,500);
                Log.e("shopbitmap", "" + delivery_boybitmap);
            } catch (Exception e) {
                e.getMessage();
                Log.e("BitmapExp", "" + e.getMessage());
            }
        }

        if(requestCode == CAPTURE_IMG){
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            //thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            thumbnail.compress(Bitmap.CompressFormat.PNG, 75, bytes);

            try {
                Deliveryboy_image.setImageBitmap(thumbnail);
                //delivery_boybitmap = thumbnail;
                delivery_boybitmap = getResizedBitmap(thumbnail,500);
                Log.e("ImageBitCapture:",""+delivery_boybitmap.toString());
            } catch (Exception e) {
                e.getMessage();
            }
        }
    }

    private void getStaffList(){
        //get Mobile verify
        if(Connectivity.isConnected(mContext)){

            String urlOtpsend = StaticUrl.stafflist;
            // +"?shop_id="+"0";
            progressDialog.show();

            StringRequest stringRequestOtp = new StringRequest(Request.Method.GET,
                    urlOtpsend,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                staffModelArrayList.clear();
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status==true){

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    Log.e("ProfileResponse:",""+jsonArray.toString());

                                    for (int i=0; i<jsonArray.length(); i++){
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        StaffModel staffModel = new StaffModel();
                                        staffModel.setId(jsonObject1.getString("id"));
                                        staffModel.setStaffName(jsonObject1.getString("staffname"));
                                        staffModel.setMobile(jsonObject1.getString("contact"));
                                        //staffModel.setAge(jsonObject1.getString("age"));
                                        staffModel.setImageUrl(jsonObject1.getString("image"));
                                        staffModelArrayList.add(staffModel);
                                    }
                                    Log.e("arraylist size", String.valueOf(staffModelArrayList.size()));
                                    staffListAdaptor.notifyDataSetChanged();
                                }else if(status==false) {
                                    Toast.makeText(mContext,"No Records Found",Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestOtp);
        }else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }
    private  void  customDialog(){
        final Dialog dialog = new Dialog(AddStaffActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_addstaff);

        final EditText name = dialog.findViewById(R.id.et_delivery_boyname);
        final EditText  contact= dialog.findViewById(R.id.et_delivery_contact);
        final Spinner sp_city= dialog.findViewById(R.id.sp_city);
        Deliveryboy_image = dialog.findViewById(R.id.img_deliveryboy);
        final Button upload = dialog.findViewById(R.id.btn_upload_boy_photo);
        //text.setText(msg);
        ArrayAdapter<String> adapter = new ArrayAdapter(mContext,android.R.layout.simple_spinner_item,cityList);
        sp_city.setAdapter(adapter);
        Deliveryboy_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //select_images();
                capture_image();

                if(delivery_boybitmap!=null){
                    Deliveryboy_image.setImageBitmap(delivery_boybitmap);
                }
            }
        });

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                select_images();
            }
        });

        Button dialogButton2 =  dialog.findViewById(R.id.btn_add_deliveryboy);
        dialogButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("ValueName:",""+name.getText().toString());
                Log.e("ValueContact:",""+contact.getText().toString());

                if(name.getText().toString().equalsIgnoreCase("")){
                    name.requestFocus();
                    name.setError("Please Enter Name");
                }else if(sp_city.getSelectedItem().toString().isEmpty()){
                    Toast.makeText(mContext,"Please Select City",Toast.LENGTH_LONG).show();
                }/*else if(contact.getText().toString().equalsIgnoreCase("")){
                    contact.requestFocus();
                    contact.setError("Please Enter Contact");
                }else if(!contact.getText().toString().matches(MobilePattern)) {
                    Toast.makeText(mContext,"Please Enter valid Contact",Toast.LENGTH_LONG).show();
                }else */{
                    StaffRegistration(name.getText().toString(), contact.getText().toString(), sp_city.getSelectedItem().toString());
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }
    public void StaffRegistration(final String NAME,final  String CONTACT, String staff_city) {

        if(Connectivity.isConnected(mContext)) {

            progressDialog.show();

            String targetUrl = StaticUrl.addstaff;

            VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                    targetUrl,
              new com.android.volley.Response.Listener<NetworkResponse>() {
                @Override
                public void onResponse(NetworkResponse response) {
                    progressDialog.dismiss();
                    String resultResponse = new String(response.data);
                    Log.d("delreg",resultResponse);
                    try {
                        JSONObject jsonObject = new JSONObject(resultResponse);
                        Log.e(TAG, "respDeleveryboy" + resultResponse.toString());
                        Log.e(TAG, "jsonObject" + jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        if(status = true)
                        {
                            JSONObject jsonObjectData =  jsonObject.getJSONObject("data");

                            /*shop_id = jsonObjectData.getString("shop_id");
                              seller_name =  jsonObjectData.getString("name");
                              admin_type =  jsonObjectData.getString("admin_type");*/
                            getStaffList();

                            Toast.makeText(mContext,"Delivery Boy Registered",Toast.LENGTH_LONG).show();

                        }else if(status = false){
                            Log.d(TAG,"status_false"+resultResponse);
                            Toast.makeText(mContext,"Registration Not Done..!!",Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("VolleYIMAGE_EXP:", "" + e.getMessage());
                        progressDialog.dismiss();
                    }
                }
            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    NetworkResponse networkResponse = error.networkResponse;

                    String errorMessage = "Unknown error";
                    if (networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            errorMessage = "Request timeout";

                        } else if (error.getClass().equals(NoConnectionError.class)) {
                            errorMessage = "Failed to connect server";
                        }
                    } else {
                        String result = new String(networkResponse.data);
                        try {
                            JSONObject response = new JSONObject(result);

                            Log.d("MultipartErrorRES:", "" + response.toString());
                            String status = response.getString("status");
                            String message = response.getString("message");
                            Log.e("Error Status", status);
                            Log.e("Error Message", message);
                            if (networkResponse.statusCode == 404) {
                                errorMessage = "Resource not found";
                            } else if (networkResponse.statusCode == 401) {
                                errorMessage = message + " Please try again";

                            } else if (networkResponse.statusCode == 400) {
                                errorMessage = message + " Check your inputs";
                            } else if (networkResponse.statusCode == 500) {
                                errorMessage = message + " Something is getting wrong";
                            }
                        } catch (JSONException e) {
                            //  progressDialog.dismiss();
                            e.printStackTrace();
                        }
                    }
                    Log.i("Error", errorMessage);
                    error.printStackTrace();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {

                    Map<String, String> params = new HashMap<>();
                    params.put("added_by", sharedPreferencesUtils.getEmailId());
                    params.put("staffname", NAME);
                    params.put("contact", CONTACT);
                    //params.put("staff_city", staff_city);

                    Log.e("Params", "" + params);
                    return params;
                }

           /* @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String credentials = "#concord$" + ":" + "!@#con co@$rd!@#";
                String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Basic " + base64EncodedCredentials);
                return headers;
            }*/

                @Override
                protected Map<String, DataPart> getByteData() {
                    Map<String, DataPart> params = new HashMap<>();
                    params.put("imageurl", new DataPart(getBytesFromBitmap(delivery_boybitmap)));
                    Log.e("ParamsImg", "" + params);
                    return params;
                }
            };

            multipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));

            VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);
        }
        else {
            Toast.makeText(mContext,"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }
    public byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }
    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }
    //RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private  AddStaffActivity.RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final AddStaffActivity.RecyclerTouchListener.ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }
    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getCityList() { //TODO Server method here
        if (Connectivity.isConnected(AddStaffActivity.this)) {
            cityList.clear();

            /*JSONObject params = new JSONObject();
            try {
                params.put("contact", gateWay.getContact());
                params.put("v_city", sp_city_spinner.getSelectedItem().toString()); //sp_city_value
                params.put("v_state", spinner.getSelectedItem().toString());
                params.put("type", "Android");//true or false
                Log.e("userParm:",""+params);
            } catch (JSONException e) {
                e.printStackTrace();
            }*/

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.activeCityForAdmin, null, new Response.Listener<JSONObject>() {

                @SuppressWarnings("CanBeFinal")
                @Override
                public void onResponse(JSONObject response) {
                    //  if (!response.isNull("posts")) {
                    try {



                        JSONArray cityJsonArray = response.getJSONArray("posts");
                        for (int i = 0; i < cityJsonArray.length(); i++) {
                            JSONObject jsonCityData = cityJsonArray.getJSONObject(i);
                            cityList.add(jsonCityData.getString("city"));
                        }

                        /*ArrayAdapter<String> adapter = new ArrayAdapter(LoginActivity.this,android.R.layout.simple_spinner_item,cityList);
                        sp_getCity.setAdapter(adapter);*/


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //gateWay.progressDialogStop();

                }
                //}
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    //TODO ServerError method here
                }
            });
            // AppController.getInstance().addToRequestQueue(request);
            VolleySingleton.getInstance(AddStaffActivity.this).addToRequestQueue(request);
        }
    }

}
