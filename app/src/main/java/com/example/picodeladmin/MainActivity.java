package com.example.picodeladmin;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;


import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;


import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.view.MenuItem;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.Menu;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import dmax.dialog.SpotsDialog;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,SwipeRefreshLayout.OnRefreshListener {

    Context mContext;
    AlertDialog progressDialog;
    private RecyclerView.LayoutManager layoutManager;
    ArrayList<OrderlistModel> orderArrayList;
    ArrayList<OTPModel> otpModelArrayList;
    ArrayList<FreelancerModel> modelArrayList;
    private FreelancerAdapter freelancerAdapter;
    private OrderlistAdapter orderlistAdapter;
    private OTPListAdaptor otpListAdaptor;
    RecyclerView rv_home_orderlist;
    CardView cv_area_list,cv_area_list2;
    String strLatitude="",strLongitude="",strDeliveryinkm="",logintype="",flag="admin",appversion,areaName="areaName",category="Grocery";
    TextView tv_network_con;
    SwipeRefreshLayout srl_refreshorderlist;
    SharedPreferencesUtils sharedPreferencesUtils;
    String back = "false";
    int page = 0;
    FloatingActionButton fab,fab2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mContext = MainActivity.this;
        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
        tv_network_con = findViewById(R.id.tv_network_con);
        rv_home_orderlist = findViewById(R.id.rv_home_orderlist);
        cv_area_list = findViewById(R.id.cv_area_list);
        cv_area_list2 = findViewById(R.id.cv_area_list2);
        orderArrayList = new ArrayList<>();
        otpModelArrayList = new ArrayList<>();
        rv_home_orderlist.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(mContext);
        rv_home_orderlist.setLayoutManager(layoutManager);
        rv_home_orderlist.setItemAnimator(new DefaultItemAnimator());
        orderlistAdapter = new OrderlistAdapter(orderArrayList,mContext,flag);
        rv_home_orderlist.setAdapter(orderlistAdapter);
        srl_refreshorderlist = findViewById(R.id.srl_refreshorderlist);
        srl_refreshorderlist.setOnRefreshListener(this);
        fab = findViewById(R.id.fab);
        fab2 = findViewById(R.id.fab2);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
                back = "false";
                page++;
                Toast.makeText(MainActivity.this,"Page"+page,Toast.LENGTH_LONG).show();
                getOrderList();
            }
        });

        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                page--;
                back = "true";
                if(page<=0){
                    page =0;
                }
                Toast.makeText(MainActivity.this,"Page"+page,Toast.LENGTH_LONG).show();
                getOrderList();
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        areaName = getIntent().getStringExtra("areaName");
        flag = getIntent().getStringExtra("areaWise_order");
        category = getIntent().getStringExtra("category");

        if(Connectivity.isConnected(mContext)){
            Log.e("Flag_value:",""+flag);
            getOrderList();
        }else {
            tv_network_con.setVisibility(View.VISIBLE);
        }

        cv_area_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(mContext,FreelancerActivity.class);
                intent.putExtra("category","Grocery");
                //intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
            }
        });

        cv_area_list2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext,FreelancerActivity.class);
                intent.putExtra("category","Fruits and Veggies");
                //intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
            }
        });

        //order details
        rv_home_orderlist.addOnItemTouchListener(new RecyclerTouchListener(mContext, rv_home_orderlist,
                new RecyclerTouchListener.ClickListener()
                {
                    @Override
                    public void onClick(View view, int position)
                    {
                        String orderId = orderArrayList.get(position).getOrder_id();
                        String cartUniqueId = orderArrayList.get(position).getCart_unique_id();
                        String orderByUserId = orderArrayList.get(position).getUser_id();
                        String cartUserId = orderArrayList.get(position).getCart_user_id();
                        String shipping_address = orderArrayList.get(position).getShipping_address();
                        String latitude = orderArrayList.get(position).getLatitude();
                        String longitude = orderArrayList.get(position).getLongitude();
                        String total_amount = orderArrayList.get(position).getTotal_amount();
                        String order_date  = orderArrayList.get(position).getOrder_date();
                        String address_id = orderArrayList.get(position).getAddress_id();
                        String assign_delboy_id = orderArrayList.get(position).getAssign_delboy_id();
                        String action_time = orderArrayList.get(position).getActionTime();
                        String address1 = orderArrayList.get(position).getAddress1();
                        String address2 = orderArrayList.get(position).getAddress2();
                        String address3 = orderArrayList.get(position).getAddress3();
                        String address4 = orderArrayList.get(position).getAddress4();
                        String address5 = orderArrayList.get(position).getAddress5();
                        String area = orderArrayList.get(position).getArea();
                        String pincode = orderArrayList.get(position).getPincode();
                        String showcart = orderArrayList.get(position).getShow_cart();
                        String updatedat = orderArrayList.get(position).getUpdated_at();
                        String aam_delivery_charge = orderArrayList.get(position).getAam_delivery_charge();
                        String delivery_date = orderArrayList.get(position).getDelivery_date();
                        String delivery_time = orderArrayList.get(position).getDelivery_time();
                        String delivery_method = orderArrayList.get(position).getDelivery_method();
                        String payment_option = orderArrayList.get(position).getPayment_option();
                        String sessionMainCat = orderArrayList.get(position).getSessionMainCat();
                        String assignShopId = orderArrayList.get(position).getAssign_shop_id();
                        String user_remark = orderArrayList.get(position).getRemark();
                        String order_count = orderArrayList.get(position).getOrder_count();
                        String nshop_name = orderArrayList.get(position).getNshop_name();
                        String nearbyshopid = orderArrayList.get(position).getNearbyshopid();

                        //chkOrderStatus(orderId,cartUniqueId,orderByUserId,cartUserId,cartUserId,shipping_address,latitude,longitude,total_amount,order_date,address_id,assign_delboy_id,action_time);

                        Intent intent = new Intent(mContext,OrderDetailActivity.class);
                        intent.putExtra("orderId",orderId);
                        intent.putExtra("cartUniqueId",cartUniqueId);
                        intent.putExtra("userId",orderByUserId);
                        intent.putExtra("cartUserId",cartUserId);
                        intent.putExtra("shipping_address",shipping_address);
                        intent.putExtra("latitude",latitude);
                        intent.putExtra("longitude",longitude);
                        intent.putExtra("total_amount",total_amount);
                        intent.putExtra("order_date",order_date);
                        intent.putExtra("address_id",address_id);
                        intent.putExtra("assign_delboy_id",assign_delboy_id);
                        intent.putExtra("action_time",action_time);
                        intent.putExtra("address1",address1);
                        intent.putExtra("address2",address2);
                        intent.putExtra("address3",address3);
                        intent.putExtra("address4",address4);
                        intent.putExtra("address5",address5);
                        intent.putExtra("area",area);
                        intent.putExtra("pincode",pincode);
                        intent.putExtra("showcart",showcart);
                        intent.putExtra("flag",flag);
                        intent.putExtra("updatedat",updatedat);
                        intent.putExtra("aam_delivery_charge",aam_delivery_charge);
                        intent.putExtra("areaName","areaName");
                        intent.putExtra("delivery_date",delivery_date);
                        intent.putExtra("delivery_time",delivery_time);
                        intent.putExtra("delivery_method",delivery_method);
                        intent.putExtra("payment_option",payment_option);
                        intent.putExtra("sessionMainCat",sessionMainCat);
                        intent.putExtra("assignShopId",assignShopId);
                        intent.putExtra("user_remark",user_remark);
                        intent.putExtra("order_count",order_count);
                        intent.putExtra("nearbyshopid",nearbyshopid);
                        //intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(intent);
                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {
                        Toast.makeText(mContext,"No Records",Toast.LENGTH_SHORT).show();
                    }
                }));
    }

    public void getOrderList(){
        progressDialog.show();
        orderArrayList.clear();
        srl_refreshorderlist.setRefreshing(true);
//areaWise_order  // category
        String targetUrlOrderlist = null;
        try {
            targetUrlOrderlist = StaticUrl.orderlist
                    + "?latitude=" + strLatitude
                    + "&longitude=" + strLongitude
                    + "&deliveryinkm=" + strDeliveryinkm
                    + "&shopid=78"
                    + "&delboyid=0"
                    + "&areaName="+ URLEncoder.encode(areaName,"utf-8")
                    + "&flag=" + flag
                    + "&page=" + page
                    + "&back=" + back
                    + "&city=" + sharedPreferencesUtils.getAdminCity()
                    + "&category=" + URLEncoder.encode(category,"utf-8")
                    + "&tblName="+ URLEncoder.encode("Seller","utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        Log.d("targetUrlOrderlist",targetUrlOrderlist);

        StringRequest orderListRequest = new StringRequest(Request.Method.GET,
                targetUrlOrderlist,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("orderlist_admin",response);
                        //cv_viewmore.setVisibility(View.VISIBLE);
                        srl_refreshorderlist.setRefreshing(false);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean strStatus = jsonObject.getBoolean("status");
                            if ((strStatus=true)){
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if(jsonArray.length()>0){

                                    for(int i = 0; i < jsonArray.length(); i++){
                                        JSONObject olderlist = jsonArray.getJSONObject(i);
                                        OrderlistModel model = new OrderlistModel();
                                        model.setOrder_id(olderlist.getString("order_id"));
                                        model.setUser_id(olderlist.getString("user_id"));
                                        model.setAssign_shop_id(olderlist.getString("assign_shop_id"));
                                        model.setAssign_delboy_id(olderlist.getString("assign_delboy_id"));
                                        model.setStatus(olderlist.getString("status"));
                                        model.setCart_user_id(olderlist.getString("cart_user_id"));
                                        model.setCart_unique_id(olderlist.getString("cart_unique_id"));
                                        model.setPayment_method(olderlist.getString("payment_method"));
                                        model.setUpdate_payment_method(olderlist.getString("update_payment_method"));
                                        model.setPayment_type(olderlist.getString("payment_type"));
                                        model.setReferrence_transaction_id(olderlist.getString("referrence_transaction_id"));
                                        model.setTotal_amount(olderlist.getString("total_amount"));
                                        model.setUpdated_total_amount(olderlist.getString("updated_total_amount"));
                                        model.setShipping_address(olderlist.getString("shipping_address"));
                                        model.setLatitude(olderlist.getString("latitude"));
                                        model.setLongitude(olderlist.getString("longitude"));
                                        model.setUpdated_shipping_charges(olderlist.getString("updated_shipping_charges"));
                                        model.setOrder_date(olderlist.getString("order_date"));
                                        model.setUser_pending_bill(olderlist.getString("user_pending_bill"));
                                        model.setPending_bill_status(olderlist.getString("pending_bill_status"));
                                        model.setPending_bill_updatedy(olderlist.getString("pending_bill_updatedy"));
                                        model.setPending_bii_date(olderlist.getString("pending_bii_date"));
                                        model.setDistance(olderlist.getString("distance"));
                                        model.setAddress_id(olderlist.getString("address_id"));
                                        model.setActionTime(olderlist.getString("action_time"));
                                        model.setAam_delivery_charge(olderlist.getString("aam_delivery_charge"));
                                        model.setAddress1(olderlist.getString("address1"));
                                        model.setAddress2(olderlist.getString("address2"));
                                        model.setAddress3(olderlist.getString("address3"));
                                        model.setAddress4(olderlist.getString("address4"));
                                        model.setAddress5(olderlist.getString("address5"));
                                        model.setArea(olderlist.getString("area"));
                                        model.setPincode(olderlist.getString("pincode"));
                                        model.setShow_cart(olderlist.getString("show_cart"));
                                        model.setDelivery_date(olderlist.getString("delivery_date"));

                                        model.setDelivery_time(olderlist.getString("delivery_time"));
                                        model.setDelivery_method(olderlist.getString("delivery_method"));
                                        model.setAdmin_approve(olderlist.getString("admin_approve"));
                                        //model.setApproveBy(olderlist.getString("approveBy"));
                                        model.setApproveBy("0");
                                        model.setPayment_option(olderlist.getString("payment_option"));
                                        model.setSessionMainCat(olderlist.getString("sessionMainCat"));
                                        model.setUpdated_at(olderlist.getString("updated_at"));
                                        model.setShop_name(olderlist.getString("shop_name"));
                                        model.setNshop_name(olderlist.getString("nshop_name"));
                                        model.setRecharge_points(olderlist.getString("recharge_points"));
                                        model.setSellername(olderlist.getString("sellername"));
                                        model.setShop_contact(olderlist.getString("shop_contact"));
                                        model.setUser_fname(olderlist.getString("user_fname"));
                                        model.setUser_lname(olderlist.getString("user_lname"));
                                        model.setRemark(olderlist.getString("user_remark"));
                                        model.setOrder_count(olderlist.getString("order_count"));
                                        model.setNearbyshopid(olderlist.getString("nearbyshopid"));
                                        model.setOrder_from(olderlist.getString("order_from"));
                                        orderArrayList.add(model);
                                    }
                                    orderlistAdapter.notifyDataSetChanged();
                                }else {
                                    tv_network_con.setVisibility(View.VISIBLE);
                                    tv_network_con.setText("No Records Found");
                                }
                            }else if(strStatus=false){
                                tv_network_con.setVisibility(View.VISIBLE);
                                tv_network_con.setText("No Records Found");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        srl_refreshorderlist.setRefreshing(false);
                        progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(mContext).addToRequestQueue(orderListRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(Connectivity.isConnected(mContext)){
            //getOrderList();
        }else {
            tv_network_con.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
            Intent intent = new Intent(mContext,MainActivity.class);
            intent.putExtra("areaName",areaName);
            intent.putExtra("areaWise_order","admin");
            intent.putExtra("category","Grocery");
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
            finishAffinity();//nav_orderbyId
        } else if (id == R.id.nav_RequestList) {////getOTPbyContact
            Intent intent = new Intent(mContext,RequestListActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }else if (id == R.id.nav_orderbyId) {////getOTPbyContact
            searchOrdeIdDialog();
        }else if (id == R.id.nav_pdf_byID) {////getPDFbyOrderID
            getPDFbyOrderID();
        }else if (id == R.id.nav_otp) {////getPDFbyOrderID
            getOTPbyContact();
        } else if (id == R.id.nav_history) {//nav_register
            Intent intent = new Intent(mContext,OrderHistoryActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }else if (id == R.id.nav_register) {//nav_register
            Intent intent = new Intent(mContext,RegistrationActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }else if (id == R.id.nav_rejectedOrder) {
            Intent intent = new Intent(mContext,RejectedOrderActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }  else if (id == R.id.nav_logout) {
            logoutDialog();
        } else if (id == R.id.nav_share) {
            Intent intent = new Intent(mContext,WhatsAppToolActivity.class);
            //intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        } else if (id == R.id.nav_addstaff) {
            Intent intent = new Intent(mContext,AddStaffActivity.class);
            //  intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);//nav_notify
        } else if (id == R.id.nav_notify) {
            Intent intent = new Intent(mContext,SendNotificationActivity.class);
            //  intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);//nav_notify
            startActivity(intent);
        }else if(id == R.id.nav_enquire){
            Intent intent = new Intent(mContext,ListShopEnquireActivity.class);
            //  intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }else if(id == R.id.nav_price){
            Intent intent = new Intent(mContext,PriceChangeActivity.class);
            //  intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }else if(id == R.id.nav_shop_link){
            Intent intent = new Intent(mContext,ShopLinkActivity.class);
            //  intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }else if(id == R.id.nav_franchisee_transaction){
            Intent intent = new Intent(mContext,FranchiseePaymentActivity.class);
            //  intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }//nav_franchisee_transaction
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onRefresh() {
        if(Connectivity.isConnected(mContext)){
            orderArrayList.clear();
            getOrderList();
        }else {
            tv_network_con.setVisibility(View.VISIBLE);
        }
    }

    //RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private MainActivity.RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final MainActivity.RecyclerTouchListener.ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }

    //logout dialog
    public void logoutDialog() {
        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to logout?")
                .setPositiveButton("Logout", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sharedPreferencesUtils.logout();
                        Intent intent = new Intent(mContext,LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                })
                .setNegativeButton("Cancel", null)
                .show();
    }

    public void getOrderbyID(String orderid_value){
        progressDialog.show();
        orderArrayList.clear();
        srl_refreshorderlist.setRefreshing(true);
//areaWise_order  // category
        String targetUrlOrderlist = null;
        try {
            targetUrlOrderlist = StaticUrl.getorderbyid
                    + "?order_id=" + orderid_value;
                    //+ "&city=" + sharedPreferencesUtils.getAdminCity();

        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d("Url_single_OrderId_",targetUrlOrderlist);

        StringRequest orderListRequest = new StringRequest(Request.Method.GET,
                targetUrlOrderlist,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("orderlist_admin",response);
                        //cv_viewmore.setVisibility(View.VISIBLE);
                        srl_refreshorderlist.setRefreshing(false);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean strStatus = jsonObject.getBoolean("status");
                            if ((strStatus==true)){
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if(jsonArray.length()>0){

                                    for(int i = 0; i < jsonArray.length(); i++){
                                        JSONObject olderlist = jsonArray.getJSONObject(i);
                                        OrderlistModel model = new OrderlistModel();
                                        model.setOrder_id(olderlist.getString("order_id"));
                                        model.setUser_id(olderlist.getString("user_id"));
                                        model.setAssign_shop_id(olderlist.getString("assign_shop_id"));
                                        model.setAssign_delboy_id(olderlist.getString("assign_delboy_id"));
                                        model.setStatus(olderlist.getString("status"));
                                        model.setCart_user_id(olderlist.getString("cart_user_id"));
                                        model.setCart_unique_id(olderlist.getString("cart_unique_id"));
                                        model.setPayment_method(olderlist.getString("payment_method"));
                                        model.setUpdate_payment_method(olderlist.getString("update_payment_method"));
                                        model.setPayment_type(olderlist.getString("payment_type"));
                                        model.setReferrence_transaction_id(olderlist.getString("referrence_transaction_id"));
                                        model.setTotal_amount(olderlist.getString("total_amount"));
                                        model.setUpdated_total_amount(olderlist.getString("updated_total_amount"));
                                        model.setShipping_address(olderlist.getString("shipping_address"));
                                        model.setLatitude(olderlist.getString("latitude"));
                                        model.setLongitude(olderlist.getString("longitude"));
                                        model.setUpdated_shipping_charges(olderlist.getString("updated_shipping_charges"));
                                        model.setOrder_date(olderlist.getString("order_date"));
                                        model.setUser_pending_bill(olderlist.getString("user_pending_bill"));
                                        model.setPending_bill_status(olderlist.getString("pending_bill_status"));
                                        model.setPending_bill_updatedy(olderlist.getString("pending_bill_updatedy"));
                                        model.setPending_bii_date(olderlist.getString("pending_bii_date"));
                                        model.setDistance(olderlist.getString("distance"));
                                        model.setAddress_id(olderlist.getString("address_id"));
                                        model.setActionTime(olderlist.getString("action_time"));
                                        model.setAam_delivery_charge(olderlist.getString("aam_delivery_charge"));
                                        model.setAddress1(olderlist.getString("address1"));
                                        model.setAddress2(olderlist.getString("address2"));
                                        model.setAddress3(olderlist.getString("address3"));
                                        model.setAddress4(olderlist.getString("address4"));
                                        model.setAddress5(olderlist.getString("address5"));
                                        model.setArea(olderlist.getString("area"));
                                        model.setPincode(olderlist.getString("pincode"));
                                        model.setShow_cart(olderlist.getString("show_cart"));
                                        model.setDelivery_date(olderlist.getString("delivery_date"));

                                        model.setDelivery_time(olderlist.getString("delivery_time"));
                                        model.setDelivery_method(olderlist.getString("delivery_method"));
                                        model.setAdmin_approve(olderlist.getString("admin_approve"));
                                        //model.setApproveBy(olderlist.getString("approveBy"));
                                        model.setApproveBy("0");
                                        model.setPayment_option(olderlist.getString("payment_option"));
                                        model.setSessionMainCat(olderlist.getString("sessionMainCat"));
                                        model.setUpdated_at(olderlist.getString("updated_at"));
                                        model.setShop_name(olderlist.getString("shop_name"));
                                        model.setRecharge_points(olderlist.getString("recharge_points"));
                                        model.setSellername(olderlist.getString("sellername"));
                                        model.setShop_contact(olderlist.getString("shop_contact"));
                                        model.setUser_fname(olderlist.getString("user_fname"));
                                        model.setUser_lname(olderlist.getString("user_lname"));
                                        model.setRemark(olderlist.getString("user_remark"));
                                        model.setOrder_count(olderlist.getString("order_count"));
                                        model.setNearbyshopid(olderlist.getString("nearbyshopid"));
                                        orderArrayList.add(model);
                                    }
                                    orderlistAdapter.notifyDataSetChanged();
                                }else {
                                    tv_network_con.setVisibility(View.VISIBLE);
                                    tv_network_con.setText("No Records Found");
                                }
                            }else if(strStatus==false){
                                tv_network_con.setVisibility(View.VISIBLE);
                                tv_network_con.setText("No Records Found");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        srl_refreshorderlist.setRefreshing(false);
                        progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(mContext).addToRequestQueue(orderListRequest);
    }

    //Method to show the OrdeID Details
    public void searchOrdeIdDialog() {

        final Dialog dialog = new Dialog(this,android.R.style.Theme_Light);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        View view = getLayoutInflater().inflate(R.layout.activity_orderbyid, null);

        Button btn_submitreson = view.findViewById(R.id.btn_search);
        final EditText et_message = view.findViewById(R.id.et_message);
        // getOrderCartList();

        btn_submitreson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value_orderid = et_message.getText().toString();
                getOrderbyID(value_orderid);
                dialog.dismiss();

                //}
            }
        });
        dialog.setContentView(view);

        dialog.show();
    }

    // Method to Get OTP By contact
    public void getOTPbyContact() {

        final Dialog dialog = new Dialog(this,android.R.style.Theme_Light);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        View view = getLayoutInflater().inflate(R.layout.dailog_get_otp, null);

        Button btn_submitreson = view.findViewById(R.id.btn_search);
        final EditText et_message = view.findViewById(R.id.et_message);
        final TextView tv_otp = view.findViewById(R.id.tv_otp);
        final TextView tv_email = view.findViewById(R.id.tv_email);
        final TextView tv_date = view.findViewById(R.id.tv_date);

        final RecyclerView rv_otpList = view.findViewById(R.id.rv_otpList);

        // getOrderCartList();
        layoutManager = new LinearLayoutManager(mContext);
        rv_otpList.setLayoutManager(layoutManager);
        rv_otpList.setItemAnimator(new DefaultItemAnimator());
        otpListAdaptor = new OTPListAdaptor(otpModelArrayList,MainActivity.this);
        rv_otpList.setAdapter(otpListAdaptor);
        otpListAdaptor.notifyDataSetChanged();

        btn_submitreson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value_contact = et_message.getText().toString();

                JSONObject params = new JSONObject();
                try {
                    params.put("contact", value_contact);
                    params.put("user_id", "4063");
                    params.put("name", "pramod");
                    params.put("start", "200");
                    params.put("end", "500");
                    params.put("flag", flag);//true or false
                    params.put("type", "otpinfo");//query type

                    Log.e("userParm:",""+params);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String url ="https://www.picodel.com/And/shopping/AppAPI/getAllUserData.php";
                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {

                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.e("ReferInsertRes", "" + response.toString());

                        if (response.isNull("posts")) {


                        }else{


                            try {
                                JSONArray postsJsonArray = response.getJSONArray("posts");

                                Log.e("OPT_RS:",""+postsJsonArray.toString());

                                for (int i = 0; i < postsJsonArray.length(); i++) {
                                    JSONObject jSonClassificationData = postsJsonArray.getJSONObject(i);

                                    UserModel userModel = new UserModel();
                                    userModel.setName(jSonClassificationData.getString("vcode"));
                                    tv_otp.setText("OTP: "+jSonClassificationData.getString("vcode"));
                                    userModel.setContact(jSonClassificationData.getString("contact"));//contact
                                    tv_date.setText("Date Time: "+jSonClassificationData.getString("vdate"));
                                    tv_email.setText("Email: "+jSonClassificationData.getString("email"));
                                    userModel.setId(jSonClassificationData.getString("id"));//user_id
                                    userModel.setShop_name(jSonClassificationData.getString("vdate"));//shop_name




                                }


                            }catch (Exception e){

                            }

                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();


                    }
                });
                VolleySingleton.getInstance(MainActivity.this).addToRequestQueue(request);

                //}
            }
        });


        //Method to get the Last 10 OTP List
        JSONObject params = new JSONObject();
        otpModelArrayList.clear();
        try {
            params.put("contact", "7276201058");
            params.put("user_id", "4063");
            params.put("name", "pramod");
            params.put("start", "200");
            params.put("end", "500");
            params.put("flag", flag);//true or false
            params.put("type", "otpinfo10");//query type

            Log.e("userParm:",""+params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String url ="https://www.picodel.com/And/shopping/AppAPI/getAllUserData.php";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {

            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(JSONObject response) {

                Log.e("ReferInsertRes", "" + response.toString());

                if (response.isNull("posts")) {


                }else{


                    try {
                        JSONArray postsJsonArray = response.getJSONArray("posts");

                        Log.e("OPT_RS:",""+postsJsonArray.toString());

                        for (int i = 0; i < postsJsonArray.length(); i++) {
                            JSONObject jSonClassificationData = postsJsonArray.getJSONObject(i);

                            OTPModel userModel = new OTPModel();
                            userModel.setVcode(jSonClassificationData.getString("vcode"));
                            userModel.setContact(jSonClassificationData.getString("contact"));//contact
                            //userModel.setId(jSonClassificationData.getString("id"));//user_id
                            userModel.setVdate(jSonClassificationData.getString("vdate"));//shop_name
                            otpModelArrayList.add(userModel);
                        }
                        otpListAdaptor = new OTPListAdaptor(otpModelArrayList,MainActivity.this);
                        rv_otpList.setAdapter(otpListAdaptor);
                        otpListAdaptor.notifyDataSetChanged();


                    }catch (Exception e){

                    }

                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();


            }
        });
        VolleySingleton.getInstance(MainActivity.this).addToRequestQueue(request);


        dialog.setContentView(view);

        dialog.show();
    }

    // Method to Get Franchise Details associate to the Shop

    public void getFranchiseeDetails(String admin_id){
        progressDialog.show();
        orderArrayList.clear();
        srl_refreshorderlist.setRefreshing(true);
//areaWise_order  // category
        String targetUrlOrderlist = null;
        try {
            targetUrlOrderlist = StaticUrl.checkadmin
                    + "?admin_id=" + admin_id;
            //+ "&city=" + sharedPreferencesUtils.getAdminCity();

        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d("Url_checkadmin_",targetUrlOrderlist);

        StringRequest orderListRequest = new StringRequest(Request.Method.GET,
                targetUrlOrderlist,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("checkadmin_Res",response);
                        //cv_viewmore.setVisibility(View.VISIBLE);
                        srl_refreshorderlist.setRefreshing(false);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean strStatus = jsonObject.getBoolean("status");
                            if ((strStatus==true)){
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if(jsonArray.length()>0){

                                    for(int i = 0; i < jsonArray.length(); i++){
                                        JSONObject olderlist = jsonArray.getJSONObject(i);
                                        OrderlistModel model = new OrderlistModel();

                                        /*model.setShop_contact(olderlist.getString("shop_contact"));
                                        model.setUser_fname(olderlist.getString("user_fname"));
                                        model.setUser_lname(olderlist.getString("user_lname"));
                                        model.setRemark(olderlist.getString("user_remark"));
                                        model.setOrder_count(olderlist.getString("order_count"));*/
                                        //orderArrayList.add(model);
                                    }
                                    //orderlistAdapter.notifyDataSetChanged();
                                }else {
                                    tv_network_con.setVisibility(View.VISIBLE);
                                    tv_network_con.setText("No Records Found");
                                }
                            }else if(strStatus==false){
                                tv_network_con.setVisibility(View.VISIBLE);
                                tv_network_con.setText("No Records Found");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        srl_refreshorderlist.setRefreshing(false);
                        progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(mContext).addToRequestQueue(orderListRequest);
    }

    // Method to Get OTP By contact
    public void getPDFbyOrderID() {

        final Dialog dialog = new Dialog(this,android.R.style.Theme_Light);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        View view = getLayoutInflater().inflate(R.layout.dialog_download_pdf, null);

        Button btn_submitreson = view.findViewById(R.id.btn_search);
        final EditText et_message = view.findViewById(R.id.et_message);


        btn_submitreson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value_id = et_message.getText().toString();

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://www.picodel.com/seller/shopwise-orders/pdffile?id="+value_id));
                startActivity(intent);
            }
        });

        dialog.setContentView(view);

        dialog.show();
    }
}
