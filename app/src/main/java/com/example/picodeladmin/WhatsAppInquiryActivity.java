package com.example.picodeladmin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import dmax.dialog.SpotsDialog;

public class WhatsAppInquiryActivity extends AppCompatActivity {


    TextView tv_refercode;
    Button bt_share,bt_receive,bt_setmessage,bt_page3,bt_page4,bt_search;
    SharedPreferencesUtils sharedPreferencesUtils;
    android.app.AlertDialog progressDialog2;
    ArrayList<UserModel> arrayList;

    RecyclerView rv_referral_points;
    ReferralPointsAdaptor referralPointsAdaptor;
    ArrayList<UserModel> ReferPointsList ;
    LinearLayoutManager layoutManager;
    String sms_text ="",start ="1",end="200";
    String temp_sms_text ="!";
    EditText et_message;
    SearchView et_name;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_whats_app_inquiry);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("WhatsApp Inquiry");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        progressDialog2 = new SpotsDialog.Builder().setContext(this).build();
        sharedPreferencesUtils = new SharedPreferencesUtils(WhatsAppInquiryActivity.this);


        tv_refercode = findViewById(R.id.tv_refercode);
        bt_share = findViewById(R.id.bt_next);
        bt_receive = findViewById(R.id.bt_page2);
        bt_page3 = findViewById(R.id.bt_page3);
        bt_page4 = findViewById(R.id.bt_page4);
        bt_search = findViewById(R.id.bt_search);
        bt_setmessage = findViewById(R.id.bt_setmessage);
        et_message = findViewById(R.id.et_message);
        et_name = findViewById(R.id.et_name);
        rv_referral_points = findViewById(R.id.rv_referral_points);
        rv_referral_points.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(this,1);
        rv_referral_points.setLayoutManager(layoutManager);
        // rv_referral_points.setItemAnimator(new DefaultItemAnimator());
        arrayList = new ArrayList<>();
        referralPointsAdaptor = new ReferralPointsAdaptor(arrayList,this,"shop");
        rv_referral_points.setAdapter(referralPointsAdaptor);

        et_message.setText("");

        bt_setmessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sms_text = et_message.getText().toString();
                et_message.setText(sms_text);
                //Log.e("name",et_name.getQuery().toString());
            }
        });



        bt_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                start = end;
                int temp = Integer.parseInt(end);
                int temp2 = temp+200;
                end = String.valueOf(temp2);
                getAllUsers(start, String.valueOf(temp2),"false");
                Log.e("start_end:",""+start+":"+end);

            }
        });

        bt_receive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                end = start;
                int temp = Integer.parseInt(start);
                int temp2 = temp-200;
                String start1 = String.valueOf(temp2);
                start = start1;
                getAllUsers(start,end,"false");
                Log.e("start_end_p:",""+start+":"+end);

            }
        });
        getAllUsers(start,end,"false");
        bt_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAllUsers(start,end,"true");
                hideKeyBoard();
            }
        });

        Log.e("name",et_name.getQuery().toString());


        rv_referral_points.addOnItemTouchListener(new RecyclerTouchListener(WhatsAppInquiryActivity.this, rv_referral_points,
                new RecyclerTouchListener.ClickListener()
                {
                    @Override
                    public void onClick(View view, int position)
                    {

                        String contact = arrayList.get(position).getContact();
                        openWhatsApp(contact);
                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {
                        // Toast.makeText(mContext,"No Records",Toast.LENGTH_SHORT).show();
                    }
                }));

    }


    private void openWhatsApp(String mobileNo) {

        try {
            //String text = "Glad to share the link to get 50/- off on your 1st order of Groceries and Vegetables from PICODEL!\nIt helps to get delivery of the order from 60 minutes to Max 3 hours...\nDownload PICODEL App and enjoy lot more unique features !";// Replace with your message.

            String toNumber = mobileNo; // Replace with mobile phone number without +Sign or leading zeros, but with country code
            //Suppose your country is India and your phone number is “xxxxxxxxxx”, then you need to send “91xxxxxxxxxx”.


            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("http://api.whatsapp.com/send?phone="+91+toNumber+"&text="+sms_text));
            startActivity(intent);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private void getAllUsers(String start, String end, String flag) { //TODO Server method here

        progressDialog2.show();
        arrayList.clear();

        JSONObject params = new JSONObject();
        try {
            params.put("contact", "7276201058");
            params.put("user_id", "4063");
            params.put("name", et_name.getQuery().toString());
            params.put("start", start);
            params.put("end", end);
            params.put("flag", flag);//true or false
            params.put("type", "inquiry");//query type

            Log.e("userParm:",""+params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String url ="https://www.picodel.com/And/shopping/AppAPI/getAllUserData.php";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {

            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(JSONObject response) {

                Log.e("ReferInsertRes", "" + response.toString());
                progressDialog2.dismiss();
                if (response.isNull("posts")) {

                    progressDialog2.dismiss();
                }else{


                    try {
                        JSONArray postsJsonArray = response.getJSONArray("posts");

                        Log.e("user_RS:",""+postsJsonArray.toString());

                        for (int i = 0; i < postsJsonArray.length(); i++) {
                            JSONObject jSonClassificationData = postsJsonArray.getJSONObject(i);

                            UserModel userModel = new UserModel();
                            userModel.setName(jSonClassificationData.getString("fname"));
                            userModel.setContact(jSonClassificationData.getString("contact"));//contact
                            userModel.setId(jSonClassificationData.getString("user_id"));//user_id
                            userModel.setShop_name(jSonClassificationData.getString("shop_name"));//shop_name

                            arrayList.add(userModel);

                        }

                        referralPointsAdaptor.notifyDataSetChanged();
                    }catch (Exception e){

                    }

                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog2.dismiss();

            }
        });
        VolleySingleton.getInstance(WhatsAppInquiryActivity.this).addToRequestQueue(request);

    }

    ///RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final RecyclerTouchListener.ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {

                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }

    private void hideKeyBoard() {
        View view = getCurrentFocus();  // Check if no view has focus:
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
