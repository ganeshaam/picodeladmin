package com.example.picodeladmin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.example.picodeladmin.firebase.WhatsAppOneRuppeActivity;

public class WhatsAppToolActivity extends AppCompatActivity {
    CardView cv_user,cv_shop,cv_market,cv_OneRupee,cv_del_boy;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_whats_app_tool);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("WhatsApp Tool Activity");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        mContext = WhatsAppToolActivity.this;


        cv_user = findViewById(R.id.cv_user);
        cv_shop = findViewById(R.id.cv_shop);
        cv_market = findViewById(R.id.cv_market);
        cv_OneRupee = findViewById(R.id.cv_OneRupee);
        cv_del_boy = findViewById(R.id.cv_del_boy);

        cv_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext,WhatAppActivity.class);
                startActivity(intent);
            }
        });

        cv_OneRupee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, WhatsAppOneRuppeActivity.class);
                startActivity(intent);

            }
        });

        cv_shop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(mContext,WhatsAppShopList.class);
                startActivity(intent);

            }
        });

        cv_market.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(mContext,WhatsAppInquiryActivity.class);
                startActivity(intent);

            }
        });

        cv_del_boy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext,WhatsAppDelboyList.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
