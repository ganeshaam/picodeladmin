package com.example.picodeladmin;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import dmax.dialog.SpotsDialog;

public class TimeSlotActivity extends Activity {

    RecyclerView rv_timeslot_rb;
    Button btn_time_slots;
    LinearLayoutManager layoutManager;
    TimeSlotAdaptor paymentModeAdaptor;
    Context mContext;
    ArrayList<String> slot_array = new ArrayList<>();
    AlertDialog progressDialog;
    SharedPreferencesUtils sharedPreferencesUtils;
    String total_amount,orderId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_slot);

        //requestWindowFeature(Window.FEATURE_NO_TITLE);

        rv_timeslot_rb = findViewById(R.id.rv_timeslot_rb);
        btn_time_slots= findViewById(R.id.btn_time_slots);

        mContext = TimeSlotActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();

        Intent intent = getIntent();
        slot_array= intent.getStringArrayListExtra("slot_array");
        orderId = intent.getStringExtra("orderId");
        total_amount = intent.getStringExtra("total_amount");
        Log.e("slot_array:",""+slot_array);

        rv_timeslot_rb.hasFixedSize();
        layoutManager = new LinearLayoutManager(mContext);
        rv_timeslot_rb.setLayoutManager(layoutManager);
        rv_timeslot_rb.setItemAnimator(new DefaultItemAnimator());

        //slot_array

        paymentModeAdaptor = new TimeSlotAdaptor(mContext,slot_array);
        rv_timeslot_rb.setAdapter(paymentModeAdaptor);

        rv_timeslot_rb.addOnItemTouchListener(new RecyclerTouchListener(mContext, rv_timeslot_rb,
                new RecyclerTouchListener.ClickListener()
                {
                    @Override
                    public void onClick(View view, int position)
                    {
                        //  delBoyId =  paymentModeList.get(position).getDelBoyId();
                        String slot = slot_array.get(position);
                        //payID= paymentModeList.get(position).getID();
                        //Toast.makeText(mContext,slot_array.get(position),Toast.LENGTH_SHORT).show();
                        Log.e("Selected Slot value:",""+slot);
                       // acceptOrder(sharedPreferencesUtils.getShopID(),orderId,slot);

                        Intent intent = new Intent(mContext,ShoplistActivity.class);
                        intent.putExtra("orderId",orderId);
                        intent.putExtra("slot",slot);
                        intent.putExtra("total_amount",total_amount);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(intent);

                        //tv_selectedvalue.setText("Payment Mode:"+""+slot);
                        //paymentModeAdaptor.notifyDataSetChanged();
                        finish();
                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {
                        // Toast.makeText(mContext,"No Records",Toast.LENGTH_SHORT).show();
                    }
                }));
    }

    ///RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private  RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final RecyclerTouchListener.ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {

                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }

    //accept order
    public void acceptOrder(String shopID, String orderId, String delivery_time) {
        if (Connectivity.isConnected(mContext)) {

            progressDialog.show();

            String urlAcceptOrder = null;
            try {
                urlAcceptOrder = StaticUrl.acceptorder
                        + "?order_id=" + orderId
                        + "&approveBy=" +  sharedPreferencesUtils.getEmailId()
                        //+ "&manager_id=" + sharedPreferencesUtils.getManagerId()
                        + "&delivery_time=" + URLEncoder.encode(delivery_time,"utf-8")
                        + "&assign_shop_id=" + shopID
                        + "&tblName=Seller"//+sharedPreferencesUtils.getAdminType()
                        + "&totalAmount="+total_amount
                        + "&shopName="+sharedPreferencesUtils.getShopName();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            // + "&delivery_time="+delivery_time;
            Log.d("urlAcceptOrder",urlAcceptOrder);
            StringRequest stringRequestAcceptOrder = new StringRequest(Request.Method.GET,
                    urlAcceptOrder,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                Log.e("acceptorderRes:",""+response);
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status = true) {
                                    //JSONObject jsonObjectData =  jsonObject.getJSONObject("data");
                                    Toast.makeText(mContext, "Order Assigned", Toast.LENGTH_SHORT).show();

                                  /*   Intent intent = new Intent(mContext,OrderListActivity.class);
                                    intent.putExtra("flag","approved_order");
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(intent); */

                                } else if (status = false) {
                                    Toast.makeText(mContext, "Sorry. This order you can not accept.", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestAcceptOrder);
        } else {
            Toast.makeText(mContext, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

}
