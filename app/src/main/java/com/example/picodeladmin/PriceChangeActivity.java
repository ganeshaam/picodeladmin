package com.example.picodeladmin;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PriceChangeActivity extends AppCompatActivity {


    private RecyclerView rv_zone_price;
    private Spinner sp_area;
    private Button btn_submit;

    SharedPreferencesUtils sharedPreferencesUtils;
    Context mContext;
    ArrayList<ZonePriceModel> arrayList;
    ArrayList<String> areaList;
    LinearLayoutManager layoutManager;
    ZonePriceAdaptor zonePriceAdaptor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_price_change);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Area Wise Price Change");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = PriceChangeActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);

        rv_zone_price = findViewById(R.id.rv_zone_price);
        sp_area = findViewById(R.id.sp_area);
        btn_submit = findViewById(R.id.btn_submit);

        rv_zone_price.setHasFixedSize(true);
        arrayList = new ArrayList<>();
        areaList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(mContext);
        rv_zone_price.setLayoutManager(layoutManager);
        rv_zone_price.setItemAnimator(new DefaultItemAnimator());
        /*zonePriceAdaptor = new ZonePriceAdaptor(arrayList, mContext);
        rv_zone_price.setAdapter(zonePriceAdaptor);
        zonePriceAdaptor.notifyDataSetChanged();*/
        // initialize Recycler view


        getZoneValue();

    }

    // get the city list at the spinner value  //add_adminDetails.php
    private void getZoneValue() { //TODO Server method here
        if (Connectivity.isConnected(PriceChangeActivity.this)) {
            arrayList.clear();

            JSONObject params = new JSONObject();
            try {
                params.put("contact", sharedPreferencesUtils.getPhoneNumber());
                params.put("city", sharedPreferencesUtils.getAdminCity());
                params.put("fc_city", sharedPreferencesUtils.getFC_City());
                Log.e("getZone_Param:", "" + params);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, StaticUrl.urlget_zone_area, params, new Response.Listener<JSONObject>() {

                @SuppressWarnings("CanBeFinal")
                @Override
                public void onResponse(JSONObject response) {

                    Log.e("getZone_Resp:", "" + response);
                    //insert successfully
                    try {
                        //String result = response.getString("posts");

                        JSONArray cityJsonArray = response.getJSONArray("posts");
                        for (int i = 0; i < cityJsonArray.length(); i++) {
                            JSONObject jsonCityData = cityJsonArray.getJSONObject(i);
                            ZonePriceModel model = new ZonePriceModel();
                            model.setPrice_val(jsonCityData.getString("price_val"));
                            model.setZone(jsonCityData.getString("zone"));
                            arrayList.add(model);
                        }

                        Log.e("zone Size List:",""+arrayList.size());
                        zonePriceAdaptor = new ZonePriceAdaptor(arrayList, mContext);
                        rv_zone_price.setAdapter(zonePriceAdaptor);
                        zonePriceAdaptor.notifyDataSetChanged();

                        JSONArray cityJsonArray2 = response.getJSONArray("arealist");
                        for (int i = 0; i < cityJsonArray2.length(); i++) {
                            JSONObject jsonCityData = cityJsonArray2.getJSONObject(i);
                           /* ZonePriceModel model = new ZonePriceModel();
                            model.setPrice_val(jsonCityData.getString("price_val"));
                            model.setZone(jsonCityData.getString("zone"));*/
                            areaList.add(jsonCityData.getString("are"));
                        }

                        ArrayAdapter<String> adapter = new ArrayAdapter(PriceChangeActivity.this,android.R.layout.simple_spinner_item,areaList);
                        sp_area.setAdapter(adapter);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                //}
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    //gateWay.progressDialogStop();
                    //TODO ServerError method here
                }
            });
            // AppController.getInstance().addToRequestQueue(request);
            VolleySingleton.getInstance(PriceChangeActivity.this).addToRequestQueue(request);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
              /*  if(sharedPreferencesUtils.getAdminType().equals("Delivery Boy")){
                    finish();
                }else {
                    Intent intent = new Intent(mContext, OrderListActivity.class);
                    intent.putExtra("flag", flag);
                    intent.putExtra("areaName", areaName);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intent);
                }*/
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
