package com.example.picodeladmin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import dmax.dialog.SpotsDialog;

public class ListShopEnquireActivity extends AppCompatActivity {

    Context mContext;
    AlertDialog progressDialog;
    private RecyclerView.LayoutManager layoutManager;
    ArrayList<ShopEnquireModel> shopEnquireModelArrayList;
    private ShopEnquireAdapter shopEnquireAdapter;
    private SharedPreferencesUtils sharedPreferencesUtils;
    RecyclerView rv_shopEnquire;
    TextView tv_network_con;
    androidx.appcompat.widget.SearchView et_search_name;
    Button bt_search;
    FloatingActionButton fb_shop_enquire;
    Spinner sp_search_type;
    String filter_type="shop_name",fc_city,city;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_shop_enquire);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("All Shop List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = ListShopEnquireActivity.this;
        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);

        fc_city = sharedPreferencesUtils.getFC_City();
        city = sharedPreferencesUtils.getAdminCity();

        tv_network_con = findViewById(R.id.tv_network_con);
        rv_shopEnquire = findViewById(R.id.rv_shopEnquire);
        fb_shop_enquire = findViewById(R.id.fb_shop_enquire);
        et_search_name = findViewById(R.id.et_search_name);
        bt_search = findViewById(R.id.bt_search);
        sp_search_type = findViewById(R.id.sp_search_type);

        shopEnquireModelArrayList = new ArrayList<>();
        rv_shopEnquire.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(mContext);
        rv_shopEnquire.setLayoutManager(layoutManager);
        rv_shopEnquire.setItemAnimator(new DefaultItemAnimator());
        shopEnquireAdapter = new ShopEnquireAdapter(shopEnquireModelArrayList,mContext);
        rv_shopEnquire.setAdapter(shopEnquireAdapter);

        if (Connectivity.isConnected(mContext)) {
            getShopEnquireList();
        } else {
            tv_network_con.setVisibility(View.VISIBLE);
        }

        /*fb_shop_enquire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enquireDialog();
            }
        });*/

        sp_search_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                filter_type= sp_search_type.getSelectedItem().toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        rv_shopEnquire.addOnItemTouchListener(new RecyclerTouchListener(mContext,rv_shopEnquire,new RecyclerTouchListener.ClickListener(){

            @Override
            public void onClick(View view, int position) {

                Toast.makeText(mContext,shopEnquireModelArrayList.get(position).getId(),Toast.LENGTH_LONG).show();
                /*Intent recharge_plan = new Intent(mContext,RechargeHistoryActivity.class);
                recharge_plan.putExtra("shop_id",shopEnquireModelArrayList.get(position).getId());
                startActivity(recharge_plan);*/
                String shop_id = shopEnquireModelArrayList.get(position).getId();
                String distance = shopEnquireModelArrayList.get(position).getDistance();
                Log.e("distance:",""+distance);
                option_Dialog(shop_id,distance);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        bt_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSearchShop(et_search_name.getQuery().toString(),filter_type);
            }
        });
    }

    private void getSearchShop(String shop, String filter_type) { //TODO Server method here


        JSONObject params = new JSONObject();
        try {
            params.put("name", et_search_name.getQuery().toString());
            params.put("type", "shop");//true or false
            params.put("filter_type", filter_type);
            params.put("fc_city", fc_city);
            params.put("city", city);
            Log.e("userParm:",""+params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String url ="https://www.picodel.com/And/shopping/AppAPI/getAllShopData.php";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {

            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(JSONObject response) {

                Log.e("ShopSearchRes", "" + response.toString());
                //progressDialog2.dismiss();
                if (response.isNull("posts")) {

                  //  progressDialog2.dismiss();
                }else{

                    try {
                        JSONArray jsonArray = response.getJSONArray("posts");
                        shopEnquireModelArrayList.clear();

                        Log.e("user_RS:",""+jsonArray.toString());
                        if(jsonArray.length()>0){
                            // Log.d("orderlist",""+jsonArray);
                            for(int i = 0; i < jsonArray.length(); i++){
                                JSONObject shopList = jsonArray.getJSONObject(i);
                                ShopEnquireModel model = new ShopEnquireModel();
                                model.setId(shopList.getString("shop_id"));
                                model.setContact(shopList.getString("contact"));
                                model.setSellerName(shopList.getString("sellername"));
                                model.setShopName(shopList.getString("shop_name"));
                                model.setArea(shopList.getString("area"));
                                model.setUpdatedAt(shopList.getString("created_at"));
                                model.setRecharge_amount("recharge_amount");
                                model.setDistance(shopList.getString("distance"));
                                model.setReg_by("signup_suggested_by");
                                shopEnquireModelArrayList.add(model);
                            }
                            Log.e("ShopList_Size",""+shopEnquireModelArrayList.size());
                            shopEnquireAdapter.notifyDataSetChanged();
                        }else {
                            tv_network_con.setVisibility(View.VISIBLE);
                            tv_network_con.setText("No Records Found");
                        }


                        //referralPointsAdaptor.notifyDataSetChanged();
                    }catch (Exception e){

                    }



                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                //progressDialog2.dismiss();

            }
        });
        VolleySingleton.getInstance(ListShopEnquireActivity.this).addToRequestQueue(request);

    }

    //shop list based on order location
    public void getShopEnquireList(){

        progressDialog.show();
        shopEnquireModelArrayList.clear();

        String  urlShopEnquireList = StaticUrl.shopenquireList+"?admin_city="+sharedPreferencesUtils.getAdminCity()
                +"&contact="+sharedPreferencesUtils.getPhoneNumber()+"&fc_city="+sharedPreferencesUtils.getFC_City();

        Log.d("urlShopEnquireList",urlShopEnquireList);
        StringRequest orderListRequest = new StringRequest(Request.Method.GET,
                urlShopEnquireList,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("urlShopEnquireList",response);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean strStatus = jsonObject.getBoolean("status");
                            if ((strStatus==true)){
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if(jsonArray.length()>0){
                                    // Log.d("orderlist",""+jsonArray);
                                    for(int i = 0; i < jsonArray.length(); i++){
                                        JSONObject shopList = jsonArray.getJSONObject(i);
                                        ShopEnquireModel model = new ShopEnquireModel();
                                        model.setId(shopList.getString("shop_id"));
                                        model.setContact(shopList.getString("contact"));
                                        model.setSellerName(shopList.getString("sellername"));
                                        model.setShopName(shopList.getString("shop_name"));
                                        model.setArea(shopList.getString("area"));
                                        model.setUpdatedAt(shopList.getString("created_at"));
                                        model.setRecharge_amount("recharge_amount");
                                        model.setReg_by("signup_suggested_by");
                                        shopEnquireModelArrayList.add(model);
                                    }
                                    Log.e("ShopList_Size",""+shopEnquireModelArrayList.size());
                                    shopEnquireAdapter.notifyDataSetChanged();
                                }else {
                                    tv_network_con.setVisibility(View.VISIBLE);
                                    tv_network_con.setText("No Records Found");
                                }
                            }else if(strStatus=false){
                                tv_network_con.setVisibility(View.VISIBLE);
                                tv_network_con.setText("No Records Found");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(mContext).addToRequestQueue(orderListRequest);
    }
    public void enquireDialog(){
        final Dialog dialog = new Dialog(this,android.R.style.Theme_Light);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        View view = getLayoutInflater().inflate(R.layout.dialog_addenquireshop, null);
        final EditText tv_enq_name= view.findViewById(R.id.tv_enq_name);
        final EditText tv_enq_shopName= view.findViewById(R.id.tv_enq_shopName);
        final EditText tv_enq_contact= view.findViewById(R.id.tv_enq_contact);
        final EditText tv_enq_area= view.findViewById(R.id.tv_enq_area);
        Button btn_addshop= view.findViewById(R.id.btn_enquire_shop);
        btn_addshop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                addEnquireShop(tv_enq_name.getText().toString(),tv_enq_shopName.getText().toString(),tv_enq_contact.getText().toString(),tv_enq_area.getText().toString());
            }
        });

        dialog.setContentView(view);
        dialog.show();
    }
    public void addEnquireShop(String sellername,String shopname,String contact,String area){
        if (Connectivity.isConnected(mContext)) {

            progressDialog.show();

            // String urlCancelOrder =  String.format(StaticUrl.cancelorder,orderId,shopId,assign_delboy_id,reason_of_rejection);

            String urlCancelOrder = StaticUrl.addshopenquire
                    + "?sellername="+sellername
                    + "&shopname="+shopname
                    + "&contact="+contact
                    + "&area="+area;

            StringRequest stringRequestCancelOrder = new StringRequest(Request.Method.GET,
                    urlCancelOrder,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.e("CancelResp:", "" + response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONObject jsonObjectData = jsonObject.getJSONObject("data");
                                    //Log.e("cancel_Order_Res:", "" + jsonObjectData.toString());
                                    getShopEnquireList();
                                } else if (!status) {
                                    Toast.makeText(mContext, "Shop Added.", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestCancelOrder);
        } else {
            Toast.makeText(mContext, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
       /* if(sharedPreferencesUtils.getAdminType().equals("Delivery Boy")){
            super.onBackPressed();
            finish();
        }else {
            Intent intent = new Intent(mContext, OrderListActivity.class);
            intent.putExtra("flag", flag);
            intent.putExtra("areaName", areaName);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }*/
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
              /*  if(sharedPreferencesUtils.getAdminType().equals("Delivery Boy")){
                    finish();
                }else {
                    Intent intent = new Intent(mContext, OrderListActivity.class);
                    intent.putExtra("flag", flag);
                    intent.putExtra("areaName", areaName);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intent);
                }*/
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    //RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ListShopEnquireActivity.RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final ListShopEnquireActivity.RecyclerTouchListener.ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }

    //Method to choose Options Details
    public void option_Dialog(final String shop_id, final String distance) {

        final Dialog dialog = new Dialog(this,android.R.style.Theme_Light);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        View view = getLayoutInflater().inflate(R.layout.activity_choose_option, null);

        Button btn_distance = view.findViewById(R.id.btn_distance);
        Button btn_history = view.findViewById(R.id.btn_history);
        //final EditText et_message = view.findViewById(R.id.et_message);
        // getOrderCartList();

        btn_distance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //getOrderbyID(value_orderid);
                distance_Dialog(shop_id,distance);
                dialog.dismiss();

                //}
            }
        });

        btn_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(mContext,shop_id,Toast.LENGTH_LONG).show();
                Intent recharge_plan = new Intent(mContext,RechargeHistoryActivity.class);
                recharge_plan.putExtra("shop_id",shop_id);
                startActivity(recharge_plan);
                dialog.dismiss();
            }
        });

        dialog.setContentView(view);

        dialog.show();
    }

    //Method to choose Options Details
    public void distance_Dialog(final String shop_id, final String distance) {

        final Dialog dialog = new Dialog(this,android.R.style.Theme_Light);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        View view = getLayoutInflater().inflate(R.layout.layout_distance_change, null);

        Button btn_set_distance = view.findViewById(R.id.btn_set_distance);
        final TextView tv_old_distance = view.findViewById(R.id.tv_old_distance);

        tv_old_distance.setText("Old Distance: "+distance);

        final EditText et_message = view.findViewById(R.id.et_message);
        // getOrderCartList();

        btn_set_distance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                changeShopDistance(shop_id,et_message.getText().toString());
                dialog.dismiss();

                //}
            }
        });



        dialog.setContentView(view);

        dialog.show();
    }

    // API To change the Distance Area  //urlchange_shop_distance

    private void changeShopDistance(String shopid, String value) { //TODO Server method here


        JSONObject params = new JSONObject();
        try {
            params.put("shop_id", shopid);
            params.put("value", value);// new Shop Distance
            params.put("fc_city", fc_city);
            params.put("city", city);
            params.put("type", "distance");
            Log.e("changeDistanceParam:",""+params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String url = StaticUrl.urlchange_shop_distance;
        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {

            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(JSONObject response) {

                Log.e("ShopDUpdateRes", "" + response.toString());
                //progressDialog2.dismiss();
                if (response.isNull("posts")) {
                    //  progressDialog2.dismiss();
                }else{

                    try {
                        String res = response.getString("posts");

                        if(res.equalsIgnoreCase("Update successfully")){
                            Toast.makeText(mContext,"Update successfully",Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                //progressDialog2.dismiss();

            }
        });
        VolleySingleton.getInstance(ListShopEnquireActivity.this).addToRequestQueue(request);

    }



}
