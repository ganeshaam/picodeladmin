package com.example.picodeladmin;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import dmax.dialog.SpotsDialog;

public class UnplacedOrderActivity extends AppCompatActivity {

    Context mContext;
    AlertDialog progressDialog;
    private RecyclerView.LayoutManager layoutManager;
    ArrayList<ShopEnquireModel> shopEnquireModelArrayList;
    private ShopEnquireAdapter shopEnquireAdapter;
    RecyclerView rv_shopEnquire;
    TextView tv_network_con;


    String filter_type="shop_name";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unplace_order_list);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Unplaced Order List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = UnplacedOrderActivity.this;
        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();

        tv_network_con = findViewById(R.id.tv_network_con);
        rv_shopEnquire = findViewById(R.id.rv_shopEnquire);


        shopEnquireModelArrayList = new ArrayList<>();
        rv_shopEnquire.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(mContext);
        rv_shopEnquire.setLayoutManager(layoutManager);
        rv_shopEnquire.setItemAnimator(new DefaultItemAnimator());
        shopEnquireAdapter = new ShopEnquireAdapter(shopEnquireModelArrayList,mContext);
        rv_shopEnquire.setAdapter(shopEnquireAdapter);

        if (Connectivity.isConnected(mContext)) {
            getUnplaceCartList();
        } else {
            tv_network_con.setVisibility(View.VISIBLE);
        }



        rv_shopEnquire.addOnItemTouchListener(new RecyclerTouchListener(mContext,rv_shopEnquire,new RecyclerTouchListener.ClickListener(){

            @Override
            public void onClick(View view, int position) {

                Toast.makeText(mContext,shopEnquireModelArrayList.get(position).getId(),Toast.LENGTH_LONG).show();
                Intent recharge_plan = new Intent(mContext,RechargeHistoryActivity.class);
                recharge_plan.putExtra("shop_id",shopEnquireModelArrayList.get(position).getId());
                startActivity(recharge_plan);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


    }

    private void getSearchShop(String shop, String filter_type) { //TODO Server method here


        JSONObject params = new JSONObject();
        try {

            params.put("type", "shop");//true or false
            params.put("filter_type", filter_type);
            Log.e("userParm:",""+params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String url ="https://www.picodel.com/And/shopping/AppAPI/getAllShopData.php";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {

            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(JSONObject response) {

                Log.e("ReferInsertRes", "" + response.toString());
                //progressDialog2.dismiss();
                if (response.isNull("posts")) {

                    //  progressDialog2.dismiss();
                }else{

                    try {
                        JSONArray jsonArray = response.getJSONArray("posts");
                        shopEnquireModelArrayList.clear();

                        Log.e("user_RS:",""+jsonArray.toString());
                        if(jsonArray.length()>0){
                            // Log.d("orderlist",""+jsonArray);
                            for(int i = 0; i < jsonArray.length(); i++){
                                JSONObject shopList = jsonArray.getJSONObject(i);
                                ShopEnquireModel model = new ShopEnquireModel();
                                model.setId(shopList.getString("shop_id"));
                                model.setContact(shopList.getString("contact"));
                                model.setSellerName(shopList.getString("sellername"));
                                model.setShopName(shopList.getString("shop_name"));
                                model.setArea(shopList.getString("area"));
                                model.setUpdatedAt(shopList.getString("created_at"));
                                model.setRecharge_amount("recharge_amount");
                                model.setReg_by("signup_suggested_by");
                                shopEnquireModelArrayList.add(model);
                            }
                            Log.e("ShopList_Size",""+shopEnquireModelArrayList.size());
                            shopEnquireAdapter.notifyDataSetChanged();
                        }else {
                            tv_network_con.setVisibility(View.VISIBLE);
                            tv_network_con.setText("No Records Found");
                        }


                        //referralPointsAdaptor.notifyDataSetChanged();
                    }catch (Exception e){

                    }



                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                //progressDialog2.dismiss();

            }
        });
        VolleySingleton.getInstance(UnplacedOrderActivity.this).addToRequestQueue(request);

    }

    //shop list based on order location
    public void getUnplaceCartList(){

        progressDialog.show();
        shopEnquireModelArrayList.clear();

        String  urlShopEnquireList = StaticUrl.GetUnplaceOrderDetsils;

        Log.d("urlShopEnquireList",urlShopEnquireList);
        StringRequest orderListRequest = new StringRequest(Request.Method.GET,
                urlShopEnquireList,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("urlShopEnquireList",response);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean strStatus = jsonObject.getBoolean("status");
                            if ((strStatus==true)){
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if(jsonArray.length()>0){
                                    // Log.d("orderlist",""+jsonArray);
                                    for(int i = 0; i < jsonArray.length(); i++){
                                        JSONObject shopList = jsonArray.getJSONObject(i);
                                        ShopEnquireModel model = new ShopEnquireModel();
                                        model.setId(shopList.getString("shop_id"));
                                        model.setContact(shopList.getString("contact"));
                                        model.setSellerName(shopList.getString("sellername"));
                                        model.setShopName(shopList.getString("shop_name"));
                                        model.setArea(shopList.getString("area"));
                                        model.setUpdatedAt(shopList.getString("created_at"));
                                        model.setRecharge_amount("recharge_amount");
                                        model.setReg_by("signup_suggested_by");
                                        shopEnquireModelArrayList.add(model);
                                    }
                                    Log.e("ShopList_Size",""+shopEnquireModelArrayList.size());
                                    shopEnquireAdapter.notifyDataSetChanged();
                                }else {
                                    tv_network_con.setVisibility(View.VISIBLE);
                                    tv_network_con.setText("No Records Found");
                                }
                            }else if(strStatus=false){
                                tv_network_con.setVisibility(View.VISIBLE);
                                tv_network_con.setText("No Records Found");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(mContext).addToRequestQueue(orderListRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    //RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final RecyclerTouchListener.ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }

}

