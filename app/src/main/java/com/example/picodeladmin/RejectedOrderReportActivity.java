package com.example.picodeladmin;

 import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

 import android.app.AlertDialog;
 import android.content.Context;
 import android.content.Intent;
 import android.os.Bundle;
 import android.util.Log;
 import android.view.MenuItem;
 import android.webkit.WebView;
 import android.widget.Toast;

 import com.android.volley.Request;
 import com.android.volley.Response;
 import com.android.volley.VolleyError;
 import com.android.volley.toolbox.StringRequest;

 import org.json.JSONException;
 import org.json.JSONObject;

 import dmax.dialog.SpotsDialog;

public class RejectedOrderReportActivity extends AppCompatActivity {

    WebView wv_terms;
    SharedPreferencesUtils sharedPreferencesUtils;
    Context mContext;
    AlertDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rejected_order_report);

        Toolbar toolbar =   findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Rejected Order History");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mContext = RejectedOrderReportActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);

         progressDialog = new SpotsDialog.Builder().setContext(mContext).build();

        wv_terms = findViewById(R.id.wv_terms);

        Intent intent = getIntent();
        String orderId = intent.getStringExtra("orderId");

        rejectedOrderReport(orderId);
    }

    public void rejectedOrderReport(String orderid){
        if(Connectivity.isConnected(mContext)){

            progressDialog.show();

            String urlAcceptOrder = StaticUrl.getRejectedorderreports
                    + "?order_id="+orderid;

            Log.d("terms",urlAcceptOrder);

            StringRequest stringRequestAcceptOrder = new StringRequest(Request.Method.GET,
                    urlAcceptOrder,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status){
                                    String TERMS = jsonObject.getString("reports");
                                    wv_terms.loadData(TERMS, "text/html", "UTF-8");
                                }else if(!status){
                                    wv_terms.loadData("Rejected Order is Empty", "text/html", "UTF-8");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                        }
                    });
            MySingleton.getInstance(mContext).addToRequestQueue(stringRequestAcceptOrder);
        }else {
            Toast.makeText(mContext,"Check Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
