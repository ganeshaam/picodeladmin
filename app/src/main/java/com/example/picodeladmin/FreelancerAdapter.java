package com.example.picodeladmin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class FreelancerAdapter extends RecyclerView.Adapter<FreelancerAdapter.FreelancerViewHolder>{

    ArrayList<FreelancerModel> freelancerModelArrayList;
    Context mContext;

    public FreelancerAdapter(ArrayList<FreelancerModel> freelancerModelArrayList, Context mContext) {
        this.freelancerModelArrayList = freelancerModelArrayList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public FreelancerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_freelancer,parent,false);
        FreelancerAdapter.FreelancerViewHolder freelancerViewHolder = new FreelancerAdapter.FreelancerViewHolder(view);
        return freelancerViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull FreelancerViewHolder freelancerViewHolder, int position) {
        TextView tv_areaName = freelancerViewHolder.tv_areaName;
        //TextView tv_freelancerReason = freelancerViewHolder.tv_freelancerReason;
        TextView tv_area_count = freelancerViewHolder.tv_area_count;

        tv_areaName.setText(freelancerModelArrayList.get(position).getAreaName());
        tv_area_count.setText(freelancerModelArrayList.get(position).getArecount());
    }

    @Override
    public int getItemCount() {
        return freelancerModelArrayList.size();
    }

    public static class FreelancerViewHolder extends RecyclerView.ViewHolder {
        TextView tv_areaName,tv_freelancerReason,tv_area_count;
        public FreelancerViewHolder(@NonNull View itemView) {
              super(itemView);
            tv_areaName = itemView.findViewById(R.id.tv_areaName);
            tv_area_count = itemView.findViewById(R.id.tv_area_count);
            //tv_freelancerReason = itemView.findViewById(R.id.tv_freelancer_reason);
        }
    }
}
