package com.example.picodeladmin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

public class SplashActivity extends AppCompatActivity {

    Context mContext;
    Handler handler;
    SharedPreferencesUtils sharedPreferencesUtils;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mContext = SplashActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(this);

         handler = new Handler();
        handler.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                if(sharedPreferencesUtils.getLoginFlag())
                {
                    Intent splashLogin = new Intent(SplashActivity.this, MainActivity.class);
                    splashLogin.putExtra("strsync","1");
                    splashLogin.putExtra("areaName","areaName");
                    splashLogin.putExtra("category","Grocery");
                    splashLogin.putExtra("areaWise_order","admin");

                    splashLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(splashLogin);
                    finish();
                }else
                {
                    Intent splashLogin = new Intent(SplashActivity.this, LoginActivity.class);
                    splashLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(splashLogin);
                    finish();
                }
            }
        },1500);
    }
}

/*
* 6:06:04 PM: Executing task 'signingReport'...

Executing tasks: [signingReport]


> Task :app:signingReport
Variant: releaseUnitTest
Config: none
----------
Variant: debugAndroidTest
Config: debug
Store: C:\Users\User\.android\debug.keystore
Alias: AndroidDebugKey
MD5: 95:F5:3D:3C:57:E0:B0:64:75:62:17:10:FB:8E:80:0F
SHA1: A1:AD:89:E4:F1:AE:AD:BE:A9:42:70:D7:36:A9:B4:78:83:DF:C2:A6
SHA-256: D3:87:C8:94:59:AC:4B:D0:1D:8C:2D:95:2E:D7:7F:6D:AA:7B:7A:30:A4:F4:68:3C:5D:D0:B7:90:09:FD:8D:C9
Valid until: Friday, July 30, 2049
----------
Variant: release
Config: none
----------
Variant: debug
Config: debug
Store: C:\Users\User\.android\debug.keystore
Alias: AndroidDebugKey
MD5: 95:F5:3D:3C:57:E0:B0:64:75:62:17:10:FB:8E:80:0F
SHA1: A1:AD:89:E4:F1:AE:AD:BE:A9:42:70:D7:36:A9:B4:78:83:DF:C2:A6
SHA-256: D3:87:C8:94:59:AC:4B:D0:1D:8C:2D:95:2E:D7:7F:6D:AA:7B:7A:30:A4:F4:68:3C:5D:D0:B7:90:09:FD:8D:C9
Valid until: Friday, July 30, 2049
----------
Variant: debugUnitTest
Config: debug
Store: C:\Users\User\.android\debug.keystore
Alias: AndroidDebugKey
MD5: 95:F5:3D:3C:57:E0:B0:64:75:62:17:10:FB:8E:80:0F
SHA1: A1:AD:89:E4:F1:AE:AD:BE:A9:42:70:D7:36:A9:B4:78:83:DF:C2:A6
SHA-256: D3:87:C8:94:59:AC:4B:D0:1D:8C:2D:95:2E:D7:7F:6D:AA:7B:7A:30:A4:F4:68:3C:5D:D0:B7:90:09:FD:8D:C9
Valid until: Friday, July 30, 2049
----------

BUILD SUCCESSFUL in 13s
1 actionable task: 1 executed
6:06:17 PM: Task execution finished 'signingReport'.
*/
