package com.example.picodeladmin;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import dmax.dialog.SpotsDialog;

public class RequestListActivity extends AppCompatActivity {

    RecyclerView rv_RequestList;
    Context mContext;
    LinearLayoutManager layoutManager;
    DeliveryRequestAdapter requestAdapter;
    ArrayList<DeliveryModel> arrayList;
    AlertDialog progressDialog;
    TextView tv_network_con;
    SharedPreferencesUtils sharedPreferencesUtils;
    ArrayList<ShopModel> orderArrayList,orderArrayList2;
    private ShopListAdapter orderlistAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_request_list);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Request Order List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        rv_RequestList = findViewById(R.id.rv_RequestList);
        tv_network_con = findViewById(R.id.tv_network_con);

        mContext = RequestListActivity.this;
        sharedPreferencesUtils = new SharedPreferencesUtils(mContext);
        progressDialog = new SpotsDialog.Builder().setContext(mContext).build();
        arrayList = new ArrayList<>();
        orderArrayList = new ArrayList<>();
        orderArrayList2 = new ArrayList<>();

        rv_RequestList.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(mContext);
        rv_RequestList.setLayoutManager(layoutManager);
        rv_RequestList.setItemAnimator(new DefaultItemAnimator());
        requestAdapter = new DeliveryRequestAdapter(arrayList,mContext,"");
        rv_RequestList.setAdapter(requestAdapter);

        getDeliveryRequestList();
        getShopList("DeliveryRequest");

        // click on the recycler item view
        rv_RequestList.addOnItemTouchListener(new RecyclerTouchListener(mContext, rv_RequestList,
                new RecyclerTouchListener.ClickListener()
                {
                    @Override
                    public void onClick(View view, int position)
                    {
                        //delBoyId =  boyModelArrayList.get(position).getDelBoyId();
                        String  Selected_id= arrayList.get(position).getShop_id();
                        //AcceptDialog(Selected_id,"");
                        geDeliveryShopList(Selected_id);
                        Log.e("selected PAYMENT MODE",""+Selected_id);
                        Toast.makeText(mContext,arrayList.get(position).getShop_id(),Toast.LENGTH_SHORT).show();
                        //tv_selectedvalue.setText("Payment Mode:"+Selected_PaymentMode);
                        //paymentModeAdaptor.notifyDataSetChanged();
                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {
                        // Toast.makeText(mContext,"No Records",Toast.LENGTH_SHORT).show();
                    }
                }));

    }

    public void getDeliveryRequestList(){
        progressDialog.show();
        arrayList.clear();


        String targetUrlShopList = "https://www.picodel.com/seller/franchisee/getdeliverydetails"+"?shop_id="+sharedPreferencesUtils.getShopID();

        Log.d("targetUrlShopList",targetUrlShopList);
        StringRequest orderListRequest = new StringRequest(Request.Method.GET,
                targetUrlShopList,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Request_list",response);
                        //cv_viewmore.setVisibility(View.VISIBLE);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean strStatus = jsonObject.getBoolean("status");
                            if ((strStatus==true)){
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if(jsonArray.length()>0){
                                    // Log.d("orderlist",""+jsonArray);
                                    for(int i = 0; i < jsonArray.length(); i++){
                                        JSONObject olderlist = jsonArray.getJSONObject(i);
                                        DeliveryModel model = new DeliveryModel();
                                        model.setShop_id(olderlist.getString("shop_id"));
                                        model.setD_from(olderlist.getString("d_from"));
                                        model.setD_destination(olderlist.getString("d_destination"));
                                        model.setD_height(olderlist.getString("d_height"));
                                        model.setD_weight(olderlist.getString("d_weight"));
                                        model.setD_no_of_items(olderlist.getString("d_no_items"));
                                        model.setD_amount(olderlist.getString("d_amount"));
                                        model.setD_cdate(olderlist.getString("d_cdate"));
                                        //Log.d("shopName",olderlist.getString("shop_name"));
                                        arrayList.add(model);
                                    }
                                    requestAdapter = new DeliveryRequestAdapter(arrayList,mContext,"");
                                    rv_RequestList.setAdapter(requestAdapter);
                                    requestAdapter.notifyDataSetChanged();
                                }else {
                                    tv_network_con.setVisibility(View.VISIBLE);
                                    tv_network_con.setText("No Records Found");
                                }
                            }else if(strStatus=false){
                                tv_network_con.setVisibility(View.VISIBLE);
                                tv_network_con.setText("No Records Found");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(mContext).addToRequestQueue(orderListRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void acceptRequest(String ID, String Shop_id){
        progressDialog.show();
        arrayList.clear();

        String targetUrlShopList = "https://www.picodel.com/seller/franchisee/acceptdeliveryrequest"+"?d_id="+ID+"&shop_id="+Shop_id;

        Log.d("URL_AcceptRequest",targetUrlShopList);
        StringRequest orderListRequest = new StringRequest(Request.Method.GET,
                targetUrlShopList,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("RequestAccept_Res",response);
                        //cv_viewmore.setVisibility(View.VISIBLE);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean strStatus = jsonObject.getBoolean("status");
                            if ((strStatus==true)){
                                //JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if(jsonObject.length()>0){
                                    // Log.d("orderlist",""+jsonArray);
                                    Toast.makeText(mContext,"Request Accepted Successfully",Toast.LENGTH_LONG).show();

                                }else {
                                    tv_network_con.setVisibility(View.VISIBLE);
                                    tv_network_con.setText("No Records Found");
                                }
                            }else if(strStatus=false){
                                tv_network_con.setVisibility(View.VISIBLE);
                                tv_network_con.setText("No Records Found");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(mContext).addToRequestQueue(orderListRequest);
    }


    // Dialog to Accept Delivery Order
    public void AcceptDialog(final String id, final  String shopid){
        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to Accept this Delivery Order?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //finishAffinity();
                        //acceptRequest(id,shopid);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    ///RecyclerTouchListener
    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private  RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final RecyclerTouchListener.ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {

                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }

    //shop list which has extra delivery boys
    public void getShopList(String shopListType){

        progressDialog.show();
        orderArrayList.clear();
        orderArrayList2.clear();

        String targetUrlShopList = "https://www.picodel.com/And/shopping/AppAPI/testDistanceapi.php?orderId="+0+"&shopListType="+"DeliveryRequest"+"&city="+sharedPreferencesUtils.getAdminCity();
        //String targetUrlShopList = "https://www.picodel.com/And/shopping/AppAPI/testDistanceapi.php?orderId="+orderId+"&shopListType="+shopListType;

        Log.d("targetUrlShopList",targetUrlShopList);
        StringRequest orderListRequest = new StringRequest(Request.Method.GET,
                targetUrlShopList,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("orderlist",response);
                        //cv_viewmore.setVisibility(View.VISIBLE);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean strStatus = jsonObject.getBoolean("status");
                            if ((strStatus==true)){
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                if(jsonArray.length()>0){
                                    // Log.d("orderlist",""+jsonArray);
                                    for(int i = 0; i < jsonArray.length(); i++){
                                        JSONObject olderlist = jsonArray.getJSONObject(i);
                                        ShopModel model = new ShopModel();
                                        model.setShop_id(olderlist.getString("shop_id"));
                                        model.setContact(olderlist.getString("contact"));
                                        model.setEmail(olderlist.getString("email"));
                                        model.setAdmin_type(olderlist.getString("admin_type"));
                                        model.setArea(olderlist.getString("area"));
                                        model.setRecharge_amount(olderlist.getString("recharge_amount"));
                                        model.setRecharge_points(olderlist.getString("recharge_points"));
                                        model.setSellername(olderlist.getString("sellername"));
                                        model.setShop_category(olderlist.getString("shop_category"));
                                        model.setShop_name(olderlist.getString("shop_name"));
                                        model.setPreferredDeliveryAreaKm(olderlist.getString("preferred_delivery_area_km"));
                                        //Log.d("shopName",olderlist.getString("shop_name"));
                                        orderArrayList.add(model);
                                    }

                                }else {
                                    tv_network_con.setVisibility(View.VISIBLE);
                                    tv_network_con.setText("No Records Found");
                                }
                            }else if(strStatus=false){
                                tv_network_con.setVisibility(View.VISIBLE);
                                tv_network_con.setText("No Records Found");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                });
        MySingleton.getInstance(mContext).addToRequestQueue(orderListRequest);
    }

    public void geDeliveryShopList(final String d_id) {

        //final Dialog dialog = new Dialog(this);
        final Dialog dialog = new Dialog(this,android.R.style.Theme_Light);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        View view = getLayoutInflater().inflate(R.layout.dialog_delivery_shoplist, null);

        final boolean[] flagcart_item = {false};
        final ListView lv = view.findViewById(R.id.lv_reasonlist);
        Button btn_submitreson = view.findViewById(R.id.btn_reason_list);
        final RecyclerView cart_list = (RecyclerView) view.findViewById(R.id.rv_cart_list);
        cart_list.setHasFixedSize(true);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        cart_list.setLayoutManager(layoutManager);
        /* cart_list.setItemAnimator(new DefaultItemAnimator());*/
        cart_list.setVisibility(View.VISIBLE);



        Log.e("CartList Size:",""+arrayList.size());
        orderlistAdapter = new ShopListAdapter(orderArrayList, mContext);
        cart_list.setAdapter(orderlistAdapter);
        orderlistAdapter.notifyDataSetChanged();

        cart_list.addOnItemTouchListener(new RecyclerTouchListener(mContext, cart_list,
                new RecyclerTouchListener.ClickListener()
                {
                    @Override
                    public void onClick(View view, int position)
                    {
                        String shopid = orderArrayList.get(position).getShop_id();
                        Toast.makeText(mContext,shopid,Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        acceptRequest(d_id,shopid);
                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {

                    }
                }));



        btn_submitreson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext,"Accepted",Toast.LENGTH_SHORT).show();
            }
        });
        dialog.setContentView(view);

        dialog.show();
    }

}
